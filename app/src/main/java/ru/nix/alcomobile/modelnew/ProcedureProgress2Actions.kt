package ru.nix.alcomobile.modelnew

import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.*
import java.io.File


fun ProcedureProgress.applyMessageForCommand(message:Message):Command?{
    return if(type==ProcedureProgressType.UNKNOWN) CommandInfo() else null
}

fun ProcedureProgress.applyContactStateForCommand(state:ContactState):Command?{
    return when(type){
        ProcedureProgressType.WAIT_FOR_EMPLOYEE ->
            if(state.allTrue()==true && employee != null)  CommandStart() else null
        else -> null
    }
}

fun ProcedureProgress.applyAlcotestProgressForCommand(alcoTestProgress: AlcotestProgress):Command?{
    return null
}

fun ProcedureProgress.applyEmployeeForCommand(employee: Employee):Command?{
    return if(type == ProcedureProgressType.WAIT_FOR_EMPLOYEE && contactState.allTrue()==true){
        CommandStart()
    }else{
        null
    }
}

fun ProcedureProgress.applyPhotoForCommand(photo: File):Command?{
    return null
}

fun ProcedureProgress.applyConnectionStateForCommand(connectionState: ConnectionState):Command?{
    return when (connectionState) {
        ConnectionState.CONNECTED -> CommandInfo()
        else -> null
    }
}

fun ProcedureProgress.applyCanDisplayPreviewForCommand(canDisplayPreview: Boolean): Command?{
    return if(type==ProcedureProgressType.UNKNOWN) {
        CommandInfo()
    }else{
        null
    }
}


//fun procedureProgressToCommands(
//        oldProcedureProgress: ProcedureProgress,
//        newProcedureProgress: ProcedureProgress):Command?{
//    return if(oldProcedureProgress.type == ProcedureProgressType.UNKNOWN){
//        CommandInfo()
//    }else if()
//    else{
//        null
//    }
//
//
//
//}