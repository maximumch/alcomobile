package ru.nix.alcomobile.audio

import android.content.Context
import android.media.MediaPlayer
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import ru.nix.alcomobile.R

import java.lang.Exception
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private const val QUEUE_SIZE = 1

enum class VoiceMessageType{
    BAD_FLOW, BLOW, CONTINUE_BLOW, ERROR_MIRROR_OR_HOLDER_FLIPPED_DURING_TEST, HARDWARE_ERROR,
    MIRROR_OR_HOLDER_FOLDED, NEGATIVE_RESULT, POSITIVE_RESULT, PRESS_START_BUTTON, PROCESSING,
    SCAN_PASS, TIMEOUT_EXPIRED, UNKNOWN_ERROR,WAIT, CAMERA_ERROR, NO_PICTURE_RESULT, LOW_VOLTAGE

}

class VoiceMessenger(context: Context):Disposable {
    private var isDisposed:Boolean = false
    private val queue:ArrayBlockingQueue<VoiceMessageType> = ArrayBlockingQueue(QUEUE_SIZE)

    override fun isDisposed(): Boolean = isDisposed

    override fun dispose() {
        disposeVoiceMessages()
        subscription?.dispose()
        instance = null
        isDisposed=true
    }

    private var voiceMessages:Map<VoiceMessageType, MediaPlayer>?=null
    private var subscription: Disposable?=null

    private fun initVoiceMessages(context: Context){
        check(!isDisposed) { "VoiceMessenger object is disposed " }
        val mVoiceMessages = hashMapOf(
                VoiceMessageType.BAD_FLOW to MediaPlayer.create(context, R.raw.bad_flow),
                VoiceMessageType.BLOW to MediaPlayer.create(context, R.raw.blow),
                VoiceMessageType.CONTINUE_BLOW to MediaPlayer.create(context, R.raw.continue_blow),
                VoiceMessageType.ERROR_MIRROR_OR_HOLDER_FLIPPED_DURING_TEST to MediaPlayer.create(context, R.raw.error_mirror_or_holder_flipped_during_test),
                VoiceMessageType.HARDWARE_ERROR to MediaPlayer.create(context, R.raw.hardware_error),
                VoiceMessageType.MIRROR_OR_HOLDER_FOLDED to MediaPlayer.create(context, R.raw.mirror_or_holder_flipped),
                VoiceMessageType.NEGATIVE_RESULT to MediaPlayer.create(context, R.raw.negative_result),
                VoiceMessageType.POSITIVE_RESULT to MediaPlayer.create(context, R.raw.positive_result),
                VoiceMessageType.PRESS_START_BUTTON to MediaPlayer.create(context, R.raw.press_start_button),
                VoiceMessageType.PROCESSING to MediaPlayer.create(context, R.raw.processing),
                VoiceMessageType.SCAN_PASS to MediaPlayer.create(context, R.raw.scan_pass),
                VoiceMessageType.TIMEOUT_EXPIRED to MediaPlayer.create(context, R.raw.timeout_expired),
                VoiceMessageType.UNKNOWN_ERROR to MediaPlayer.create(context, R.raw.unknown_error),
                VoiceMessageType.WAIT to MediaPlayer.create(context, R.raw.wait),
                VoiceMessageType.LOW_VOLTAGE to MediaPlayer.create(context, R.raw.low_voltage)
        )
        voiceMessages = mVoiceMessages

    }

    suspend fun  setInputStream(stream: Observable<VoiceMessageType>){
        if(isDisposed) throw IllegalStateException("VoiceMessenger object is disposed ")
        subscription?.dispose()
        subscription = stream.subscribe {
            enqueueMessage(it)
            //playVoiceMessage(it)
        }

        coroutineScope {
            launch(Dispatchers.IO) {
                while(!isDisposed){
                    val message= queue.poll(Long.MAX_VALUE,TimeUnit.SECONDS)
                    playVoiceMessage(message)
                }
            }
        }

    }

    private fun enqueueMessage(message:VoiceMessageType){
        if(queue.remainingCapacity()==0){
                try {
                    queue.remove()
                }catch (e:Exception){

                }
        }
        if(queue.remainingCapacity()>0) queue.add(message)
    }

    suspend private fun playVoiceMessage(type:VoiceMessageType){
        voiceMessages?.get(type)?.apply {
            if(!isPlaying){
                start()
                waitForComplete()
            }
        }
    }

    suspend fun MediaPlayer.waitForComplete() = suspendCoroutine<Unit> {continuation->
        setOnCompletionListener {
            continuation.resume(Unit)
            setOnCompletionListener(null)
        }
        setOnErrorListener { mediaPlayer: MediaPlayer, i: Int, i1: Int ->
            continuation.resume(Unit)
            setOnErrorListener(null)
            true
        }



    }


    private fun disposeVoiceMessages(){
        val mVoiceMessages = voiceMessages
        voiceMessages = null

        if(mVoiceMessages!=null){
            for((_,mediaPlayer) in mVoiceMessages){
                mediaPlayer.reset()
                mediaPlayer.release()
            }
        }}


    companion object  {
        @Volatile private var instance: VoiceMessenger?=null

        suspend fun getInstance(context: Context) = coroutineScope {
            instance ?: synchronized(this) {
                instance ?: VoiceMessenger(context).also {
                    instance = it
                    it.initVoiceMessages(context)
                }
            }
        }
    }




}