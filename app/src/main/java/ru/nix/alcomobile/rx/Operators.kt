package ru.nix.alcomobile.rx


import android.util.Log
import android.view.Surface
import android.view.SurfaceHolder
import io.reactivex.Observable
import io.reactivex.Observable.combineLatest
import io.reactivex.Observable.merge
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.functions.Function4
import io.reactivex.observables.GroupedObservable
import ru.nix.alcomobile.camera.CameraCommand
import ru.nix.alcomobile.camera.CameraDataType
import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.*
import ru.nix.alcomobile.model.*
import java.io.File

fun Observable<AlcotestProgress>.filterResult():Observable<AlcotestResult>{

    return this.filter {it.type == AlcotestProgressType.RESULT || it.type == AlcotestProgressType.TIMEOUT }
            .map{
                val success = (it.type==AlcotestProgressType.RESULT)
                AlcotestResult(success,it.param?.toDouble()?:0.0)}
}

fun xxx_filterStartCamera(
        surfaceFromActivity:Observable<Surface>,
        activityCanDisplayPreview:Observable<Boolean>,
        needDataFromCamera:Observable<CameraDataType>,
        isCameraDeviceFree:Observable<Boolean>,
        cameraPermissionGranted:Observable<Boolean>
        ):Observable<Surface>{


    val predicateObservable = combineLatest(
            activityCanDisplayPreview,
            needDataFromCamera.map{cameraDataType ->
                Log.d("xxx_happy", "cameraTypeMap at start cameraDataType=${cameraDataType.name}, result = ${cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE}")
                cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE
            },
            isCameraDeviceFree,
            cameraPermissionGranted,
            Function4<Boolean,Boolean,Boolean,Boolean,Boolean>{t1,t2,t3,t4 -> t1 && t2 && t3 && t4}
    )

    return combineLatest(
            predicateObservable,
            surfaceFromActivity,
            BiFunction<Boolean,Surface,Pair<Boolean,Surface>>{ t,surface -> Pair(t,surface)  }
    )
            .filter { it.first }
            .map { it.second }


}

fun xxx_filterStopCamera(
        activityCanDisplayPreview:Observable<Boolean>,
        needDataFromCamera:Observable<CameraDataType>,
        isCameraDeviceFree:Observable<Boolean>,
        cameraPermissionGranted:Observable<Boolean>
):Observable<Boolean>{


    val predicateObservable = combineLatest(
            activityCanDisplayPreview,
            needDataFromCamera.map{cameraDataType ->
                Log.d("xxx_happy", "cameraTypeMap at stop cameraDataType=${cameraDataType.name}, result = ${cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE}")
                cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE},
            isCameraDeviceFree,
            cameraPermissionGranted,
            Function4<Boolean,Boolean,Boolean,Boolean,Boolean>{t1,t2,t3,t4 ->
                Log.d("xxx_happy", "Function4. t1=$t1, t2=$t2, t3=$t3, t4=$t4, result = ${!t3 && !(t1 && t2 && t4 )} ")
                !t3 && !(t1 && t2 && t4 )  }
    )

    return predicateObservable.filter { it }
}


fun filterCameraStartStop(
        surfaceFromActivity:Observable<SurfaceHolder>,
        activityCanDisplayPreview:Observable<Boolean>,
        needDataFromCamera:Observable<CameraDataType>,
        isCameraDeviceFree:Observable<Boolean>,
        cameraPermissionGranted:Observable<Boolean>
):Observable<Pair<Boolean,SurfaceHolder>>{


    val cameraCanOperateConditionsObservable = combineLatest(
            activityCanDisplayPreview,
            needDataFromCamera.map{cameraDataType ->
                Log.d("xxx_happy", "cameraTypeMap at filter cameraDataType=${cameraDataType.name}, result = ${cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE}")
                cameraDataType==CameraDataType.IMAGE || cameraDataType==CameraDataType.QR_CODE
            },
            cameraPermissionGranted,
            Function3<Boolean, Boolean, Boolean,Boolean> { t1, t2, t3 -> t1 && t2 && t3 }
    )

    val cameraStarStopConditionsObservable = combineLatest(
            cameraCanOperateConditionsObservable,
            isCameraDeviceFree,
            BiFunction<Boolean,Boolean,Pair<Boolean,Boolean>> { t1, t2 ->
                Pair(
                        (t1==true && t2 == true) || (t1==false && t2 == false), //Change state predicate
                        t1                                                      // New state
                )
            }
    )

    return combineLatest(
            cameraStarStopConditionsObservable
                    .filter {it.first}
                    .map { it.second } ,
            surfaceFromActivity,
            BiFunction<Boolean,SurfaceHolder,Pair<Boolean,SurfaceHolder>> { state, surfaceHolder -> Pair(state,surfaceHolder) }
    )
            .distinctUntilChanged{t1,t2->
                t1.first == t2.first
            }



}

fun filterCameraCommand(
        surfaceFromActivity:Observable<SurfaceHolder>,
        activityCanDisplayPreview:Observable<Boolean>,
        needDataFromCamera:Observable<CameraDataType>,
        isCameraDeviceFree:Observable<Boolean>,
        cameraPermissionGranted:Observable<Boolean>,
        needQRCode:Observable<Boolean>,
        needPhoto:Observable<Boolean>
):Observable<Pair<CameraCommand, SurfaceHolder>>{




    val startStop = filterCameraStartStop(
            surfaceFromActivity,
            activityCanDisplayPreview,
            needDataFromCamera,
            isCameraDeviceFree,
            cameraPermissionGranted
    )
            .map {
                val command = if(it.first){
                    CameraCommand.START
                }else{
                    CameraCommand.STOP
                }
                Pair(command,it.second)
            }
            .share()

    val qrCode = needQRCode.withLatestFrom(
            startStop,
            BiFunction<Boolean, Pair<CameraCommand,SurfaceHolder>, Pair<Pair<CameraCommand,SurfaceHolder>,Boolean>> { t1, t2 ->
                Pair(t2, t1)
            })
            .filter{it.first.first==CameraCommand.START}
            .map{
                val command = if(it.second){
                    CameraCommand.QRCODE_START
                }else{
                    CameraCommand.QRCODE_STOP
                }
                Pair(command,it.first.second)
            }
            //.distinctUntilChanged{t1,t2 -> t1.first == t2.first}

    val photo =  needPhoto.withLatestFrom(
            startStop,
            BiFunction< Boolean, Pair<CameraCommand,SurfaceHolder>, Pair<Pair<CameraCommand,SurfaceHolder>,Boolean>> { t1, t2 ->
            Pair(t2, t1)
            })
            .filter{it.first.first==CameraCommand.START}
            .map{
                Pair(CameraCommand.TAKE_PHOTO,it.first.second)
            }

    return merge(startStop,qrCode,photo)

}


fun createProcedureProgress(
        alcotestProgress: Observable<AlcotestProgress>,
        messages: Observable<Message>,
        employees: Observable<Employee>,
        photos: Observable<File>,
        connectionStates: Observable<ConnectionState>,
        activityCanDisplayPreview: Observable<Boolean>
):Observable<ProcedureProgress>{

    var procedureProgress = ProcedureProgress(ProcedureProgressType.UNKNOWN)
    return Observable.create { emitter ->

        fun emitNewValue(procedureProgressNew:ProcedureProgress){
            if(procedureProgressNew!=procedureProgress){
                procedureProgress =   procedureProgressNew
                emitter.onNext(procedureProgressNew)
            }
        }


        alcotestProgress.subscribe{
            procedureProgress.applyAlcotestProgress(it).let{ procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        messages.subscribe{
            procedureProgress.applyMessage(it).let{ procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        employees.subscribe {
            procedureProgress.applyEmployee(it).let{ procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        photos.subscribe {
            procedureProgress.applyPhoto(it).let{ procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        connectionStates.subscribe{
            procedureProgress.applyConnectionState(it).let{procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        activityCanDisplayPreview.subscribe {
            procedureProgress.applyCanDisplayPreview(it).let{procedureProgressNew ->
                emitNewValue(procedureProgressNew)
            }
        }

        emitter.onNext(procedureProgress)

    }



}


fun Observable<String>.chunk(size:Int):Observable<String> = flatMap{ str ->
    Observable.fromIterable(str.chunked(size))
}


enum class ReliableWriteAction{
    WRITE, COMMIT, ABORT
}

enum class WriteDataSource{
    DATA_TO_WRITE, WRITTEN_DATA
}

fun createReliableWriter(
        dataToStartWriteStream:Observable<String>,
        writtenData:Observable<String>
//): Observable<GroupedObservable<ReliableWriteAction, Pair<ReliableWriteAction, String>>> {
): Observable<Pair<ReliableWriteAction, String>> {
    var lastDataToWrite = ""
    return Observable.merge(
            writtenData
                    .doOnNext{
                        println()

                    }
                    .map {
                        if(it==lastDataToWrite) Pair(ReliableWriteAction.COMMIT, "") else Pair(ReliableWriteAction.ABORT,"")
                    },
            dataToStartWriteStream
                    .doOnNext { lastDataToWrite = it }
                    .map {
                        Pair(ReliableWriteAction.WRITE, it)
                    }
    )
}