package ru.nix.alcomobile.DAO

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class MTypeConverter {
    companion object {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale("US"))
        @TypeConverter
        @JvmStatic
        @Synchronized
        fun fromDate(date: Date?):String?{
            return if(date==null){
                null
            }else{
                format.format(date)
            }
        }

        @TypeConverter
        @JvmStatic
        @Synchronized
        fun toDate(value:String?):Date?{
            return if(value==null){
                null
            }else{
                format.parse(value)
            }
        }
    }


}