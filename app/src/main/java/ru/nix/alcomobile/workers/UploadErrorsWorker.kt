package ru.nix.alcomobile.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.coroutineScope
import ru.nix.alcomobile.Repository
import ru.nix.alcomobile.utilites.InjectorUtils

class UploadErrorsWorker (
        context: Context,
        workerParams: WorkerParameters

) : CoroutineWorker(context, workerParams)  {
    private val TAG by lazy { UploadErrorsWorker::class.java.simpleName }
    private val repository = InjectorUtils.provideRepository(context)

    override suspend fun doWork(): Result = coroutineScope {

        try{
            when(repository.startUploadErrors()){
                Repository.ResultForWorker.SUCCESS -> Result.success()
                Repository.ResultForWorker.FAILURE -> Result.failure()
                Repository.ResultForWorker.RETRY -> Result.retry()
            }

        } catch (ex: Exception) {
            Log.e(TAG, "Error uploading data", ex)
            Result.failure()
        }
    }






}
