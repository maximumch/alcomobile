package ru.nix.alcomobile.model

import ru.nix.alcomobile.DAO.ErrorData
import ru.nix.alcomobile.DAO.LogData
import ru.nix.alcomobile.DAO.ResultData


const val ID_ERROR_TIMEOUT = 1
const val ID_ERROR_FLOW = 2
const val ID_ERROR_HARDWARE = 6
const val ID_ERROR_APP = 7

data class ResultRequest(
        val CommandGuid:String,
        val Data: ResultData,
        val Params:List<Any> = ArrayList()
)



data class LogRequest(
        val CommandGuid:String,
        val Data: LogData,
        val Params:List<Any> = ArrayList()
)




data class ErrorRequest(
        val CommandGuid:String,
        val Data: ErrorData,
        val Params:List<Any> = ArrayList()
)



fun ResultData.setUploaded(uploaded: Boolean):ResultData{
    return ResultData(
            photo = photo,
            result = result,
            idc = idc,
            date = date,
            name = name,
            uploaded = uploaded
    )

}


