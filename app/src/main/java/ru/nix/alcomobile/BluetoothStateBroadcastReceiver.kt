package ru.nix.alcomobile

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class BluetoothStateBroadcastReceiver(val callback: ()->Unit) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d("happy_bluetooth", "State changed: ${intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,-1000)}")
        if(intent.action==BluetoothAdapter.ACTION_STATE_CHANGED){
            if(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,-1)==BluetoothAdapter.STATE_OFF){

                callback.invoke()

            }
        }




    }
}
