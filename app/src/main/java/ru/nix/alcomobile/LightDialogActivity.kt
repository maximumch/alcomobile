package ru.nix.alcomobile

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_light_dialog.*
import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.viewmodelsnew.AlcotestViewModel
import kotlin.math.min


private const val MAX_LUMINANCE_VALUE = 2000

class LightDialogActivity : AppCompatActivity(), SensorEventListener {


    private lateinit var viewModel: AlcotestViewModel
    private lateinit var sensorManager: SensorManager
    private var light: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_light_dialog)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        viewModel = provideViewModel()
        seek_bar.max = MAX_LUMINANCE_VALUE

        viewModel.luminanceThreshold.observe(this, Observer {
            luminance_level.text = "$it"
            seek_bar.progress = min(MAX_LUMINANCE_VALUE,it)
        })


        seek_bar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if(fromUser) viewModel.setLuminanceThreshold(progress)

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })




    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    private fun provideViewModel(): AlcotestViewModel {
        val factory = InjectorUtils.provideAlcotestViewModelFactory(this)
        return ViewModelProviders.of(this,factory).get(AlcotestViewModel::class.java)
    }

    override fun onAccuracyChanged(sensor: Sensor?, p1: Int) {

    }

    override fun onSensorChanged(sensor: SensorEvent) {
        val luminance = sensor.values[0].toInt()
        lightbulb.setImageResource(
                if(luminance<viewModel.luminanceThreshold.value?:0) R.drawable.ic_lightbulb_on else R.drawable.ic_lightbulb_off

        )




    }
}
