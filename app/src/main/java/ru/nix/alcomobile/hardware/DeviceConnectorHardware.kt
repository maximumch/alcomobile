package ru.nix.alcomobile.hardware

import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import ru.nix.alcomobile.rx.ReliableDataWriter
import ru.nix.alcomobile.rx.chunk
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap



private const val MAX_DELAY=500L
private const val MAX_ATTEMPTS = 5
private const val DELAY_BEFORE_NEXT_ATTEMPT = 200L
class DeviceConnectorHardware (
context: Context
):DeviceConnector{

    init{
        Log.d("happy","new instance of DeviceConnectorHardware")
    }

    private var mInputObservable: Observable<String> = Observable.empty()
    private var mInputDisposable: Disposable = mInputObservable.subscribe()
    private val mConnectionState: PublishSubject<ConnectionState> = PublishSubject.create()
    private val mOutputData: PublishSubject<String> = PublishSubject.create()
    private val mWrittenData:PublishSubject<String> = PublishSubject.create()
    private val mReliableWriteCompleted:PublishSubject<Int> = PublishSubject.create()
    private val mReliableWriteTimeoutExpired:PublishSubject<Boolean> = PublishSubject.create()


    override var inputData: Observable<String>
        get() = mInputObservable
        set(value) {
            mInputDisposable.dispose()
            mInputDisposable = subscribeOnData(value)

        }

    override val connectionState: Observable<ConnectionState> get() = mConnectionState
    override val outputData: Observable<String> get() = mOutputData


    private val bluetoothAdapter:BluetoothAdapter = (context.applicationContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?)?.adapter?:throw(Exception())
    private val devices = HashMap<String,BluetoothDevice>()
    private val supportedUUID =UUID.fromString(sUUID);
    private val characteristicsUUID=UUID.fromString(cUUID);
    private var mGatt:BluetoothGatt?=null
    private val dataWriteCompletedStream = PublishSubject.create<Boolean>()




    @SuppressLint("CheckResult")
    private fun subscribeOnData(data:Observable<String>):Disposable{
        var lastDataToWrite = ""
        var attemptsCount = 0




        mWrittenData.subscribe {
            Log.d("happyHARD", "writtenData = $it")
            if(it==lastDataToWrite){
                commitReliableWrite()
            }else{
                if(attemptsCount<MAX_ATTEMPTS){
                    Observable.timer(DELAY_BEFORE_NEXT_ATTEMPT, TimeUnit.MILLISECONDS).subscribe {
                        writeDataReliable(lastDataToWrite)
                    }
                }
            }
        }

        mReliableWriteTimeoutExpired.subscribe {
            Log.d("happyHARD", "timeout expired = $it")
        }


        return Observable.zip(
                data.doOnNext {
                    Log.d("happyHARD", "input data = $it")
                }
                        .chunk(12),
                Observable.merge(
                        Observable.just(true),
                        mReliableWriteTimeoutExpired,
                        mReliableWriteCompleted.map { true }.delay(DELAY_BEFORE_NEXT_ATTEMPT, TimeUnit.MILLISECONDS)
                ),
                BiFunction{str:String, _:Boolean -> str}
        )

                .subscribe {
                    Log.d("happyHARD", "dataToStartWrite = $it")
                    lastDataToWrite = it
                    writeDataReliable(it)
                }




//        return Observable.zip(
//                data,
//                Observable.concat(
//                        Observable.just(true),
//                        dataWriteCompletedStream
//                ), BiFunction { data: String, _: Boolean ->
//            data
//        }
//        ).subscribe { dataToWrite ->
//            Log.d("happynew1", "Data to write = $dataToWrite")
//            Observable.zip(
//                    Observable.fromIterable(dataToWrite.chunked(12)),
//                    Observable.interval(300,300, TimeUnit.MILLISECONDS),
//                    BiFunction<String,Long, String> {t1, t2 -> t1}
//            ).subscribe {dataToWriteChunk->
//                Log.d("happynew1", "dataToWriteChunk = $dataToWriteChunk")
//
//                ReliableDataWriter(
//                        dataToWriteChunk,
//                        mWrittenData,
//                        mReliableWriteCompleted,
//                        { s -> writeDataReliable(s) },
//                        { commitReliableWrite() },
//                        { abortReliableWrite() },
//                        5,
//                        1000L
//                ).subscribe(
//                        { dataWriteCompletedStream.onNext(true)
//                            Log.d("happynew1", "reliable write completed")
//                        },
//                        { dataWriteCompletedStream.onNext(false)
//                            Log.d("happynew1", "reliable write aborted") }
//                )
//            }
//
//        }
    }

    private fun writeDataUnreliable(commandString:String){
        val characteristic = mGatt
                ?.getService(supportedUUID)
                ?.getCharacteristic(characteristicsUUID)
        val ba = commandString.toByteArray()
        characteristic?.value = ba// + byteArrayOf(0x0D,0x0A)
        characteristic?.let{
            val result = mGatt?.writeCharacteristic(characteristic)
            Log.d("happy", "Write characteristics result = $result")
        }
    }

    private var d:Disposable?=null

    private fun writeDataReliable(commandString:String){
        Log.d("happyHARD", "Begin write")
        val characteristic = mGatt
                ?.getService(supportedUUID)
                ?.getCharacteristic(characteristicsUUID)
        val ba = commandString.toByteArray()
        characteristic?.value = ba// + byteArrayOf(0x0D,0x0A)
        characteristic?.let{
            mGatt?.beginReliableWrite()
            val result = mGatt?.writeCharacteristic(characteristic)
            Log.d("happyHARD", "Write characteristics result = $result")

        }

        d=Observable.timer(MAX_DELAY, TimeUnit.MILLISECONDS).subscribe {
            mReliableWriteTimeoutExpired.onNext(true)
        }
    }

    private fun commitReliableWrite(){
        val result = mGatt?.executeReliableWrite()
        Log.d("happy", "Commit reliable write result = $result")
    }

    private fun abortReliableWrite(){
        val result = mGatt?.abortReliableWrite()
        Log.d("happy", "Abort reliable write result = $result")
    }

    private val bluetoothGattCallback = object: BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            Log.d("happyConnect","Callback connection state changed = $newState")
            mGatt = null
            when(newState){
                BluetoothProfile.STATE_CONNECTED ->{
                    mGatt = gatt
                    gatt?.discoverServices()
                    mConnectionState.onNext(ConnectionState.CONNECTED)
                }
                BluetoothProfile.STATE_CONNECTING ->{
                    mConnectionState.onNext(ConnectionState.CONNECTING)
                }
                BluetoothProfile.STATE_DISCONNECTING -> {
                    mConnectionState.onNext(ConnectionState.DISCONNECTING)
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    mConnectionState.onNext(ConnectionState.DISCONNECTED)
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            Log.d("happy","onServicesDiscovered = $status")
            if (status == BluetoothGatt.GATT_SUCCESS) {
                val characteristic = gatt
                        ?.getService(supportedUUID)
                        ?.getCharacteristic(characteristicsUUID)
                gatt?.setCharacteristicNotification(characteristic, true)
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            characteristic?.let{
                mOutputData.onNext(it.getStringValue(0))
                Log.d("happynew1",it.getStringValue(0))
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            characteristic?.let{
                Log.d("happyHARD", "Written Data = ${it.getStringValue(0)}")
                mWrittenData.onNext(it.getStringValue(0))
            }
            d?.dispose()
        }

        override fun onReliableWriteCompleted(gatt: BluetoothGatt?, status: Int) {
            Log.d("happyHARD","onReliableWriteCompleted status = $status")
            mReliableWriteCompleted.onNext(status)

        }

    }


    override fun scanDevices():Observable<BTDevice>{
       val d = Observable.create<BluetoothDevice> {subscriber->
                Log.d("happy", "start scan")
                if(!bluetoothAdapter.isEnabled){
                    subscriber.onError(BluetoothDisabledException())
                }else {

                    val scanner = bluetoothAdapter.bluetoothLeScanner
                    val scanCallback = object : ScanCallback() {
                        override fun onScanFailed(errorCode: Int) {
                            subscriber.onError(Throwable("Error BLE scan. Error code = ${errorCode}"))
                        }

                        override fun onScanResult(callbackType: Int, result: ScanResult?) {
                            if (!subscriber.isDisposed) {
                                result?.let {
                                    subscriber.onNext(it.device)
                                }
                            }
                        }

                        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
                            if (!subscriber.isDisposed) {
                                results?.let {
                                    for (btDevice in it) {
                                        subscriber.onNext(btDevice.device)
                                    }
                                }
                            }
                        }
                    }
                    val filter = ScanFilter.Builder()
                            .setServiceUuid(ParcelUuid(UUID.fromString(sUUID)))
                            .build()

                    val scanSettings = ScanSettings.Builder().build()


                    scanner?.startScan(listOf(filter),scanSettings,scanCallback)
                    //scanner?.startScan(scanCallback)

                    subscriber.setCancellable{
                        Log.d("happy", "Unsubscribed: scan")
                        if(bluetoothAdapter.isEnabled) scanner.stopScan(scanCallback)
                    }
                }
       }.doOnSubscribe {
           devices.clear()
       }

        val r = d
                .take(SCAN_TIMEOUT_SECONDS,TimeUnit.SECONDS)
                .share()
                .distinct()
                .doOnNext {device ->
                    devices[device.address] = device
                }.doOnError {
                    Log.d("happyScan","Error $it")
                }
        return r.map{ device -> BTDevice(device.name,device.address) }
    }

    override fun connectDevice(context: Context, address:String) {
            mGatt?.disconnect()
            val device = devices[address]
            devices.remove(address)
            device?.connectGatt(  context,true,bluetoothGattCallback )
            mConnectionState.onNext(ConnectionState.CONNECTING)
    }

    override fun disconnect() {
        mConnectionState.onNext(ConnectionState.DISCONNECTED)
        mGatt?.disconnect()
        mGatt?.close()
        mGatt = null

    }
}

