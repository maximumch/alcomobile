package ru.nix.alcomobile.viewmodelsnew

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import android.content.Context
import android.net.Uri
import android.view.SurfaceHolder
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.nix.alcomobile.Director
import ru.nix.alcomobile.Repository
import ru.nix.alcomobile.audio.VoiceMessenger
import ru.nix.alcomobile.hardware.AlcotestProgressType
import ru.nix.alcomobile.hardware.ConnectionState

class AlcotestViewModel internal constructor(
        private val director: Director,
        private val repository: Repository
): ViewModel() {
    val connectionState:LiveData<ConnectionState> = director.connectionState
    private val voiceMessageObservable = director.voiceMessage
    var voiceMessenger:VoiceMessenger?=null

    //--------------------------------------------------------------------------
    val procedureProgress = director.procedureProgress
    val textMessageType = director.textMessageType
    val vibrate = director.vibrate
    val showPreview = director.showPreview
    val playCameraShutter = director.playCameraShutter
    val result = director.result
    val results get() = director.results()
    val breathProgress = director.breathProgress
    val deviceName = director.deviceName
    val deviceAddress = director.deviceAddress
    var certFileSelected = false
    var certFileUri: Uri? = null
    val certificateError = repository.certificateError
    val certificate = repository.certificate
    val torchMode = repository.torchModeLiveData
    val luminanceThreshold = repository.luminanceThresholdLiveData
    val torchLiveData = repository.torchLiveData
//    val isTestInProgress = when(procedureProgress.value?.alcoTestProgress?.type){
//        null, AlcotestProgressType.READY -> false
//        else -> true
//    }


    fun notifyBluetoothTurnOff() = director.notifyBluetoothTurnOff()
    fun torchOn() = director.torchOn()
    fun torchOff() = director.torchOff()
    fun stop() = director.stop()
    fun startTest() = director.startTest()
    fun startInfo() = director.startInfo()

    val debugTextLiveData = director.debugTextLiveData







    fun initVoiceMessenger(context:Context){
        if(voiceMessenger==null || voiceMessenger?.isDisposed == true) {
            viewModelScope.launch {
                voiceMessenger = VoiceMessenger.getInstance(context)
                voiceMessenger?.setInputStream(voiceMessageObservable)
            }
        }
    }

    fun disposeVoiceMessenger(){
        voiceMessenger?.dispose()
        voiceMessenger = null
    }



    fun startDevicesScan()=director.scanDevices()
    //fun startTest() = director.startTest()

    fun onSurfaceHolderCreated(surfaceHolder:SurfaceHolder) =
            director.surfaceFromActivityObserver.onNext(surfaceHolder)
    fun activityCanDisplayPreviewChanged(canDisplay:Boolean) =
            director.activityCanDisplayPreviewObserver.onNext(canDisplay)
    fun reconnect(context: Context) = director.reconnect(context)
    fun disconnect() = director.disconnect()
    fun addCertificate(context: Context, fileUri: Uri, password: String) {
        viewModelScope.launch { repository.addCertificate(context,fileUri,password) }
    }
    fun removeCertificate(context: Context){
        viewModelScope.launch { repository.removeCertificate(context) }
    }

    fun changeTorchMode() = repository.changeTorchMode()
    fun setLuminanceThreshold(value:Int) {   repository.luminanceThreshold = value   }
    fun setLuminance(value:Int){repository.luminance = value}



    fun testLed(){


    }

}