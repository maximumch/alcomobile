package ru.nix.alcomobile.DAO

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "log")
data class LogData(
        val msg:String,
        val date:Long,
        val uploaded:Boolean?=false,
        @PrimaryKey val id:String = UUID.randomUUID().toString()
)

fun LogData.setUploaded() = LogData(
        msg = msg,
        date = date,
        uploaded = true,
        id = id
)