package ru.nix.alcomobile.utilites

import android.content.Context
import android.net.Uri
import java.io.File
import java.io.FileInputStream
import java.lang.IllegalArgumentException

    fun uriToFileInputStream(context: Context, fileUri: Uri):FileInputStream{
        context.contentResolver?.openFileDescriptor(fileUri, "r")?.fileDescriptor.let{
            return FileInputStream(it)
        }
        throw IllegalArgumentException("Can not get file descriptor by uri")
    }

    fun replaceOnlyFileInDir(dirDst: File, fileName: String, fileInputStream: FileInputStream) {
        // Удалаяем все файлы из директории
        deleteAllFileInDir(dirDst)
        //Создаем новый файл
        val file = File(dirDst, fileName)
        file.createNewFile()
        //Копируем содержимое из потока в созданный новый файл
        fileInputStream.channel.use{ sourceChannel ->
            file.outputStream().channel.use{ destinationChannel ->
                destinationChannel.transferFrom(sourceChannel, 0, sourceChannel.size())
            }
        }
    }

    fun deleteAllFileInDir(dir: File) {
        for(file in dir.listFiles()) {
            file.delete()
        }
    }

