package ru.nix.alcomobile


import android.content.*
import android.net.Uri
import android.preference.PreferenceManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.*
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.RequestBody
import ru.nix.alcomobile.DAO.*
import ru.nix.alcomobile.api.RESPONSE_STATUS_ABSENT
import ru.nix.alcomobile.api.RESPONSE_STATUS_COMPLETE
import ru.nix.alcomobile.api.UploaderApi
import ru.nix.alcomobile.model.ErrorRequest
import ru.nix.alcomobile.model.LogRequest
import ru.nix.alcomobile.model.ResultRequest
import ru.nix.alcomobile.utilites.*
import ru.nix.alcomobile.workers.UploadErrorsWorker
import ru.nix.alcomobile.workers.UploadLogsWorker
import ru.nix.alcomobile.workers.UploadResultsWorker
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

import javax.net.ssl.X509TrustManager
import kotlin.Exception

private const val KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS = "key_last_connected_aloctester_address"
private const val KEY_LAST_CONNECTED_ALCOTESTER_NAME = "key_last_connected_aloctester_name"

private const val DIR_FOR_CERTIFICATE = "certDir"
private const val FILE_FOR_CERTIFICATE = "certFile"
private const val KEY_CERTIFICATE_NAME = "key_certificate_name"
private const val KEY_CERTIFICATE_PASSWORD= "key_certificate_password"
private const val KEY_TORCH_MODE = "key_torch_mode"
private const val KEY_LUMINANCE_THRESHOLD = "key_luminance_threshold"

private const val ACTION_FILE = "ru.nix.replicator.action.FILE"
private const val ACTION_RECORD = "ru.nix.replicator.action.RECORD"

private const val EXTRA_FILE_URI = "ru.nix.replicator.extra.file_uri"
private const val EXTRA_FILE_CLOUD_NAME = "ru.nix.replicator.extra.file_cloud_name"
private const val EXTRA_SOURCE_APP= "ru.nix.replicator.extra.source_app"
private const val EXTRA_RECORD_REQUEST_DATA = "ru.nix.replicator.extra.record_request_data"
private const val EXTRA_RECORD_REQUEST_TYPE = "ru.nix.replicator.extra.record_request_type"
private const val EXTRA_RECORD_REQUEST_ID = "ru.nix.replicator.extra.record_request_id"
private const val RESULT_COMMAND_GUID = "0B0145FF-8EB5-4A23-9F06-4ED739081486"
private const val LOG_COMMAND_GUID = "349F87E5-8160-4318-9327-E911E71389C8"
private const val ERROR_COMAND_GUID = "F58CE482-863A-4136-B545-492F9C101F61"

private const val BUFFER_SIZE = 128*1024

class RepositoryImplNoReplicator private constructor(
        context: Context,
        private val errorDataDAO: ErrorDataDAO,
        private val logDataDAO: LogDataDAO,
        private val resultDataDAO: ResultDataDAO
        ):Repository {

    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private lateinit var uploaderApi: UploaderApi
    private val dirForPictures = InjectorUtils.dirForPictures
    private val luminanceBelowThresholdPublishSubject = PublishSubject.create<Boolean>()
    private val torchModePublishSubject = PublishSubject.create<TorchMode>()

    override val torchLiveData = MutableLiveData<Boolean>()
    override val results: LiveData<List<ResultData>> = resultDataDAO.getAllLiveData()
    override val certificateError: SingleLiveEvent<String?> = SingleLiveEvent()
    override val certificate: MutableLiveData<String> = MutableLiveData()
    override val torchModeLiveData: MutableLiveData<TorchMode> = MutableLiveData()
    private var torchMode:TorchMode = TorchMode.OFF
    set(value) {
        field=value
        torchModeLiveData.postValue(value)
        torchModePublishSubject.onNext(value)
        nextTorchValue()
    }

    override val luminanceThresholdLiveData: MutableLiveData<Int> = MutableLiveData()
    override var luminanceThreshold = 0
    set(value){
        field = value
        sharedPreferences.edit().putInt(KEY_LUMINANCE_THRESHOLD,value).apply()
        luminanceThresholdLiveData.postValue(value)
        nextTorchValue()
    }
    override var luminance: Int = 0
        set(value) {
            field = value
            nextTorchValue()
        }

    init {
        certificate.postValue(sharedPreferences.getString(KEY_CERTIFICATE_NAME, null))
        torchMode = sharedPreferences.getString(KEY_TORCH_MODE, null)?.let {
                    TorchMode.valueOf(it)
                } ?: TorchMode.AUTO
        luminanceThreshold = sharedPreferences.getInt(KEY_LUMINANCE_THRESHOLD, 0)
        updateUploaderApi(context)
    }

    override val torchObservable: Observable<Boolean>
    //get() = luminanceBelowThresholdPublishSubject.distinctUntilChanged()

        get() = Observable.merge(
                Observable.just(luminance<luminanceThreshold),
                luminanceBelowThresholdPublishSubject.distinctUntilChanged()
        )
    override val luminanceBelowThresholdObservable: Observable<Boolean>
        get() = Observable.merge(
                Observable.just(luminance<luminanceThreshold),
                luminanceBelowThresholdPublishSubject.distinctUntilChanged()
        )
    override val torchModeObservable: Observable<TorchMode>
        get() = Observable.merge(
                Observable.just(torchMode),
                torchModePublishSubject.distinctUntilChanged()
        )

    override fun uploadFile(file: File){

    }

    override fun uploadResult(photo:File, result: Double, idc:Int, name:String){
        val data = ResultData(photo.name, result,idc, System.currentTimeMillis(),name)
        resultDataDAO.insert(data)
        enqueueUploadResultsWorker()
    }

    override fun uploadLog(msg:String){
        val data = LogData(msg, System.currentTimeMillis())
        logDataDAO.insert(data)
        enqueueUploadLogsWorker()
    }

    override fun uploadError(idc:Int, errorId:Int){
        val data = ErrorData(errorId,idc, System.currentTimeMillis())
        errorDataDAO.insert(data)
        enqueueUploadErrorsWorker()
    }

    override fun changeTorchMode() {
        val newMode = when(torchMode){
            TorchMode.AUTO -> TorchMode.OFF
            TorchMode.OFF -> TorchMode.AUTO
        }
        sharedPreferences.edit().putString(KEY_TORCH_MODE, newMode.toString()).apply()
        torchMode = newMode
    }



    private fun enqueueUploadResultsWorker() {
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        val request = OneTimeWorkRequestBuilder<UploadResultsWorker>()
                .setConstraints(constraints)
                .build()
        WorkManager.getInstance().enqueueUniqueWork("uploadResults", ExistingWorkPolicy.KEEP, request)
    }

    private fun enqueueUploadLogsWorker() {
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        val request = OneTimeWorkRequestBuilder<UploadLogsWorker>()
                .setConstraints(constraints)
                .build()
        WorkManager.getInstance().enqueueUniqueWork("uploadLogs", ExistingWorkPolicy.KEEP, request)
    }

    private fun enqueueUploadErrorsWorker() {
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        val request = OneTimeWorkRequestBuilder<UploadErrorsWorker>()
                .setConstraints(constraints)
                .build()
        WorkManager.getInstance().enqueueUniqueWork("uploadErrors", ExistingWorkPolicy.KEEP, request)
    }

    override suspend fun addCertificate(context: Context, fileUri: Uri, password: String) {
        try {
            val fileName = fileUri.lastPathSegment?.substringAfterLast("/")
            if (fileName != null) {
                context.contentResolver?.openFileDescriptor(fileUri, "r")?.use {
                    FileInputStream(it.fileDescriptor).use { inputStream ->
                        //Проверяем сертификат и пароль
                        if (checkCertificate(inputStream, password)) {

                            uriToFileInputStream(context, fileUri).use {
                                // Проверка прошла успешно -> Копируем файл сертификата в локальное хранилище
                                replaceOnlyFileInDir(
                                        context.getDir(DIR_FOR_CERTIFICATE, 0),
                                        FILE_FOR_CERTIFICATE,
                                        it
                                )
                                //Сохраняем название сертификата в SharedPreferences
                                updateCertificateRecord(fileName, password)
                                //Обновляем объект серверного API
                                updateUploaderApi(context)
                                enqueueUploadErrorsWorker()
                                enqueueUploadLogsWorker()
                                enqueueUploadResultsWorker()
                            }
                        }
                    }
                }
            }
        }catch(e: Exception){
            certificateError.postValue("Error. " + e.message)
        }


    }

    override suspend fun removeCertificate(context: Context){
        deleteAllFileInDir(context.getDir(DIR_FOR_CERTIFICATE,0))
        clearCertificateRecord()
    }

    override var lastConnectedAlcotesterAddress:String?
        get(){
            return sharedPreferences.getString(KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS,null)
        }
        set(value) {
            sharedPreferences.edit().putString(KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS,value).apply()
        }

    override var lastConnectedAlcotesterName:String?
        get(){
            return sharedPreferences.getString(KEY_LAST_CONNECTED_ALCOTESTER_NAME,null)
        }
        set(value) {
            sharedPreferences.edit().putString(KEY_LAST_CONNECTED_ALCOTESTER_NAME,value).apply()
        }

    private fun checkCertificate(inputStream: InputStream, password:String) =
            try{
                SslUtils.getSslContext(inputStream,password)
                true
            }catch (e:Exception){
                certificateError.postValue(e.message)
                false
            }

    private fun clearCertificateRecord(){
        sharedPreferences.edit().remove(KEY_CERTIFICATE_NAME).apply()
        certificate.postValue(null)
    }



    private fun updateCertificateRecord(fileName: String, password:String) {
        sharedPreferences.edit()
                .putString(KEY_CERTIFICATE_NAME, fileName)
                .putString(KEY_CERTIFICATE_PASSWORD, password)
                .apply()
        certificate.postValue(fileName)
    }

    private fun getCertificateInputStream(context: Context):InputStream?{
        sharedPreferences.getString(KEY_CERTIFICATE_NAME,null).let{filename ->
            File(context.getDir(DIR_FOR_CERTIFICATE, 0), FILE_FOR_CERTIFICATE).let{file->
                if(file.exists()){
                    return file.inputStream()
                }
            }
        }
        return null
    }

    private fun getCertificatePassword() = sharedPreferences.getString(KEY_CERTIFICATE_PASSWORD,null)

    internal fun updateUploaderApi(context: Context){
        val certificateStream = getCertificateInputStream(context)
        val certificatePassword = getCertificatePassword()
        if(certificateStream!=null && certificatePassword!=null){
            val sslContext = SslUtils.getSslContext(certificateStream,certificatePassword)
            val x509TrustManager = SslUtils.trustAllCerts[0] as X509TrustManager
            uploaderApi = UploaderApi.create(sslContext,x509TrustManager)
        }
    }

    override suspend fun startUploadResults(): Repository.ResultForWorker {
        var resultForWorker  = Repository.ResultForWorker.SUCCESS
        val results = resultDataDAO.getUnuploaded()
        for(result in results){
            try {
                uploadFile(result.photo, File(dirForPictures, result.photo))
                uploadResult(result)
                resultDataDAO.update(result.setUploaded())
            }catch (e:Exception){
                resultForWorker = resultForWorker.and(
                        when{
                            e is UninitializedPropertyAccessException -> Repository.ResultForWorker.FAILURE
                            else -> Repository.ResultForWorker.RETRY
                        }
                )
            }
        }
        return resultForWorker
    }

    suspend fun uploadFile(fileName:String, file:File){
        var response: UploaderApi.MResponse = uploaderApi.statusAsync(fileName).await().body()
                ?: throw IllegalStateException("uploaderApi.statusAsync with null body")
        var offset:Int = if (response.status == RESPONSE_STATUS_ABSENT) {
            uploaderApi.uploadDescriptorAsync(fileName, file.length().toInt()).await().body()?.size?: throw IllegalStateException("uploaderApi.uploadDescriptorAsync with null body")
        } else {
            response.size?: throw IllegalStateException("uploaderApi.statusAsync with null size")
        }

        var bytesRead:Int=0

        val inputStream = FileInputStream(file).apply {
            if (offset > 0) skip(offset.toLong())
        }

        inputStream.use {
            val buffer = ByteArray(BUFFER_SIZE)

            while (it.read(buffer).also { bytesRead = it } != -1) {
                val requestBody =
                        RequestBody.create(MediaType.parse("application/octet-stream"), buffer, 0, bytesRead)
                response = uploaderApi.uploadPartAsync(fileName, offset, bytesRead, requestBody).await().body()
                        ?: throw java.lang.IllegalStateException("uploaderApi.uploadPartAsync return null body")
                offset += bytesRead
            }
            check(response.status == RESPONSE_STATUS_COMPLETE){"Last part upload call mast return status COMPLETE"}
        }
    }

    suspend fun uploadResult(result:ResultData){
        val request = ResultRequest(RESULT_COMMAND_GUID,result)
        val gson = Gson()
        val str = gson.toJson(request)
        val success = uploaderApi.processRecordAsync(str).await().isSuccessful
        check(success){ "uploaderApi.processRecordAsync is not successful" }
    }

    override suspend fun startUploadLogs(): Repository.ResultForWorker {
        var resultForWorker  = Repository.ResultForWorker.SUCCESS
        val logs = logDataDAO.getUnuploaded()
        for(log in logs){
            try {
                uploadLog(log)
                logDataDAO.update(log.setUploaded())
            }catch (e:Exception){
                resultForWorker = resultForWorker.and(
                        when{
                            e is UninitializedPropertyAccessException -> Repository.ResultForWorker.FAILURE
                            else -> Repository.ResultForWorker.RETRY
                        }
                )
            }
        }

        return resultForWorker
    }

    private suspend fun uploadLog(log:LogData){
        val request = LogRequest(LOG_COMMAND_GUID,log)
        val gson = Gson()
        val str = gson.toJson(request)
        val success = uploaderApi.processRecordAsync(str).await().isSuccessful
        check(success){ "uploaderApi.processRecordAsync is not successful" }
    }

    override suspend fun startUploadErrors(): Repository.ResultForWorker {
        var resultForWorker  = Repository.ResultForWorker.SUCCESS
        val errors = errorDataDAO.getUnuploaded()
        for(error in errors){
            try{
                uploadError(error)
                errorDataDAO.update(error.setUploaded())
            }catch (e:Exception){
                resultForWorker = resultForWorker.and(
                        when{
                            e is UninitializedPropertyAccessException -> Repository.ResultForWorker.FAILURE
                            else -> Repository.ResultForWorker.RETRY
                        }
                )
            }
        }
        return resultForWorker
    }

    private suspend fun uploadError(error: ErrorData){
        val request = ErrorRequest(ERROR_COMAND_GUID,error)
        val gson = Gson()
        val str = gson.toJson(request)
        val success = uploaderApi.processRecordAsync(str).await().isSuccessful
        check(success){ "uploaderApi.processRecordAsync is not successful" }
    }

    private fun nextTorchValue(){
        torchLiveData.postValue(torchMode==TorchMode.AUTO && luminance<luminanceThreshold)
        luminanceBelowThresholdPublishSubject.onNext(luminance<luminanceThreshold)
    }




    companion object  {
        @Volatile private var instance: RepositoryImplNoReplicator?=null

        fun getInstance(
                context: Context,
                errorDataDAO: ErrorDataDAO,
                logDataDAO: LogDataDAO,
                resultDataDAO: ResultDataDAO
                ) =
                instance ?: synchronized(this) {
                    instance ?: RepositoryImplNoReplicator(context,errorDataDAO,logDataDAO,resultDataDAO).also {
                        instance = it

                    }
                }
    }


}



fun Repository.ResultForWorker.and(another: Repository.ResultForWorker): Repository.ResultForWorker {
    return when{
        this== Repository.ResultForWorker.RETRY || another == Repository.ResultForWorker.RETRY -> Repository.ResultForWorker.RETRY
        this== Repository.ResultForWorker.FAILURE || another == Repository.ResultForWorker.FAILURE -> Repository.ResultForWorker.FAILURE
        else -> Repository.ResultForWorker.SUCCESS
    }
}
