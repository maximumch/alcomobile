package ru.nix.alcomobile.utilites

import android.content.Context
import android.os.Environment
import android.util.Log
import io.reactivex.Observable
import ru.nix.alcomobile.DAO.AppDatabase
import ru.nix.alcomobile.Director
import ru.nix.alcomobile.Repository
import ru.nix.alcomobile.RepositoryImplNoReplicator
import ru.nix.alcomobile.camera.CameraHelper
import ru.nix.alcomobile.camera.CameraOldHelper
import ru.nix.alcomobile.hardware.*
import ru.nix.alcomobile.viewmodelsnew.AlcotestViewModelFactory
import ru.nix.alcomobile.viewmodelsnew.DeviceSettingsViewModelFactory
import ru.nix.alcomobile.viewmodels.DevicesScanViewModelFactory
import java.io.File

object InjectorUtils {
    val dirForPictures: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
    val fakeAlcotester = false
    @Volatile private var deviceConnector: DeviceConnector?=null


    fun provideDeviceConnector(context: Context): DeviceConnector =
            deviceConnector ?: synchronized(this){
                deviceConnector ?: DeviceConnectorHardware(context).also{
                    deviceConnector = it
                }
            }

    fun provideAlcotestViewModelFactory(
            context: Context
    ): AlcotestViewModelFactory {
        //val cameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        Log.d("happyStart", "onCreate 3")
        val director = Director.getInstance(context.applicationContext)
        val repository = provideRepository(context)
        return AlcotestViewModelFactory(director,repository)
    }

    fun provideRepository(context: Context): Repository {
        Log.d("happyStart", "onCreate 4")
        val database = AppDatabase.getInstance(context.applicationContext)
        //val r = RepositoryImplWithReplicator.getInstance(context)
        val r = RepositoryImplNoReplicator.getInstance(
                context,
                database.errorDataDAO(),
                database.logDataDAO(),
                database.resultDataDAO())

        return r
    }

    fun provideCameraHelper(context: Context): CameraHelper {

            return CameraOldHelper(context)

    }

    fun provideAlcotesterConnector(context: Context, commandsObservable: Observable<Command>):AlcotesterConnector{
        if(fakeAlcotester){
            return AlcotesterConnectorFakeOld(null,null)
        }else {
            return AlcotesterConnectorHardware(context, commandsObservable)
        }
    }

    fun provideDeviceScanViewModelFactory(context: Context):DevicesScanViewModelFactory{
        val director = Director.getInstance(context.applicationContext)
        return DevicesScanViewModelFactory(director)

    }

    fun provideDeviceSettingsViewModelFactory(context: Context): DeviceSettingsViewModelFactory {
        val director = Director.getInstance(context.applicationContext)
        return DeviceSettingsViewModelFactory(director)

    }



}