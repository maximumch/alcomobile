package ru.nix.alcomobile.hardware

import android.bluetooth.BluetoothAdapter
import android.content.Context
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.collections.ArrayList





class AlcotesterConnectorFakeOld(
        private val bluetoothAdapter: BluetoothAdapter?,
        private val commandsObservable:Observable<Command>?
):AlcotesterConnector {

    private  val fixedAddress = byteArrayOf(0x1F,0x0F,0x0F,0x0F,0x0F,0x0F )


    override fun scanDevices(): Observable<BTDevice> {
        val minDevicesCount = 3
        val maxDevicesCount = 10
        val devices = ArrayList<BTDevice>()

        val devicesCount = minDevicesCount+Random().nextInt(maxDevicesCount - minDevicesCount+1)
        devices.clear()
        for(i in (1..devicesCount)){
            devices.add(BTDevice("Device-$i", createRandomAddress()))
        }

        devices.add(BTDevice("Device-fixed", fixedAddress.toHexString()))

        val oBase = Observable.fromIterable(devices)
        val oTimer = Observable.intervalRange(0L,maxDevicesCount.toLong(),1L,1L, TimeUnit.SECONDS)
        return Observable.zip(oBase,oTimer, BiFunction { t1, t2 -> t1  })
    }

    override fun connectDevice(context: Context, address: String): Observable<Pair<ConnectionStateOld, Observable<String>>> {

        return Observable.just(Pair(ConnectionStateOld.CONNECTED,
                Observable.just(""))

        )


    }

    fun makeTestOnDemand(commandsObservable:Observable<Command>):Observable<String>{
        return commandsObservable
                .filter { command -> command.commandType == CommandType.START }
                .concatMap { command -> makeTest() }
    }

    fun postTestOnDemand2(commandsObservable:Observable<Command>):Observable<Observable<String>>{
        return commandsObservable
                .filter { command -> command.commandType == CommandType.START }
                .map { command -> makeTest() }
    }

    fun postTestOnDemand(commandsObservable:Observable<Command>):Observable<String>{
        return Observable.create{subscriber->
            val stateTest = AtomicBoolean(false)
            val periodObservable = Observable.interval(1000, TimeUnit.MILLISECONDS)
            periodObservable.subscribe{_-> if(!stateTest.get()) subscriber.onNext("\$END")}
            commandsObservable.subscribe{command->
                if(command.commandType==CommandType.START) {
                    if(stateTest.compareAndSet(false,true)){
                        makeTest().subscribe(
                                {str ->  subscriber.onNext(str) },
                                {},
                                {stateTest.set(false)}
                        )
                    }
                }
            }








        }


    }




    fun makeTest():Observable<String>{
        val rnd = Random().nextInt(10)
        val result = if(rnd>4){
            1.0/rnd
        }else{
            0.0
        }

        val resultString = DecimalFormat().run {
            minimumIntegerDigits = 1
            maximumFractionDigits = 2
            isGroupingUsed = false
            format(result)
        }

        val events = arrayListOf("\$END","\$WAIT","\$WAIT","\$STANBY", "\$TRIGGER","\$BREATH", "\$RESULT,$resultString-OK" )

        val timerObservable = Observable.intervalRange(0L,events.size.toLong(),1L,1L, TimeUnit.SECONDS)
        val oBase = Observable.fromIterable(events)
        return Observable.zip(timerObservable, oBase, BiFunction { t1, t2 -> t2 })
    }


    private fun createRandomAddress():String{
        val r= Random()
        val ba = ByteArray(6)
        r.nextBytes(ba)
        return ba.toHexString()

    }

    private fun ByteArray.toHexString()=this.joinToString(":"){ String.format("%02X",(it.toInt() and 0xFF)) }

    override fun disconnect() {

    }

}