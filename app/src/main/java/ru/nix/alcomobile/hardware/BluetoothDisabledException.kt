package ru.nix.alcomobile.hardware

class BluetoothDisabledException: Throwable("Bluetooth disabled")