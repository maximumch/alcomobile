package ru.nix.alcomobile.hardware

enum class ConnectionState(){
    DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING, SEARCHING
}