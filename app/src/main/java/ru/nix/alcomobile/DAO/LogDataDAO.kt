package ru.nix.alcomobile.DAO

import androidx.room.*

@Dao
interface LogDataDAO {
    @Query("SELECT * FROM log ORDER BY date desc")
    fun getAll():List<LogData>

    @Query("SELECT * FROM log WHERE uploaded = 0 ORDER BY date desc")
    fun getUnuploaded():List<LogData>

    @Insert
    fun insert(value:LogData)

    @Update
    fun update(value:LogData)

    @Delete
    fun delete(value:LogData)


}