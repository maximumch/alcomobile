package ru.nix.alcomobile.model

import android.util.Log
import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.*
import java.io.File
import kotlin.IllegalArgumentException

enum class ProcedureProgressType{
    UNKNOWN, INITIAL_WAIT_FOR_CONTACT, WAIT_FOR_CONTACT, READY, WAIT_FOR_EMPLOYEE, WAIT_FOR_ALCOTEST, RESULT, ERROR_TIMEOUT,ERROR_HARDWARE, ERROR_APP, ERROR_CONTACTS, WAIT_AFTER_CONTACTS_ERROR
}

data class ProcedureProgress(
        val type:ProcedureProgressType = ProcedureProgressType.UNKNOWN,
        val alcoTestProgress:AlcotestProgress=AlcotestProgress(),
        val employee: Employee?=null,
        val photo:File? = null,
        val result:Double?=null,
        val contactState:Map<Int,Int> = HashMap(),
        val canDisplayPreview: Boolean = true

){
    init{
        Log.d("happyStrProgress","$this")
    }
}

fun ProcedureProgress.setType(newType:ProcedureProgressType):ProcedureProgress{
    return ProcedureProgress(newType,alcoTestProgress,employee,photo,result,contactState,canDisplayPreview)
}

fun ProcedureProgress.clearProgressAndSetType(newType:ProcedureProgressType):ProcedureProgress{
    return ProcedureProgress(type=newType, contactState = contactState,canDisplayPreview = canDisplayPreview)
}

fun ProcedureProgress.setAlcotestProgress(newAlcotestProgress: AlcotestProgress):ProcedureProgress{
    return ProcedureProgress(type,newAlcotestProgress,employee,photo,result,contactState,canDisplayPreview)
}

fun ProcedureProgress.setEmployee(newEmployee: Employee):ProcedureProgress{
    return ProcedureProgress(type,alcoTestProgress,newEmployee,photo,result,contactState,canDisplayPreview)
}

fun ProcedureProgress.setPhoto(newPhoto: File):ProcedureProgress{
    return ProcedureProgress(type,alcoTestProgress,employee,newPhoto,result,contactState,canDisplayPreview)
}

fun ProcedureProgress.setResult(newResult: Double):ProcedureProgress{
    return ProcedureProgress(type,alcoTestProgress,employee,photo,newResult,contactState,canDisplayPreview)
}

fun ProcedureProgress.setContactState(key:Int,state:Int):ProcedureProgress{
    val newContactState =  HashMap(contactState)
    newContactState[key] = state
    return ProcedureProgress(type,alcoTestProgress,employee,photo,result,newContactState,canDisplayPreview)
}

fun ProcedureProgress.setCanDisplayPreview(newCanDisplayPreview: Boolean):ProcedureProgress{
    return ProcedureProgress(type,alcoTestProgress,employee,photo,result,contactState,newCanDisplayPreview)
}

fun ProcedureProgress.isContactsOk():Boolean = (contactState[1]?:0==1) && (contactState[2]?:0==1)







fun ProcedureProgress.applyMessage(message:Message):ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyMessage $message $this")
    return when(message.messageType){
        MessageType.RED_BUTTON_PRESSED -> {
            if(type==ProcedureProgressType.READY && canDisplayPreview) this.setType(ProcedureProgressType.WAIT_FOR_EMPLOYEE) else this}
        MessageType.CONTACT1_STATE -> {
            val state = message.params[KEY_CONTACT_STATE]?.toInt()?:throw IllegalArgumentException("Message CONTACT1_STATE with NULL contact state")
            setContactState(1,state).run {
                when(isContactsOk()){
                    true -> {
                        when (type) {
                            ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT -> setType(ProcedureProgressType.READY)
                            ProcedureProgressType.WAIT_FOR_CONTACT -> {
                                when(alcoTestProgress?.type){
                                    AlcotestProgressType.READY -> setType(ProcedureProgressType.READY)
                                    AlcotestProgressType.SAMPLE_PROCESSING -> setType(ProcedureProgressType.WAIT_FOR_ALCOTEST)
                                    AlcotestProgressType.RESULT -> setType(ProcedureProgressType.RESULT)
                                    else -> setType(ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR)
                                }
                            }
                            else -> setType(ProcedureProgressType.UNKNOWN)
                        }
                    }
                    false -> {
                        when (type) {
                            ProcedureProgressType.WAIT_FOR_EMPLOYEE -> setType(ProcedureProgressType.ERROR_CONTACTS)
                            ProcedureProgressType.WAIT_FOR_ALCOTEST -> setType(ProcedureProgressType.ERROR_CONTACTS)
                            ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT -> this
                            else -> setType(ProcedureProgressType.WAIT_FOR_CONTACT)
                        }
                    }
                }

//                if(isContactsOk() && (type==ProcedureProgressType.WAIT_FOR_CONTACT ||  type==ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT)) {
//                    setType(ProcedureProgressType.READY)
//                }else if(!isContactsOk() && (type==ProcedureProgressType.READY || type==ProcedureProgressType.RESULT)) {
//                    setType(ProcedureProgressType.WAIT_FOR_CONTACT)
//                }else if(!isContactsOk() && (type==ProcedureProgressType.WAIT_FOR_EMPLOYEE || type==ProcedureProgressType.WAIT_FOR_ALCOTEST)){
//                    setType(ProcedureProgressType.ERROR_CONTACTS)
//                }else{
//                    this
//                }
            }
        }
        MessageType.CONTACT2_STATE -> {
            val state = message.params[KEY_CONTACT_STATE]?.toInt()?:throw IllegalArgumentException("Message CONTACT1_STATE with NULL contact state")
            setContactState(2,state).run {
                when(isContactsOk()){
                    true -> {
                        when (type) {
                            ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT -> setType(ProcedureProgressType.READY)
                            ProcedureProgressType.WAIT_FOR_CONTACT -> {
                                when(alcoTestProgress?.type){
                                    AlcotestProgressType.READY -> setType(ProcedureProgressType.READY)
                                    AlcotestProgressType.SAMPLE_PROCESSING -> setType(ProcedureProgressType.WAIT_FOR_ALCOTEST)
                                    AlcotestProgressType.RESULT -> setType(ProcedureProgressType.RESULT)
                                    else -> setType(ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR)
                                }
                            }
                            else -> setType(ProcedureProgressType.UNKNOWN)
                        }
                    }
                    false -> {
                        when (type) {
                            ProcedureProgressType.WAIT_FOR_EMPLOYEE ->setType(ProcedureProgressType.ERROR_CONTACTS)
                            ProcedureProgressType.WAIT_FOR_ALCOTEST -> setType(ProcedureProgressType.ERROR_CONTACTS)
                            ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT -> this
                            else -> setType(ProcedureProgressType.WAIT_FOR_CONTACT)
                        }
                    }
                }
//                if(isContactsOk() && (type==ProcedureProgressType.WAIT_FOR_CONTACT  ||  type==ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT)){
//                    setType(ProcedureProgressType.READY)
//                }else if(!isContactsOk() && (type==ProcedureProgressType.READY || type==ProcedureProgressType.RESULT)) {
//                    setType(ProcedureProgressType.WAIT_FOR_CONTACT)
//                }else if(!isContactsOk() && (type==ProcedureProgressType.WAIT_FOR_EMPLOYEE || type==ProcedureProgressType.WAIT_FOR_ALCOTEST)){
//                    setType(ProcedureProgressType.ERROR_CONTACTS)
//                }else{
//                    this
//                }
            }
        }
        else -> this
    }
}

fun ProcedureProgress.applyAlcotestProgress(alcoTestProgress:AlcotestProgress):ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyAlcotestProgress $alcoTestProgress $this")

    return (
            when(type){
        ProcedureProgressType.UNKNOWN -> {
            when(alcoTestProgress.type){
                AlcotestProgressType.READY -> if(isContactsOk()) {
                    this.setType(ProcedureProgressType.READY)
                }else{
                    this.setType(ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT)
                }
                else -> this
            }
        }

        ProcedureProgressType.INITIAL_WAIT_FOR_CONTACT -> {
            when(alcoTestProgress.type){
                AlcotestProgressType.READY -> if(isContactsOk()) {
                    this.setType(ProcedureProgressType.READY)
                }else{
                    this
                }
                else -> this
            }
        }

        ProcedureProgressType.READY -> {
            when(alcoTestProgress.type){
                AlcotestProgressType.READY -> this
                else ->  this.clearProgressAndSetType(ProcedureProgressType.UNKNOWN)
            }
        }

        ProcedureProgressType.WAIT_FOR_EMPLOYEE ->{
            when(alcoTestProgress.type){
                AlcotestProgressType.READY -> this
                else -> this.clearProgressAndSetType(ProcedureProgressType.UNKNOWN)
            }
        }

        ProcedureProgressType.WAIT_FOR_ALCOTEST ->{
            when(alcoTestProgress.type){
                AlcotestProgressType.RESULT ->
                    this
                            .setType(ProcedureProgressType.RESULT)
                            //.setAlcotestProgress(alcoTestProgress)
                            .setResult((alcoTestProgress.param?:throw IllegalArgumentException("Result is null")).toDouble())


                AlcotestProgressType.TIMEOUT -> this.clearProgressAndSetType(ProcedureProgressType.ERROR_TIMEOUT)
                AlcotestProgressType.HARDWARE_ERROR -> this.setType(ProcedureProgressType.ERROR_HARDWARE)
                else -> this.setType(ProcedureProgressType.WAIT_FOR_ALCOTEST)
            }
        }

        ProcedureProgressType.RESULT ->{
            when(alcoTestProgress.type){
                AlcotestProgressType.READY,
                AlcotestProgressType.END_AFTER_RESULT -> this.clearProgressAndSetType(ProcedureProgressType.READY)
                else -> this
            }
        }

        ProcedureProgressType.ERROR_TIMEOUT,
        ProcedureProgressType.ERROR_HARDWARE,
        ProcedureProgressType.ERROR_APP,
        ProcedureProgressType.ERROR_CONTACTS,
        ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR-> {
            when(alcoTestProgress.type){
                AlcotestProgressType.READY-> if(isContactsOk()) {
                    this.clearProgressAndSetType(ProcedureProgressType.READY)
                }else{
                    this.clearProgressAndSetType(ProcedureProgressType.WAIT_FOR_CONTACT)
                }
                else -> this.clearProgressAndSetType(ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR)
            }
        }

        ProcedureProgressType.WAIT_FOR_CONTACT -> this
    }
            ).setAlcotestProgress(alcoTestProgress)
}

fun ProcedureProgress.applyEmployee(employee: Employee):ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyEmployee $employee $this")
    return if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE){
        this.setType(ProcedureProgressType.WAIT_FOR_ALCOTEST).setEmployee(employee)
    }else{
        this
    }
}

fun ProcedureProgress.applyPhoto(photo: File):ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyPhoto $photo $this")
    return if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST){
        this.setPhoto(photo)
    }else{
        this
    }
}

fun ProcedureProgress.applyConnectionState(connectionState: ConnectionState):ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyConnectionState $connectionState $this")
    return when(connectionState){
        ConnectionState.DISCONNECTED,
        ConnectionState.DISCONNECTING -> if(alcoTestProgress?.type!=AlcotestProgressType.READY) {
            ProcedureProgress(type = ProcedureProgressType.ERROR_HARDWARE)
        }else{
            ProcedureProgress()
        }//  this.setType(ProcedureProgressType.ERROR_HARDWARE) //ProcedureProgress(ProcedureProgressType.ERROR_HARDWARE, alcoTestProgress, employee)
        ConnectionState.CONNECTING,
        ConnectionState.SEARCHING,
        ConnectionState.CONNECTED -> ProcedureProgress()// this.setType(ProcedureProgressType.UNKNOWN) //ProcedureProgress(ProcedureProgressType.UNKNOWN, alcoTestProgress, employee)
    }
}

fun ProcedureProgress.applyCanDisplayPreview(canDisplayPreview: Boolean): ProcedureProgress{
    Log.d("happy_apply","ProcedureProgress.applyCanDisplayPreview $canDisplayPreview $this")
    return if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE && !canDisplayPreview){
        this.setType(ProcedureProgressType.READY).setCanDisplayPreview(canDisplayPreview)
        //ProcedureProgress(ProcedureProgressType.READY, alcoTestProgress, employee)
    }else if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST && !canDisplayPreview){
        this.setType(ProcedureProgressType.ERROR_APP).setCanDisplayPreview(canDisplayPreview)
        //ProcedureProgress(ProcedureProgressType.ERROR_APP, alcoTestProgress, employee)
    }
    else{
        this.setCanDisplayPreview(canDisplayPreview)
    }
}

