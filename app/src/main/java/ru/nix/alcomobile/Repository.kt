package ru.nix.alcomobile

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import io.reactivex.Observable
import ru.nix.alcomobile.DAO.ResultData
import ru.nix.alcomobile.utilites.SingleLiveEvent

import java.io.File

interface Repository{
    enum class ResultForWorker{
        SUCCESS, FAILURE, RETRY
    }
    val results: LiveData<List<ResultData>>
    fun uploadFile(file: File)
    fun uploadResult(photo:File, result: Double, idc:Int, name:String)
    fun uploadLog(msg:String)
    fun uploadError(idc:Int,errorId:Int)
    suspend fun addCertificate(context: Context, fileUri: Uri, password: String)
    suspend fun removeCertificate(context: Context)

    var lastConnectedAlcotesterAddress:String?
    var lastConnectedAlcotesterName:String?
    val certificateError: SingleLiveEvent<String?>
    val certificate: LiveData<String>
    val torchModeLiveData:LiveData<TorchMode>
    val luminanceThresholdLiveData:LiveData<Int>
    var luminance:Int
    val torchObservable:Observable<Boolean>
    var luminanceThreshold:Int

    fun changeTorchMode()

    suspend fun startUploadResults():ResultForWorker
    suspend fun startUploadLogs(): ResultForWorker
    suspend fun startUploadErrors(): ResultForWorker
    val torchLiveData: LiveData<Boolean>
    val luminanceBelowThresholdObservable:Observable<Boolean>
    val torchModeObservable:Observable<TorchMode>
}

