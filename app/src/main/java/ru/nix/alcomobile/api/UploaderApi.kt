package ru.nix.alcomobile.api


import android.util.Log
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

/**
 * API communication setup
 */

const val RESPONSE_STATUS_ABSENT = "absent"
const val RESPONSE_STATUS_PART = "part"
const val RESPONSE_STATUS_COMPLETE = "complete"
interface UploaderApi {

    @GET("Uploader/Status/{fileName}")
    fun statusAsync(
        @Path("fileName") fileName: String): Deferred<Response<MResponse>>

    @GET("Uploader/SelfCheck/identity")
    fun identityAsync(): Deferred<Response<String>>

    @Headers("Accept: application/json")
    @POST("Uploader/Upload/{fileName}")
    fun uploadDescriptorAsync(
        @Path("fileName")  fileName:String,
        @Query("size")  fileLength:Int
    ): Deferred<Response<MResponse>>

    @Headers("Accept: application/json")
    @PUT("Uploader/Upload/{fileName}")
    fun uploadPartAsync(
        @Path("fileName") fileName:String,
        @Query("startPos") offset:Int,
        @Query("contentLength") length:Int,
        @Body data: RequestBody
    ): Deferred<Response<MResponse>>

    @Headers("Accept: application/json")
    @POST("Uploader/Process")
    fun processRecordAsync(
        @Body data: String
    ): Deferred<Response<ResponseBody>>

    @Headers("Accept: application/json")
    @POST("Uploader/UploadRecs")
    fun uploadRecordAsync(
        @Body data: String
    ): Deferred<Response<ResponseBody>>

    @Headers("Accept: application/json")
    @POST("Uploader/DownloadRecs")
    fun downloadRecordAsync(
        @Body data: String
    ): Deferred<Response<ResponseBody>>



    data class MResponse(val success: Boolean?, val errCode: Int?, val errMsg: String?, val status: String?, val size: Int?, val data: String?)

    companion object {
        private const val BASE_URL = "https://taylz.nix.ru/"
        fun create(sslContext: SSLContext, x509TrustManager: X509TrustManager): UploaderApi = create(HttpUrl.parse(BASE_URL)!!, sslContext, x509TrustManager)
        fun create(httpUrl: HttpUrl,sslContext: SSLContext, x509TrustManager: X509TrustManager): UploaderApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Log.d("API", it)
            })
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .sslSocketFactory(sslContext.socketFactory, x509TrustManager)
                .build()
            return Retrofit.Builder()
                    .baseUrl(httpUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
                    .create(UploaderApi::class.java)
        }
    }
}