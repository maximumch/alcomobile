package ru.nix.alcomobile.DAO

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase

private const val DB_NAME = "dbRoom.db"

@Database(entities = [ErrorData::class, LogData::class, ResultData::class], version = 1)
@TypeConverters(MTypeConverter::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun errorDataDAO(): ErrorDataDAO
    abstract fun logDataDAO(): LogDataDAO
    abstract fun resultDataDAO(): ResultDataDAO

    companion object Factory {

        @Volatile private var instance: AppDatabase?=null

        fun getInstance(context: Context):AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context,false).also { instance = it }
            }

        }

        fun buildDatabase(ctx:Context, memoryOnly: Boolean): AppDatabase{
            val b = if(memoryOnly){
                Room.inMemoryDatabaseBuilder(ctx.applicationContext,AppDatabase::class.java)

            } else{
                Room.databaseBuilder(ctx.applicationContext, AppDatabase::class.java,DB_NAME)
            }

            b.addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
//                    val request = OneTimeWorkRequestBuilder<InitDatabaseWorker>().build()
//                    WorkManager.getInstance().enqueue(request)
                }
            })

            b.fallbackToDestructiveMigration()

            return b.build()
        }
    }


}