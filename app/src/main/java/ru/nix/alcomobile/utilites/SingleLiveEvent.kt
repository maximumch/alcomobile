package ru.nix.alcomobile.utilites




import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import android.util.Log
import java.util.concurrent.atomic.AtomicBoolean




class SingleLiveEvent<T>(): MutableLiveData<T>(){
    private val mutableLiveData: MutableLiveData<T> = MutableLiveData()
    private val mPending = AtomicBoolean(false)



    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner, Observer<T>(){
            if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(it)
                }
        })

    }

//    override fun onActive() {
//        Log.d("happy_voice", "Single event onActive")
//        super.onActive()
//
//    }

    override fun setValue(value: T) {
        mPending.set(true)
        super.setValue(value)
    }


    override fun postValue(value: T) {
        mPending.set(true)
        super.postValue(value)
    }
}
