package ru.nix.alcomobile.modelnew

import ru.nix.alcomobile.hardware.KEY_CONTACT_STATE
import ru.nix.alcomobile.hardware.Message
import ru.nix.alcomobile.hardware.MessageType

private const val CONTACTS_NUMBER = 2

data class ContactState(
        val states:Map<Int,Boolean> = HashMap()
)

fun ContactState.clear():ContactState = ContactState()

fun ContactState.applyMessage(message:Message):ContactState{
    val key = when(message.messageType){
        MessageType.CONTACT1_STATE -> 1
        MessageType.CONTACT2_STATE -> 2
        else ->null
    }

    return if(key!=null){
        val state = message.params[KEY_CONTACT_STATE]?.let{
            when (it) {
                "1" -> true
                "0" -> false
                else -> null
            }
        }?:throw IllegalArgumentException("Message CONTACT1_STATE with illegal contact state")

        val statesNew = HashMap(states).apply {
            put(key, state)
        }
        ContactState(statesNew)
    }else{
        this
    }
}

fun ContactState.allTrue():Boolean? {
    var result = true
    for(key in 1..CONTACTS_NUMBER){
        val state = states[key]
        if(state==null){
            return null
        }else{
            result = result && state
        }
    }
    return result
}

fun ContactState.getState(key:Int) = states[key]
fun ContactState.allDefined():Boolean = (states.size == CONTACTS_NUMBER)