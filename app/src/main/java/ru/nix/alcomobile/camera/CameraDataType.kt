package ru.nix.alcomobile.camera

enum class CameraDataType {
    NONE, QR_CODE, IMAGE
}