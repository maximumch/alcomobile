package ru.nix.alcomobile.hardware

import java.text.DecimalFormat

enum class CommandType(){
    START, INFO, ZERO, SERIAL, GUID, AREF, PRESSURE, CALIBRATION, LED_ON, LED_OFF, STOP
}

abstract class Command(val commandType: CommandType, private val parameter:String?){
    val commandString:String get(){
        return "$" + commandType.name +
                if(parameter!=null){
                    ",$parameter"
                }else{
                    ""
                } + "\r\n"
    }
}

class CommandStart() : Command(CommandType.START,null)

class CommandInfo() : Command(CommandType.INFO,null)

class CommandZero() : Command(CommandType.ZERO,null)

class CommandSerial(serial:Int): Command(CommandType.SERIAL, serial.let{
    DecimalFormat().run {
        minimumIntegerDigits = 5
        isGroupingUsed = false
        format(it)
    }
})

class CommandGuid(guid:String): Command(CommandType.GUID, guid)

class CommandAref(aref:Int): Command(CommandType.AREF, aref.let{
    DecimalFormat().run {
        minimumIntegerDigits = 5
        isGroupingUsed = false
        format(it)
    }
})

class CommandPressure(pressure:Int): Command(CommandType.PRESSURE, pressure.let{
    DecimalFormat().run {
        minimumIntegerDigits = 5
        maximumFractionDigits = 0
        isGroupingUsed = false
        format(it)
    }
})

class CommandCalibration(calibrationConstant:Int): Command(CommandType.CALIBRATION, calibrationConstant.let{
    DecimalFormat().run {
        minimumIntegerDigits = 5
        maximumFractionDigits = 0
        isGroupingUsed = false
        format(it)
    }
})

class CommandStop() : Command(CommandType.STOP,null)

class CommandLedOn() : Command(CommandType.LED_ON,null)

class CommandLedOff() : Command(CommandType.LED_OFF,null)