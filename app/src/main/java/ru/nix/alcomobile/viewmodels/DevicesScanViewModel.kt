package ru.nix.alcomobile.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import android.content.Context
import ru.nix.alcomobile.Director
import ru.nix.alcomobile.hardware.BTDevice

class DevicesScanViewModel internal constructor(
    private val director: Director
): ViewModel() {
    val devicesList: LiveData<List<BTDevice>> = director.devicesList
    val devicesScanning:LiveData<Boolean> = director.devicesScanning
    val bluetoothDisabledError:LiveData<Boolean> = director.bluetoothDisabledError

    fun startDevicesScan()=director.scanDevices()
    fun connect(context:Context, device: BTDevice) = director.connectTo(context, device)
}