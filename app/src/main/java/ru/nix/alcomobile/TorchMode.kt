package ru.nix.alcomobile

enum class TorchMode {
    AUTO, OFF
}