package ru.nix.alcomobile.viewmodelsnew

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.nix.alcomobile.Director
import ru.nix.alcomobile.Repository


class AlcotestViewModelFactory(
        //private val cameraManager: CameraManager
        private val director: Director,
        private val repository: Repository
): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AlcotestViewModel(director, repository) as T
    }
}