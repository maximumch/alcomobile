package ru.nix.alcomobile.hardware

import android.util.Log
import ru.nix.alcomobile.model.ProcedureProgress
import ru.nix.alcomobile.model.ProcedureProgressType
import ru.nix.alcomobile.model.applyMessage

enum class AlcotestProgressType{
    UNKNOWN, READY, WAIT_AFTER_START, WAIT_FOR_FLOW, FLOW_DETECTED, WAIT_FOR_SAMPLE, FLOW_ERROR, WAIT_AFTER_FLOW_ERROR,
    SAMPLE_PROCESSING, RESULT, TIMEOUT, END_AFTER_RESULT, END_AFTER_TIMEOUT, HARDWARE_ERROR

}

data class AlcotestProgress(val type:AlcotestProgressType=AlcotestProgressType.UNKNOWN, val param:String? = null)

fun AlcotestProgress.applyMessage(message:Message):AlcotestProgress{
    return when(message.messageType){
        MessageType.END -> {
            when(type){
                AlcotestProgressType.RESULT -> AlcotestProgress(AlcotestProgressType.END_AFTER_RESULT)
                AlcotestProgressType.TIMEOUT -> AlcotestProgress(AlcotestProgressType.END_AFTER_TIMEOUT)
                else ->  AlcotestProgress(AlcotestProgressType.READY)
            }
        }

        MessageType.WAIT -> {
            when(type){
                AlcotestProgressType.READY -> AlcotestProgress(AlcotestProgressType.WAIT_AFTER_START)
                AlcotestProgressType.FLOW_ERROR -> AlcotestProgress(AlcotestProgressType.WAIT_AFTER_FLOW_ERROR)
                else -> this
            }
        }

        MessageType.BREATH -> AlcotestProgress(AlcotestProgressType.SAMPLE_PROCESSING)
        MessageType.RESULT -> AlcotestProgress(AlcotestProgressType.RESULT, message.params[KEY_TEST_RESULT])
        MessageType.STANDBY -> {
            when(type){
                AlcotestProgressType.WAIT_AFTER_START -> AlcotestProgress(AlcotestProgressType.WAIT_FOR_FLOW)
                AlcotestProgressType.WAIT_AFTER_FLOW_ERROR -> AlcotestProgress(AlcotestProgressType.WAIT_FOR_FLOW)
                AlcotestProgressType.FLOW_DETECTED -> AlcotestProgress(AlcotestProgressType.WAIT_FOR_SAMPLE)
                else -> this
            }
        }

        MessageType.TRIGGER -> AlcotestProgress(AlcotestProgressType.FLOW_DETECTED)
        MessageType.FLOW_ERROR -> AlcotestProgress(AlcotestProgressType.FLOW_ERROR)
        MessageType.TIME_OUT -> AlcotestProgress(AlcotestProgressType.TIMEOUT)
        MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH,
            MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW,
            MessageType.ERROR_NOT_ALL_COIL_TESTS_PASSED,
            MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING,
            MessageType.ERROR_PUMP_IS_NOT_CONNECTED,
            MessageType.ERROR_PUMP_CANNOT_TURN_ON -> AlcotestProgress(AlcotestProgressType.HARDWARE_ERROR)

        else->this
    }
}
