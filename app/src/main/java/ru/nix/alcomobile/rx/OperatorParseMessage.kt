package ru.nix.alcomobile.rx

import io.reactivex.*
import io.reactivex.disposables.Disposable
import ru.nix.alcomobile.hardware.Message
import ru.nix.alcomobile.hardware.parseMessages
import java.lang.StringBuilder


class OperatorParseMessage:ObservableOperator<Message,String>{
    override fun apply(child: Observer<in Message>?): Observer<in String> {
        return object:Observer<String>{
            val buffer:StringBuilder = StringBuilder("")

            override fun onComplete() {
                child?.onComplete()
            }

            override fun onSubscribe(d: Disposable?) {
                child?.onSubscribe(d)
            }

            override fun onNext(value: String?) {
                buffer.append(value)
                val parseResult = parseMessages(buffer.toString())
                if(parseResult.second>0){
                    buffer.delete(0,parseResult.second)
                }

                if(parseResult.first.isNotEmpty()) {
                    for (message in parseResult.first) {
                        child?.onNext(message)
                    }
                }
            }

            override fun onError(e: Throwable?) {
                child?.onError(e)
            }

        }
    }


}