package ru.nix.alcomobile.DAO

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "error")
data class ErrorData(
        val errorId:Int,
        val idc:Int,
        val date:Long,
        val uploaded:Boolean?=false,
        @PrimaryKey val id:String = UUID.randomUUID().toString()
)

fun ErrorData.setUploaded() = ErrorData(
        errorId = errorId,
        idc = idc,
        date = date,
        uploaded = true,
        id = id
)