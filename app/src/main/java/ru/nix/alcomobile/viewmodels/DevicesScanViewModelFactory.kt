package ru.nix.alcomobile.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.nix.alcomobile.Director

class DevicesScanViewModelFactory internal constructor(
        private val director: Director
): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DevicesScanViewModel(director) as T
    }
}
