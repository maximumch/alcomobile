package ru.nix.alcomobile.rx

import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

private const val ERROR_MESSAGE_MAX_ATTEMPTS = "Reliable write error. Max write attempts reached"
private const val ERROR_MESSAGE_MAX_DELAY = "Reliable write error. Max delay time expired"
class ReliableDataWriter(
        private val dataToWrite:String,
        private val writtenData: Observable<String>,
        private val canRewrite:Observable<Int>,
        private val writeAction:(String)-> Unit,
        private val onWriteSuccessAction:() -> Unit,
        private val onWriteErrorAction:()-> Unit,
        private val maxAttempts:Int,
        private val maxDelay:Long
        ): Completable() {
    private var observer:CompletableObserver?=null
    private var attempts = 0
    private var disppsable:CompositeDisposable=CompositeDisposable()

    override fun subscribeActual(s: CompletableObserver?) {
        observer = s
        disppsable.add(
                writtenData.subscribe {
                    if(it==dataToWrite){
                        onWriteSuccessAction.invoke()
                        disppsable.clear()
                        disppsable.dispose()
                        observer?.onComplete()
                    }else{
                        attempts++
                        if(attempts<maxAttempts){
                            canRewrite.take(1).subscribe {status ->
                                writeAction.invoke(dataToWrite)
                            }
                                    .also{canWriteDisposable -> disppsable.add(canWriteDisposable)}
                            onWriteErrorAction.invoke()
                        }else{
                            disppsable.clear()
                            disppsable.dispose()
                            observer?.onError(Throwable(ERROR_MESSAGE_MAX_ATTEMPTS))
                        }
                    }

                })

        writeAction.invoke(dataToWrite)

        Observable.timer(maxDelay, TimeUnit.MILLISECONDS).subscribe {
           // observer?.onError(Throwable(ERROR_MESSAGE_MAX_DELAY))

            disppsable.clear()
            disppsable.dispose()
        }
    }
}