package ru.nix.alcomobile.camera

import android.util.Size
import android.view.SurfaceHolder
import io.reactivex.Observable
import ru.nix.alcomobile.modelnew.CameraCommandType
import java.io.File

interface CameraHelper {
    var surfaceHoldersStream:Observable<SurfaceHolder>
    var cameraCommandTypeStream:Observable<CameraCommandType>
    val qrCodeGot:Observable<Employee>
    val photoTaken:Observable<File>
    val cameraError:Observable<Throwable>


//    fun previewSubscribe(
//            previewRequestsObservable: Observable<Pair<CameraCommand, SurfaceHolder>>,
//            isCameraDeviceFree: Observer<Boolean>,
//            qrCodeGot:Observer<Employee>,
//            photoTaken:Observer<File>)


}



data class CameraChannelConfig(val orientation: Int, val size: Size)

enum class CameraCommand{
    START, STOP, QRCODE_START, QRCODE_STOP, TAKE_PHOTO
}