package ru.nix.alcomobile

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import android.view.SurfaceHolder
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import ru.nix.alcomobile.audio.VoiceMessageType
import ru.nix.alcomobile.camera.CameraDataType
import ru.nix.alcomobile.camera.CameraHelper
import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.*
import ru.nix.alcomobile.modelnew.*
import ru.nix.alcomobile.modelnew.AlcotestResult

import ru.nix.alcomobile.rx.*
import ru.nix.alcomobile.rxnew.createProcedureProgressNew
import ru.nix.alcomobile.rxnew.createTorchOnOffStream
import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.utilites.SingleLiveEvent
import java.io.File
import java.util.concurrent.TimeUnit


class Director private constructor(context: Context) {
    private val yyyContext:Context = context.applicationContext


    private val repository:Repository = InjectorUtils.provideRepository(context)
    private val deviceConnector:DeviceConnector = InjectorUtils.provideDeviceConnector(context)
    private val cameraHelper: CameraHelper
    //val cameraConfig: CameraConfig get() = cameraHelper.cameraConfig

    private val surfaceHolderFromActivity = PublishSubject.create<SurfaceHolder>()
    private val activityCanDisplayPreview = PublishSubject.create<Boolean>()
    private val needDataFromCamera = PublishSubject.create<CameraDataType>()
    private val isCameraDeviceFree = PublishSubject.create<Boolean>()
    private val cameraPermissionGranted = Observable.just(true) // TODO camera permission check
    private val needQRCode = PublishSubject.create<Boolean>()
    private val needPhoto = PublishSubject.create<Boolean>()
    private val qrCodeGot = PublishSubject.create<Employee>()
    private val photoTaken = PublishSubject.create<File>()
    private val luminanceOk = PublishSubject.create<Boolean>()


    //private val alcotesterDeviceData = PublishSubject.create<String>()
    private val alcotesterDeviceConnectionState = PublishSubject.create<ConnectionState>()
    //private val xxx_alcotestProgress = PublishSubject.create<AlcotestProgress>()
    //private val alcotestMessage = PublishSubject.create<Message>()

    val surfaceFromActivityObserver: Observer<SurfaceHolder> get()= surfaceHolderFromActivity
    val activityCanDisplayPreviewObserver: Observer<Boolean> get()= activityCanDisplayPreview
    val luminanceOkObserver:Observer<Boolean> get() = luminanceOk

    val needQRCodeObserver:Observer<Boolean> get() = needQRCode
    val needPhotoObserver:Observer<Boolean> get() = needPhoto

    private val mNeedSendResultToServer = PublishSubject.create<ProcedureProgress>()
    val needSendResultToServerObservable: Observable<ProcedureProgress> get() = mNeedSendResultToServer

    private val mNeedSendErrorToServer = PublishSubject.create<ProcedureProgress>()
    val needSendErrorToServerObservable: Observable<ProcedureProgress> get() = mNeedSendErrorToServer


    //private val commandPublishSubject = PublishSubject.create<Command>()
    private val mHardwareInputData = PublishSubject.create<String>()
    private val previewPublishSubject = PublishSubject.create<Boolean>()

    var cameraFreeObservable:Observable<Boolean> = Observable.just(true)
    val cameraFreePublishSubject:PublishSubject<Boolean> = PublishSubject.create()

    private val mVoiceMessageSingleLiveEvent = SingleLiveEvent<VoiceMessageType>()
    val voiceMessageSingleLiveEvent:LiveData<VoiceMessageType> = mVoiceMessageSingleLiveEvent

//    private val mVoiceMessageObservable = PublishSubject.create<VoiceMessageType>()
//    val voiceMessageObservable:Observable<VoiceMessageType> = mVoiceMessageObservable



    private var alcotesterConnectDisposable:Disposable? = null
    private var alcotesterConnectNewStateDisposable:Disposable? = null
    private var scanDisposable:Disposable?=null
    private var connectPreviewDisposable:Disposable?=null
    private var previewDisposable:Disposable?=null
    private val mCompositeDisposable = CompositeDisposable()

    private val mAlcotestProgress: MutableLiveData<AlcotestProgress> = MutableLiveData()
    val alcotestProgress:LiveData<AlcotestProgress> get() = mAlcotestProgress

    private val mDevicesList:MutableLiveData<List<BTDevice>> = MutableLiveData()
    val devicesList:LiveData<List<BTDevice>> get() = mDevicesList

    private val mDevicesScanning:MutableLiveData<Boolean> = MutableLiveData()
    val devicesScanning:LiveData<Boolean> get() = mDevicesScanning

    private val mBluetoothDisabledError: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val bluetoothDisabledError:LiveData<Boolean> get() = mBluetoothDisabledError

    //private val _connectionState: SingleLiveEvent<ConnectionState> = SingleLiveEvent()


//    private val mProcedureProgress: MutableLiveData<ProcedureProgress> = MutableLiveData()
//    val procedureProgress:LiveData<ProcedureProgress> get() = mProcedureProgress

//    private val mResult: MutableLiveData<ProcedureProgress> = MutableLiveData()
//    val result:LiveData<ProcedureProgress> get() = mResult

    private val mSerial: MutableLiveData<Int> = MutableLiveData()
    val serial:LiveData<Int> get() = mSerial

    private val mGuid: MutableLiveData<String> = MutableLiveData()
    val guid:LiveData<String> get() = mGuid

    private val mHardvare: MutableLiveData<String> = MutableLiveData()
    val hardware:LiveData<String> get() = mHardvare

    private val mFirmvare: MutableLiveData<String> = MutableLiveData()
    val firmware:LiveData<String> get() = mFirmvare

    private val mTestsCountTotal: MutableLiveData<Int> = MutableLiveData()
    val testsCountTotal:LiveData<Int> get() = mTestsCountTotal

    private val mTestsCountPositive: MutableLiveData<Int> = MutableLiveData()
    val testsCountPositive:LiveData<Int> get() = mTestsCountPositive

    private val mTestsCountFailed: MutableLiveData<Int> = MutableLiveData()
    val testsCountFailed:LiveData<Int> get() = mTestsCountFailed

    private val mAref: MutableLiveData<Int> = MutableLiveData()
    val aref:LiveData<Int> get() = mAref

    private val mPressure: MutableLiveData<Int> = MutableLiveData()
    val pressure:LiveData<Int> get() = mPressure

    private val mCalibration: MutableLiveData<Int> = MutableLiveData()
    val calibration:LiveData<Int> get() = mCalibration

    private val mGotOK: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val gotOK:LiveData<Boolean> get() = mGotOK

    private val mGotErrorSafetyButton: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val gotErrorSafetyButton:LiveData<Boolean> get() = mGotErrorSafetyButton

    private val mGotHardwareError: SingleLiveEvent<Pair<MessageType,String?>> = SingleLiveEvent()
    val gotHardwareError:LiveData<Pair<MessageType,String?>> get() = mGotHardwareError

    private val mContactState = MutableLiveData<Map<Int,Int>?>()
    val contactState:LiveData<Map<Int,Int>?> = mContactState

    //----------------------------------------------------------------
    private val _procedureProgress = MutableLiveData<ProcedureProgress>(ProcedureProgress())
    val procedureProgress:LiveData<ProcedureProgress> get() = _procedureProgress

    private val _textMessageType = MutableLiveData<TextMessageType>()
    val textMessageType:LiveData<TextMessageType> = _textMessageType

    private val _vibrate = SingleLiveEvent<Unit>()
    val vibrate:LiveData<Unit> get() = _vibrate

    private val _showPreview = MutableLiveData<Boolean>(false)
    val showPreview:LiveData<Boolean> get() = _showPreview

    private val _playCameraShutter = SingleLiveEvent<Unit>()
    val playCameraShutter:LiveData<Unit> get() = _playCameraShutter

    private val _result = SingleLiveEvent<AlcotestResult>()
    val result:LiveData<AlcotestResult> get() = _result

    private val _voiceMessage = PublishSubject.create<VoiceMessageType>()
    val voiceMessage:Observable<VoiceMessageType> get() = _voiceMessage

    private val _programmingResponse = SingleLiveEvent<ProgrammingResponseType>()
    val programmingResponse: LiveData<ProgrammingResponseType> get() = _programmingResponse

    private val _connectionState: MutableLiveData<ConnectionState> = MutableLiveData()
    val connectionState:LiveData<ConnectionState> get() = _connectionState

    private val _breathProgress: MutableLiveData<Long?> = MutableLiveData(null)
    val breathProgress:LiveData<Long?> get() = _breathProgress

    private val _deviceName: MutableLiveData<String?> = MutableLiveData(repository.lastConnectedAlcotesterName)
    val deviceName:LiveData<String?> get() = _deviceName

    private val _deviceAddress: MutableLiveData<String?> = MutableLiveData(repository.lastConnectedAlcotesterAddress)
    val deviceAddress:LiveData<String?> get() = _deviceAddress




    private var debugText = ""
    val debugTextLiveData = MutableLiveData<String>()

    init{
        //deviceConnector.inputData = mHardwareInputData
        deviceConnector.connectionState.subscribe {
            alcotesterDeviceConnectionState.onNext(it)
            Log.d("happyConnection", "Connection state = $it")
        }.also { mCompositeDisposable.add(it) }

        //Stream of messages from alcotester device
        val messageStream = deviceConnector.outputData
//                .doOnNext {
//                    debugText = it+debugText
//                    debugTextLiveData.postValue(debugText)
//                }
                .lift(OperatorParseMessage())

        cameraHelper =InjectorUtils.provideCameraHelper(context)
        cameraHelper.surfaceHoldersStream = surfaceHolderFromActivity
        cameraHelper.photoTaken.subscribe { photoTaken.onNext(it) }.also { mCompositeDisposable.add(it) }
        cameraHelper.qrCodeGot.subscribe { qrCodeGot.onNext(it) }.also { mCompositeDisposable.add(it) }

        val procedureProgress = createProcedureProgressNew(messageStream,
                qrCodeGot,
                photoTaken,
                alcotesterDeviceConnectionState,
                activityCanDisplayPreview,
                cameraHelper.cameraError
                )

        deviceConnector.inputData = Observable.merge(
                procedureProgress.hardwareInputStream.map { command -> command.commandString },
                mHardwareInputData
        )
        cameraHelper.cameraCommandTypeStream = procedureProgress.cameraCommandStream


        //TODO Delete debugging code
        var employee:Employee?=null
        var procedureProgressType=ru.nix.alcomobile.modelnew.ProcedureProgressType.UNKNOWN

        procedureProgress.procedureProgressStream.subscribe {
            Log.d("happysettings", "${it.settings}")
            it?.also { this._procedureProgress.postValue(it) }
            if(it.employee!=null && employee==null){
                _vibrate.postValue(Unit)
            }

            //TODO Delete debugging code below
            Log.d("happynew","Progress = $it")
            if((it.employee!=null && employee==null) || (it.employee==null && employee != null)){
                Log.d("happynewtext", "Employee= ${it.employee}")
            }
            employee=it.employee
            if(it.type!=procedureProgressType){
                Log.d("happynewtext", "ProgressType= ${it.type}")
            }
            procedureProgressType=it.type
            if(it.connectionState!=_connectionState.value) _connectionState.postValue(it.connectionState)

            updateBreathProgress(it.alcoTestProgress.type)




        }.also { mCompositeDisposable.add(it) }


        procedureProgress.textMessageStream.subscribe {
            it?.also{ _textMessageType.postValue(it) }
            Log.d("happynewtext","TextMessage = $it")
        }.also { mCompositeDisposable.add(it) }

        procedureProgress.voiceMessageStream.subscribe {
            it?.also{ _voiceMessage.onNext(it)}
        }.also { mCompositeDisposable.add(it) }

        procedureProgress.cameraCommandStream.subscribe {
            _showPreview.postValue(
                when(it){
                    CameraCommandType.STOP_PREVIEW -> false
                    CameraCommandType.TAKE_PHOTO,
                    CameraCommandType.GET_QR_CODE,
                    CameraCommandType.START_PREVIEW -> true
                    else -> false
                }
            )
            if(it==CameraCommandType.TAKE_PHOTO) _playCameraShutter.postValue(Unit)
        }.also { mCompositeDisposable.add(it) }


        procedureProgress.alcotestResultStream.observeOn(Schedulers.io()).subscribe {
            _result.postValue(it)
            if(it!=null) {
                repository.uploadFile(it.photo)
                repository.uploadResult(it.photo, it.concentration, it.employee.idc, it.employee.name)
            }
        }.also { mCompositeDisposable.add(it) }

        procedureProgress.messageForLogStream.observeOn(Schedulers.io()).subscribe {
            if(it!=null){
                repository.uploadLog(it)
            }
        }.also { mCompositeDisposable.add(it) }

        procedureProgress.alcotestErrorStream.observeOn(Schedulers.io()).subscribe {
            if(it!=null){
                repository.uploadError(it.employee.idc,it.type.code)
            }
        }.also { mCompositeDisposable.add(it) }

        procedureProgress.programmingResponseStream.observeOn(Schedulers.io()).subscribe {
            _programmingResponse.postValue(it)
            //if(it==ProgrammingResponseType.OK) startInfo()
            if(it==ProgrammingResponseType.OK) repository.uploadLog("OK Response")
        }.also { mCompositeDisposable.add(it) }




        procedureProgress.hardwareInputStream.doOnNext{ Log.d("happynewtext","HardwareCommand = $it")  }
        procedureProgress.alcotestResultStream.subscribe{ Log.d("happynewtext","Result = $it") }.also { mCompositeDisposable.add(it) }
        procedureProgress.alcotestErrorStream.subscribe { Log.d("happynewtext","AlcotestError = $it")  }.also { mCompositeDisposable.add(it) }
        procedureProgress.messageForLogStream.subscribe { Log.d("happynewtextlog","MessageForLog = $it")  }.also { mCompositeDisposable.add(it) }

//        luminanceOk.subscribe{
//            if(it){
//                mHardwareInputData.onNext(CommandLedOff().commandString)
//            }else{
//                mHardwareInputData.onNext(CommandLedOn().commandString)
//            }
//
//        }



        createTorchOnOffStream(procedureProgress.cameraCommandStream,
                repository.luminanceBelowThresholdObservable,
                repository.torchModeObservable)
                .observeOn(Schedulers.io())
                .subscribe{
            Log.d("TORCH", "$it")
            repository.uploadLog("TORCH ${if(it) "ON" else "OFF" }")
            if(it){
                mHardwareInputData.onNext(CommandLedOn().commandString)
            }else{
                mHardwareInputData.onNext(CommandLedOff().commandString)
            }
        }.also { mCompositeDisposable.add(it) }








    }

    var breathDisposable: Disposable? = null

    private fun updateBreathProgress(alcotestProgressType: AlcotestProgressType) {
        when(alcotestProgressType){

            AlcotestProgressType.WAIT_FOR_FLOW -> _breathProgress.postValue(0)
            AlcotestProgressType.FLOW_DETECTED ->{
                Log.d("happyBreath","FLOW DETECTED")
                if(breathDisposable?.isDisposed != false) {
                    breathDisposable = Observable
                            .interval(/*2350*/ 3350 / 8, TimeUnit.MILLISECONDS)
                            .subscribe { n ->
                                if (breathDisposable?.isDisposed == false) {
                                    _breathProgress.postValue(n + 1)
                                    if (n > 6) {
                                        breathDisposable?.dispose()

                                    }
                                }
                            }
                }
            }
            AlcotestProgressType.WAIT_FOR_SAMPLE ->{}
            else ->{
                breathDisposable?.dispose()
                _breathProgress.postValue(null)
            }
        }
    }


//    init{
//        repository = InjectorUtils.provideRepository(context)
//        deviceConnector = InjectorUtils.provideDeviceConnector(context)
//        deviceConnector.inputData = mHardwareInputData
//        deviceConnector.connectionState.subscribe {
//            alcotesterDeviceConnectionState.onNext(it)
//            //_connectionState.postValue(it)
//            Log.d("happyConnection", "Connection state = $it")
//        }
//        _connectionState.postValue(ConnectionState.DISCONNECTED)
//
//        //Stream of messages from alcotester device
//        val messageStream = deviceConnector.outputData.lift(OperatorParseMessage())
//
//        //deviceConnector = InjectorUtils.provideAlcotesterConnector(context,commandPublishSubject)
//        cameraHelper =InjectorUtils.provideCameraHelper(context)
//
//        val filterCameraCommand = filterCameraCommand(
//                surfaceHolderFromActivity,
//                activityCanDisplayPreview,
//                needDataFromCamera,
//                isCameraDeviceFree,
//                cameraPermissionGranted,
//                needQRCode,
//                needPhoto
//        ).share()
//
//        cameraHelper.previewSubscribe(filterCameraCommand, isCameraDeviceFree, qrCodeGot, photoTaken)
//
//        alcotesterDeviceConnectionState.subscribe {
//            _connectionState.postValue(it)
//            if(it!=ConnectionState.CONNECTED) mContactState.postValue(null)
//            repository.uploadLog("Device connection state changed: ${it.name}")
//
//        }
//
//       messageStream
//                .lift(OperatorAlcotestProgress())
//                .doOnNext {
//                    mAlcotestProgress.postValue(it)
//                    xxx_alcotestProgress.onNext(it)
//                }
//                .subscribe()
//
//        val procedureProgress = createProcedureProgress(
//                xxx_alcotestProgress,
//                messageStream,
//                qrCodeGot,
//                photoTaken,
//                alcotesterDeviceConnectionState,
//                activityCanDisplayPreview
//        ).share()
//
//
//
//
//        procedureProgress.subscribe {
//            mProcedureProgress.postValue(it)
//            mContactState.postValue(it.contactState)
//
//            when(it.type){
//                ProcedureProgressType.UNKNOWN -> {
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                }
//
//                ProcedureProgressType.READY -> {
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                }
//
//                ProcedureProgressType.WAIT_FOR_EMPLOYEE ->{
//                    Log.d("xxx_happy", "CHECKPOINT")
//
//                    needDataFromCamera.onNext(CameraDataType.IMAGE)
//                    needQRCode.onNext(true)
//                }
//
//                ProcedureProgressType.WAIT_FOR_ALCOTEST ->{
//                    if(it.alcoTestProgress?.type==AlcotestProgressType.READY || it.alcoTestProgress==null){
//                        Log.d("yyy_happy","START COMMAND")
//                        mHardwareInputData.onNext(CommandStart().commandString)
//
//                    }
//                    if(it.alcoTestProgress?.type==AlcotestProgressType.SAMPLE_PROCESSING) {
//                        Log.d("yyy_happy","TAKE PICTURE")
//                        needPhoto.onNext(true)
//                    }
//                    if(it.alcoTestProgress?.type==AlcotestProgressType.FLOW_ERROR) {
//                        mNeedSendErrorToServer.onNext(it)
//                    }
//                }
//
//                ProcedureProgressType.RESULT -> {
//                    Log.d("happyResult", "ProcedureProgressType.RESULT")
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                    mNeedSendResultToServer.onNext(it)
//                }
//
//                ProcedureProgressType.ERROR_TIMEOUT ->{
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                    mNeedSendErrorToServer.onNext(it)
//                }
//
//                ProcedureProgressType.ERROR_HARDWARE ->{
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                    mNeedSendErrorToServer.onNext(it)
//                }
//
//                ProcedureProgressType.ERROR_APP ->{
//                    needDataFromCamera.onNext(CameraDataType.NONE)
//                    mNeedSendErrorToServer.onNext(it)
//                }
//            }
//
//            if(it.type==ProcedureProgressType.WAIT_FOR_EMPLOYEE){
//                needDataFromCamera.onNext(CameraDataType.IMAGE)
//                needQRCode.onNext(true)
//            }
//
//            if(it.contactState.isEmpty()){
//               startInfo()
//            }
//
//            Log.d("xyy_happy","PROCEDURE PROGRESS = ${it}")
//
//        }
//
//        procedureProgress.lift(OperatorVoiceMessage()).subscribe{
//            mVoiceMessageSingleLiveEvent.postValue(it)
//            mVoiceMessageObservable.onNext(it)
//            //playVoiceMessage(yyyContext,it)
//        }
//
//
//        isCameraDeviceFree.onNext(true)
//
//        filterCameraCommand.subscribe { Log.d("xxy_happy","Command ${it.first.name}") }
//
//        qrCodeGot.subscribe {
//            Log.d("xxy_happy","QRCode= $it") }
//
//        photoTaken.subscribe { Log.d("xxy_happy","Photo= $it") }
//
//        needSendResultToServerObservable.subscribe{
//            val result = it.result
//            val photo = it.photo
//            val idc = it.employee?.idc
//            if(result!=null && photo!=null && idc!=null){
//                Log.d("happyResult","needSendResultToServerObservable")
//
//                repository.uploadFile(photo)
//                repository.uploadResult(photo, result, idc)
//            }
//        }
//
//        needSendErrorToServerObservable.subscribe{
//            Log.d("happy", "needSendErrorToServerObservable $it")
//            val idc = it.employee?.idc
//            val errorId = if(it.type==ProcedureProgressType.ERROR_TIMEOUT) {
//                ID_ERROR_TIMEOUT
//            }else if(it.type==ProcedureProgressType.ERROR_HARDWARE){
//                ID_ERROR_HARDWARE
//            }else if(it.type==ProcedureProgressType.ERROR_APP){
//                ID_ERROR_APP
//            }else if(it.alcoTestProgress?.type==AlcotestProgressType.FLOW_ERROR)
//                ID_ERROR_FLOW
//            else{
//                null
//            }
//            if(idc!=null && errorId!=null) repository.uploadError(idc, errorId)
//        }
//
//        messageStream.subscribe { message->
//            if(message.messageType==MessageType.LOG){
//                message.Params[KEY_LOG_MESSAGE]?.also{messageString->
//                    repository.uploadLog(messageString)
//                }
//            }
//            if(message.messageType==MessageType.SERIAL){
//                message.Params[KEY_SERIAL]?.toInt()?.also{ mSerial.postValue(it) }
//            }
//            if(message.messageType==MessageType.GUID){
//                message.Params[KEY_GUID]?.also{ mGuid.postValue(it) }
//            }
//            if(message.messageType==MessageType.HARDWARE){
//                message.Params[KEY_HARDWARE]?.also{ mHardvare.postValue(it) }
//            }
//            if(message.messageType==MessageType.FIRMWARE){
//                message.Params[KEY_FIRMWARE]?.also{ mFirmvare.postValue(it) }
//            }
//            if(message.messageType==MessageType.COUNT_TESTS){
//                message.Params[KEY_COUNT_TESTS_TOTAL]?.toInt()?.also{ mTestsCountTotal.postValue(it)}
//                message.Params[KEY_COUNT_TESTS_POSITIVE]?.toInt()?.also{ mTestsCountPositive.postValue(it)}
//            }
//            if(message.messageType==MessageType.COUNT_TESTS_TOTAL){
//                message.Params[KEY_COUNT_TESTS_TOTAL]?.toInt()?.also{ mTestsCountTotal.postValue(it)}
//            }
//            if(message.messageType==MessageType.COUNT_TESTS_POSITIVE){
//                message.Params[KEY_COUNT_TESTS_POSITIVE]?.toInt()?.also{ mTestsCountPositive.postValue(it)}
//            }
//            if(message.messageType==MessageType.COUNT_TESTS_FAILED){
//                message.Params[KEY_COUNT_TESTS_FAILED]?.toInt()?.also{ mTestsCountFailed.postValue(it)}
//            }
//            if(message.messageType==MessageType.AREF){
//                message.Params[KEY_AREF]?.toInt()?.also{ mAref.postValue(it)}
//            }
//            if(message.messageType==MessageType.PRESSURE){
//                message.Params[KEY_PRESSURE]?.toInt()?.also{ mPressure.postValue(it)}
//            }
//            if(message.messageType==MessageType.CALIBRATION){
//                message.Params[KEY_CALIBRATION]?.toInt()?.also{ mCalibration.postValue(it)}
//            }
//            if(message.messageType==MessageType.OK){
//                mGotOK.postValue(true)
//                startInfo()
//            }
//            if(message.messageType==MessageType.ERROR_SAFETY_BUTTON_NOT_PRESSED){
//                mGotErrorSafetyButton.postValue(true)
//
//            }
//            if(message.messageType==MessageType.ERROR_PUMP_CANNOT_TURN_ON ||
//                    message.messageType==MessageType.ERROR_PUMP_IS_NOT_CONNECTED ||
//                    message.messageType==MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING ||
//                    message.messageType==MessageType.ERROR_NOT_ALL_COIL_TESTS_PASSED ||
//                    message.messageType==MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW ||
//                    message.messageType==MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH){
//                mGotHardwareError.postValue(Pair(message.messageType,
//                        message.Params[KEY_VOLTAGE]))
//                repository.uploadLog("Hardware error: ${message.messageType.name} ${message.Params[KEY_VOLTAGE]?:""}")
//
//            }
//
//        }
//
//        deviceConnector.outputData.subscribe {
//            Log.d("happyStr",it)
//        }
//
//
//
//
//    }

    fun results() = repository.results

    fun xxx_setNeedDataFromCamera(cameraDataType: CameraDataType){
        needDataFromCamera.onNext(cameraDataType)
    }

    fun disconnect(){
        deviceConnector.disconnect()

    }

    fun scanDevices(){
        val devices = ArrayList<BTDevice>()
        mDevicesList.postValue(devices)
        mDevicesScanning.postValue(true)
        scanDisposable?.dispose()
        deviceConnector.scanDevices()
                .subscribe({
                    devices.add(it)
                    mDevicesList.postValue(devices)
                    Log.d("happy", "name = ${it.name}, address = ${it.address}")
                },{
                    if(it is BluetoothDisabledException) mBluetoothDisabledError.postValue(true)
                    mDevicesScanning.postValue(false)
                    Log.d("happy", "scan error")
                },{
                    mDevicesScanning.postValue(false)
                    Log.d("happy", "scan finished")
                })
                .also { scanDisposable = it }
    }

    fun reconnect(context: Context){
        val lastAddress = repository.lastConnectedAlcotesterAddress
        if(lastAddress!=null) {
            _connectionState.postValue(ConnectionState.SEARCHING)
            var d:Disposable?=null
            d = deviceConnector.scanDevices().subscribe(

                    {device ->
                        if(device.address==lastAddress) {

                            connectTo(context, lastAddress)
                            d?.dispose()
                        }
                    },
                    {throwable ->
                        if(throwable is BluetoothDisabledException)  mBluetoothDisabledError.postValue(true)
                        _connectionState.postValue(ConnectionState.DISCONNECTED)
                    },
                    {
                        _connectionState.postValue(ConnectionState.DISCONNECTED)
                    }
            )
        }
    }

    fun connectTo(context: Context,device:BTDevice){
        repository.lastConnectedAlcotesterAddress = device.address
        repository.lastConnectedAlcotesterName = device.name
        _deviceAddress.postValue(device.address)
        _deviceName.postValue(device.name)
        connectTo(context, device.address)
    }

    private fun connectTo(context: Context,address:String){
        scanDisposable?.dispose()
        alcotesterConnectDisposable?.dispose()
        val d = CompositeDisposable()
        deviceConnector.connectDevice(context,address)
    }



    fun startTest(){
        mHardwareInputData.onNext(CommandStart().commandString)
    }

    fun  startInfo(){
        mHardwareInputData.onNext(CommandInfo().commandString)
    }

    fun setSerial(serial:Int){
        mHardwareInputData.onNext(CommandSerial(serial).commandString)
    }

    fun setGuid(guid:String){
        mHardwareInputData.onNext(CommandGuid(guid).commandString)
    }

    fun setAref(aref:Int){
        mHardwareInputData.onNext(CommandAref(aref).commandString)
    }

    fun setPressure(pressure:Int){
        mHardwareInputData.onNext(CommandPressure(pressure).commandString)
    }

    fun setCalibration(calibration:Int){
        mHardwareInputData.onNext(CommandCalibration(calibration).commandString)
    }

    fun countsToZero(){
        mHardwareInputData.onNext(CommandZero().commandString)
    }

    fun notifyBluetoothTurnOff(){
        alcotesterDeviceConnectionState.onNext(ConnectionState.DISCONNECTED)
        mBluetoothDisabledError.postValue(true)
    }



//    private fun playVoiceMessage(context:Context, type:VoiceMessageType){
//        val resId = when(type){
//            VoiceMessageType.BAD_FLOW -> R.raw.bad_flow
//            VoiceMessageType.BLOW -> R.raw.blow
//            VoiceMessageType.CONTINUE_BLOW -> R.raw.continue_blow
//            VoiceMessageType.ERROR_MIRROR_OR_HOLDER_FLIPPED_DURING_TEST -> R.raw.error_mirror_or_holder_flipped_during_test
//            VoiceMessageType.HARDWARE_ERROR -> R.raw.hardware_error
//            VoiceMessageType.MIRROR_OR_HOLDER_FOLDED -> R.raw.mirror_or_holder_flipped
//            VoiceMessageType.NEGATIVE_RESULT -> R.raw.negative_result
//            VoiceMessageType.POSITIVE_RESULT -> R.raw.positive_result
//            VoiceMessageType.PRESS_START_BUTTON -> R.raw.press_start_button
//            VoiceMessageType.PROCESSING -> R.raw.processing
//            VoiceMessageType.SCAN_PASS -> R.raw.scan_pass
//            VoiceMessageType.TIMEOUT_EXPIRED -> R.raw.timeout_expired
//            VoiceMessageType.UNKNOWN_ERROR ->R.raw.unknown_error
//            VoiceMessageType.WAIT -> R.raw.wait
//            VoiceMessageType.CAMERA_ERROR -> R.raw.unknown_error
//            VoiceMessageType.NO_PICTURE_RESULT -> R.raw.unknown_error
//        }
//        Log.d("happy_voice","Play $resId")
//
//        var mediaPlayer: MediaPlayer? = MediaPlayer.create(context, resId)
//        Log.d("happy_voice","Player started $mediaPlayer")
//        mediaPlayer?.setOnCompletionListener {
//            Log.d("happy_voice","Player completed $it")
//            mediaPlayer?.release()
//            Log.d("happy_voice","Player released $it")
//            mediaPlayer = null
//        }
//
//        mediaPlayer?.setOnErrorListener(MediaPlayer.OnErrorListener{ mediaPlayer: MediaPlayer, i: Int, i1: Int ->
//            Log.d("happy_voice","Error $i $i1 $mediaPlayer ")
//            true
//        })
//
//        mediaPlayer?.setOnInfoListener{ mediaPlayer: MediaPlayer, i: Int, i1: Int ->
//            Log.d("happy_voice","Info $i $i1 $mediaPlayer ")
//            true
//        }
//
//        mediaPlayer?.start()
//    }

    fun torchOn(){
        mHardwareInputData.onNext(CommandLedOn().commandString)
    }
    fun torchOff(){
        mHardwareInputData.onNext(CommandLedOff().commandString)
    }

    fun stop(){
        mHardwareInputData.onNext(CommandStop().commandString)
    }



    companion object  {
        @Volatile private var instance: Director?=null

        fun getInstance(context: Context) =
                instance ?: synchronized(this) {
                    instance ?: Director(context).also {
                        instance = it
                    }
                }
    }
}
