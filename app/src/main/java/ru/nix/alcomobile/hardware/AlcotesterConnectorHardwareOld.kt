package ru.nix.alcomobile.hardware

import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap



class AlcotesterConnectorHardware (
context: Context,
commandsObservable:Observable<Command>
):AlcotesterConnector{
    private val bluetoothAdapter:BluetoothAdapter = (context.applicationContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?)?.adapter?:throw(Exception())
    private val devices = HashMap<String,BluetoothDevice>()
    private val supportedUUID =UUID.fromString(sUUID);
    private val characteristicsUUID=UUID.fromString(cUUID);
    private var mGatt:BluetoothGatt?=null


    init {
        commandsObservable.subscribe{command ->
            Log.d("happy","Got command ${command.commandString}")
            val characteristic = mGatt
                ?.getService(supportedUUID)
                ?.getCharacteristic(characteristicsUUID)


//            for(commandStringChunk in command.commandString.chunked(16)){
//                characteristic?.value =commandStringChunk.toByteArray()
//                characteristic?.let{
//                    var result:Boolean? = false
//                    var n=0
//                    do{
//                        if(n>0) Thread.sleep(100)
//                        result = mGatt?.writeCharacteristic(characteristic)
//                        n++
//                        Log.d("happy","Write command result $result chunk = $commandStringChunk")
//                    }while ((result==false) && n<10)
//
//                }
//            }
//            characteristic?.value = byteArrayOf(0x0D,0x0A)
//            characteristic?.let{
//               var result:Boolean? = false
//                var n=0
//                do{
//                    if(n>0) Thread.sleep(100)
//                    result = mGatt?.writeCharacteristic(characteristic)
//                    n++
//                    Log.d("happy","Write command result $result chunk = rn")
//                }while ((result==false) && n<10)
//
//
//
//            }




            val ba = command.commandString.toByteArray()
            characteristic?.value = ba + byteArrayOf(0x0D,0x0A)

            characteristic?.let{
                val result = mGatt?.writeCharacteristic(characteristic)
                Log.d("happy","Write command result $result")
            }
        }
    }



    override fun scanDevices():Observable<BTDevice>{
       val d = Observable.create<BluetoothDevice> {subscriber->
                Log.d("happy", "start scan")
                if(!bluetoothAdapter.isEnabled){
                    subscriber.onError(BluetoothDisabledException())
                }else {

                    val scanner = bluetoothAdapter.bluetoothLeScanner
                    val scanCallback = object : ScanCallback() {
                        override fun onScanFailed(errorCode: Int) {
                            subscriber.onError(Throwable("Error BLE scan. Error code = ${errorCode}"))
                        }

                        override fun onScanResult(callbackType: Int, result: ScanResult?) {
                            if (!subscriber.isDisposed) {
                                result?.let {
                                    subscriber.onNext(it.device)
                                }
                            }
                        }

                        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
                            if (!subscriber.isDisposed) {
                                results?.let {
                                    for (btDevice in it) {
                                        subscriber.onNext(btDevice.device)
                                    }
                                }
                            }
                        }
                    }
                    scanner?.startScan(scanCallback)
                    subscriber.setCancellable() {
                        Log.d("happy", "Unsubscribed: scan")
                        if(bluetoothAdapter.isEnabled) scanner.stopScan(scanCallback)
                    }
                }
       }.doOnSubscribe {
           devices.clear()
       }

        val r = d
                .take(SCAN_TIMEOUT_SECONDS,TimeUnit.SECONDS)
                .share()
                .distinct()
                .doOnNext {device ->
                    devices[device.address] = device
                }

        return r.map{ device -> BTDevice(device.name, device.address) }

    }

    override fun connectDevice(context: Context, address:String): Observable<Pair<ConnectionStateOld,Observable<String>>> {
        val connObservable = Observable.create<Pair<ConnectionStateOld,Observable<BluetoothGattCharacteristic>>> { emitter->

            val bluetoothGattCallback = object: BluetoothGattCallback() {
                var rawDataEmitter:ObservableEmitter<BluetoothGattCharacteristic>?=null



                override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
                    mGatt = null
                    when(newState){
                        BluetoothProfile.STATE_CONNECTED ->{
                            mGatt = gatt
                            gatt?.discoverServices()
                            val rawData = Observable.create<BluetoothGattCharacteristic>{
                                rawDataEmitter = it
                            }
                            emitter.onNext(Pair(ConnectionStateOld.CONNECTED,rawData))
                        }
                        BluetoothProfile.STATE_CONNECTING ->{
                            rawDataEmitter?.onComplete()
                            emitter.onNext(Pair(ConnectionStateOld.CONNECTING,Observable.empty()))
                        }
                        BluetoothProfile.STATE_DISCONNECTING -> {
                            rawDataEmitter?.onComplete()
                            emitter.onNext(Pair(ConnectionStateOld.DISCONNECTING,Observable.empty()))
                        }
                        BluetoothProfile.STATE_DISCONNECTED -> {
                            rawDataEmitter?.onComplete()
                            emitter.onNext(Pair(ConnectionStateOld.DISCONNECTED,Observable.empty()))
                        }
                    }
                }

                override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        val characteristic = gatt
                                ?.getService(supportedUUID)
                                ?.getCharacteristic(characteristicsUUID)

                        gatt?.setCharacteristicNotification(characteristic, true)
                    }
                }

                override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
                    characteristic?.let{
                        rawDataEmitter?.onNext(it)
                    }
                }

//                override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
//                    Log.d("happy", "onCharacteristicWrite value = ${characteristic?.getStringValue(0)}")
//                }



            }
            val device = devices[address]
            var gatt = device?.connectGatt(  context,true,bluetoothGattCallback )
            emitter.onNext(Pair(ConnectionStateOld.CONNECTING, Observable.empty()))
            emitter.setCancellable{
                Log.d("happy","Unsubscribed: connection")

                mGatt?.disconnect()
                mGatt?.close()
                mGatt=null
            }

        }





        return connObservable
                .share()
                .map{t ->
                    Pair(t.first, t.second.map { characteristics ->
                        characteristics.getStringValue(0)?:""
                    })
                }
    }

    override fun disconnect() {
        mGatt?.disconnect()
        mGatt?.close()
        mGatt=null
    }
















}

