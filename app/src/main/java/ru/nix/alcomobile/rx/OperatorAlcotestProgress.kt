package ru.nix.alcomobile.rx

import io.reactivex.ObservableOperator
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import ru.nix.alcomobile.hardware.AlcotestProgress
import ru.nix.alcomobile.hardware.AlcotestProgressType
import ru.nix.alcomobile.hardware.Message
import ru.nix.alcomobile.hardware.applyMessage

class OperatorAlcotestProgress: ObservableOperator<AlcotestProgress, Message> {
    override fun apply(child: Observer<in AlcotestProgress>?): Observer<in Message> {
        return object:Observer<Message>{
            var currentProgress:AlcotestProgress = AlcotestProgress(AlcotestProgressType.UNKNOWN)

            override fun onComplete() {
                child?.onComplete()
            }

            override fun onSubscribe(d: Disposable?) {
                child?.onSubscribe(d)
            }

            override fun onNext(message: Message?) {
               if(message!=null){
                   val newProgress = currentProgress.applyMessage(message)
//                   if(newProgress!=currentProgress){
                       currentProgress = newProgress
                       child?.onNext(newProgress)
//                   }
               }
            }

            override fun onError(e: Throwable?) {
                child?.onError(e)
            }

        }



    }
}