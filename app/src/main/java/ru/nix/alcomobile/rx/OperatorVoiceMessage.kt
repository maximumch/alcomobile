package ru.nix.alcomobile.rx

import android.util.Log
import io.reactivex.ObservableOperator
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import ru.nix.alcomobile.audio.VoiceMessageType
import ru.nix.alcomobile.hardware.AlcotestProgressType
import ru.nix.alcomobile.model.ProcedureProgress
import ru.nix.alcomobile.model.ProcedureProgressType



class OperatorVoiceMessage : ObservableOperator<VoiceMessageType, ProcedureProgress> {
    override fun apply(child: Observer<in VoiceMessageType>): Observer<in ProcedureProgress> {
        return object:Observer<ProcedureProgress>{
            var lastProcedureProgress:ProcedureProgress = ProcedureProgress()

            override fun onComplete() {
                child.onComplete()
            }

            override fun onSubscribe(d: Disposable?) {
                child.onSubscribe(d)
            }

            override fun onNext(procedureProgress: ProcedureProgress) {
                Log.d("happy_voice","ProcedureProgress $procedureProgress")
                if(!procedureProgress.canDisplayPreview) return
                val voiceMessageType = when(procedureProgress.type){
                    ProcedureProgressType.READY -> if(procedureProgress.alcoTestProgress?.type == AlcotestProgressType.READY) VoiceMessageType.PRESS_START_BUTTON else null
                    ProcedureProgressType.WAIT_FOR_EMPLOYEE -> VoiceMessageType.SCAN_PASS
                    ProcedureProgressType.WAIT_FOR_ALCOTEST ->{
                        when(procedureProgress.alcoTestProgress?.type) {
                            //AlcotestProgressType.TIMEOUT -> VoiceMessageType.TIMEOUT_EXPIRED
                            AlcotestProgressType.FLOW_ERROR -> VoiceMessageType.BAD_FLOW
                            AlcotestProgressType.WAIT_FOR_SAMPLE -> VoiceMessageType.CONTINUE_BLOW
                            AlcotestProgressType.WAIT_FOR_FLOW -> VoiceMessageType.BLOW
                            AlcotestProgressType.WAIT_AFTER_FLOW_ERROR -> VoiceMessageType.WAIT
                            AlcotestProgressType.WAIT_AFTER_START-> VoiceMessageType.WAIT
                            AlcotestProgressType.SAMPLE_PROCESSING -> if(procedureProgress.photo!=null)  VoiceMessageType.PROCESSING else null
                            else -> null
                        }
                    }

                    ProcedureProgressType.RESULT -> {
                        procedureProgress.result?.let {result ->
                            if(result>0){
                                VoiceMessageType.POSITIVE_RESULT
                            }else{
                                VoiceMessageType.NEGATIVE_RESULT
                            }
                        }
                    }

                    ProcedureProgressType.ERROR_TIMEOUT -> VoiceMessageType.TIMEOUT_EXPIRED
                    ProcedureProgressType.ERROR_HARDWARE -> VoiceMessageType.HARDWARE_ERROR
                    ProcedureProgressType.WAIT_FOR_CONTACT ->  if(lastProcedureProgress.type!=ProcedureProgressType.WAIT_FOR_CONTACT)  VoiceMessageType.MIRROR_OR_HOLDER_FOLDED else null
                    ProcedureProgressType.ERROR_CONTACTS -> if(lastProcedureProgress.type!=ProcedureProgressType.ERROR_CONTACTS) VoiceMessageType.ERROR_MIRROR_OR_HOLDER_FLIPPED_DURING_TEST else null
                    ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR -> if(lastProcedureProgress.type!=ProcedureProgressType.WAIT_AFTER_CONTACTS_ERROR) VoiceMessageType.WAIT else null
                    else -> null
                }
                voiceMessageType?.also {
                    Log.d("happy_voice","VoiceMessageType $it")
                    child.onNext(it)
                }
                lastProcedureProgress = procedureProgress
            }

            override fun onError(e: Throwable?) {
                child.onError(e)
            }

        }

    }


}