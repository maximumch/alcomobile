package ru.nix.alcomobile


import android.content.*
import android.database.ContentObserver
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.nix.alcomobile.DAO.ErrorData
import ru.nix.alcomobile.DAO.LogData
import ru.nix.alcomobile.DAO.ResultData
import ru.nix.alcomobile.model.*
import ru.nix.alcomobile.utilites.SingleLiveEvent
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

private const val KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS = "key_last_connected_aloctester_address"
private const val KEY_LAST_CONNECTED_ALCOTESTER_NAME = "key_last_connected_aloctester_name"

private const val ACTION_FILE = "ru.nix.replicator.action.FILE"
private const val ACTION_RECORD = "ru.nix.replicator.action.RECORD"

private const val EXTRA_FILE_URI = "ru.nix.replicator.extra.file_uri"
private const val EXTRA_FILE_CLOUD_NAME = "ru.nix.replicator.extra.file_cloud_name"
private const val EXTRA_SOURCE_APP= "ru.nix.replicator.extra.source_app"
private const val EXTRA_RECORD_REQUEST_DATA = "ru.nix.replicator.extra.record_request_data"
private const val EXTRA_RECORD_REQUEST_TYPE = "ru.nix.replicator.extra.record_request_type"
private const val EXTRA_RECORD_REQUEST_ID = "ru.nix.replicator.extra.record_request_id"
private const val RESULT_COMMAND_GUID = "0B0145FF-8EB5-4A23-9F06-4ED739081486"

class RepositoryImplWithReplicator private constructor(context: Context):Repository {
    override val luminanceBelowThresholdObservable: Observable<Boolean>
        get() = throw NotImplementedError()
    override val torchModeObservable: Observable<TorchMode>
        get() = throw NotImplementedError()
    override val torchLiveData: LiveData<Boolean>
        get() = throw NotImplementedError()
    override var luminanceThreshold: Int
        get() = throw NotImplementedError()
        set(value) {}
    override var luminance: Int
        get() = throw NotImplementedError()
        set(value) {throw NotImplementedError()}
    override val torchObservable: Observable<Boolean>
        get() = throw NotImplementedError()
    override val luminanceThresholdLiveData: LiveData<Int>
        get() = throw NotImplementedError()



    override val torchModeLiveData: LiveData<TorchMode>
        get() = throw NotImplementedError()

    override fun changeTorchMode() {
        throw NotImplementedError()
    }

    override suspend fun startUploadLogs(): Repository.ResultForWorker {
        throw NotImplementedError()
    }

    override suspend fun startUploadErrors(): Repository.ResultForWorker {
        throw NotImplementedError()
    }

    override suspend fun startUploadResults(): Repository.ResultForWorker {
        throw NotImplementedError()
    }

    override val certificateError: SingleLiveEvent<String?>
        get() = throw NotImplementedError()
    override val certificate: LiveData<String>
        get() = throw NotImplementedError()
    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val contentResolver = context.contentResolver
    private val appName = context.packageName
    private val _results = MutableLiveData<List<ResultData>>()
    override val results: LiveData<List<ResultData>> = _results

    init{
        Log.d("happyStart", "onCreate 5")
        Observable
                .just(true)
                .delay(100, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe { updateResults() }

        Log.d("happyStart", "onCreate 6")

        contentResolver.registerContentObserver(
                Uri.parse("content://ru.nix.replicator.datasync.provider/record"),
                true,
                RecordsObserver()
                )

        contentResolver.registerContentObserver(
                Uri.parse("content://ru.nix.replicator.datasync.provider/file"),
                true,
                RecordsObserver()
        )


    }

    private fun updateResults(){
        Log.d("happyStart", "updateResults 1")
        val cursor = contentResolver.query(
                Uri.parse("content://ru.nix.replicator.datasync.provider/record"),
                null,
                "sourceApp=?",
                arrayOf(appName),
                null)
        Log.d("happyStart", "updateResults 2")
        val gson = Gson()
        val results = ArrayList<ResultData>()
        Log.d("happyStart", "updateResults 3")
        if (cursor != null) {
            Log.d("happyStart", "updateResults cursorCount = ${cursor.count}")
            cursor.moveToFirst()
            while (cursor.moveToNext()) {
                Log.d("happyStart", "updateResults 4")
                val requestDataString = cursor.getString(cursor.getColumnIndex("requestData"))
                Log.d("happyStart", "updateResults 41")
                val resultData = gson.fromJson<ResultRequest>(requestDataString, ResultRequest::class.java)
                Log.d("happyStart", "updateResults 42")
                if (resultData.CommandGuid == RESULT_COMMAND_GUID) {
                    Log.d("happyStart", "updateResults 43")
                    val recordUploaded = cursor.getInt(cursor.getColumnIndex("done"))!=0
                    Log.d("happyStart", "updateResults 44")
                    val fileUploaded = isFileUploaded(resultData.Data.photo)
                    Log.d("happyStart", "updateResults 45")
                    results.add(resultData.Data.setUploaded(recordUploaded && fileUploaded))
                    Log.d("happyStart", "updateResults 46")
                    _results.postValue(results)
                }
            }
        }
        Log.d("happyStart", "updateResults 5")
        _results.postValue(results)
        Log.d("happyStart", "updateResults 6")
        cursor?.close()
        Log.d("happyStart", "updateResults 7")
    }

    private fun isFileUploaded(fileName:String):Boolean{
        val cursor = contentResolver.query(
                Uri.parse("content://ru.nix.replicator.datasync.provider/file"),
                null,
                "cloudName=?",
                arrayOf(fileName),
                null)
        return if (cursor != null) {
            if(cursor.moveToFirst()) {
                cursor.getInt(cursor.getColumnIndex("done"))!=0
            }else false
        }else false


    }

    override fun uploadFile(file: File){
        val values = ContentValues().apply {
            put("uri",Uri.fromFile(file).toString())
            put("cloudName","alcotest${file.name}")
            put("directionUpload",true)
            put("sourceApp",appName)
        }
        try {
            val resultUri = contentResolver.insert(Uri.parse("content://ru.nix.replicator.datasync.provider/file"), values)
        }catch (e:Exception){
            // replicator app not installed
        }
    }

    override fun uploadResult(photo:File, result: Double, idc:Int, name:String){
        val data = ResultData("alcotest${photo.name}", result,idc, System.currentTimeMillis(),name)
        val request = ResultRequest(RESULT_COMMAND_GUID,data)
        val gson = Gson()
        val str = gson.toJson(request)
        val values = ContentValues().apply {
            put("requestData",str)
            put("requestId", UUID.randomUUID().toString())
            put("requestType","PROCESS")
            put("sourceApp",appName)
        }
        try {
            val resultUri = contentResolver.insert(Uri.parse("content://ru.nix.replicator.datasync.provider/record"), values)



        }catch (e:Exception){
            // replicator app not installed
        }
    }

    override fun uploadLog(msg:String){
        val data = LogData(msg, System.currentTimeMillis())
        val request = LogRequest("349F87E5-8160-4318-9327-E911E71389C8",data)
        val gson = Gson()
        val str = gson.toJson(request)
        val values = ContentValues().apply {
            put("requestData",str)
            put("requestId", UUID.randomUUID().toString())
            put("requestType","PROCESS")
            put("sourceApp",appName)
        }

        try {
            val resultUri = contentResolver.insert(Uri.parse("content://ru.nix.replicator.datasync.provider/record"), values)
        }catch(e:Exception){
            // replicator app not installed
        }


    }

    override fun uploadError(idc:Int, errorId:Int){
        val data = ErrorData(errorId,idc, System.currentTimeMillis())
        val request = ErrorRequest("F58CE482-863A-4136-B545-492F9C101F61",data)
        val gson = Gson()
        val str = gson.toJson(request)
        val values = ContentValues().apply {
            put("requestData",str)
            put("requestId", UUID.randomUUID().toString())
            put("requestType","PROCESS")
            put("sourceApp",appName)
        }
        try {
            contentResolver.insert(Uri.parse("content://ru.nix.replicator.datasync.provider/record"), values)
        }catch(e:Exception){
            // replicator app not installed
        }
    }

    override suspend fun addCertificate(context: Context, fileUri: Uri, password: String) {
        throw NotImplementedError()
    }

    override suspend fun removeCertificate(context: Context) {
        throw NotImplementedError()
    }




    override var lastConnectedAlcotesterAddress:String?
        get(){
            return sharedPreferences.getString(KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS,null)
        }
        set(value) {
            sharedPreferences.edit().putString(KEY_LAST_CONNECTED_ALCOTESTER_ADDRESS,value).apply()
        }

    override var lastConnectedAlcotesterName:String?
        get(){
            return sharedPreferences.getString(KEY_LAST_CONNECTED_ALCOTESTER_NAME,null)
        }
        set(value) {
            sharedPreferences.edit().putString(KEY_LAST_CONNECTED_ALCOTESTER_NAME,value).apply()
        }




    companion object  {
        @Volatile private var instance: RepositoryImplWithReplicator?=null

        fun getInstance(context: Context) =
                instance ?: synchronized(this) {
                    instance ?: RepositoryImplWithReplicator(context).also {
                        instance = it
                    }
                }
    }

    inner class RecordsObserver(): ContentObserver(Handler(Looper.getMainLooper())) {

        private var updateScheduled = false
        private var disposable: Disposable? = null
        override fun onChange(selfChange: Boolean, uri: Uri?) {
            super.onChange(selfChange, uri)
            Log.d("happyobserver", "$uri")
            Log.d("happySchedule", "Thread0: ${Thread.currentThread()}")
            if(!updateScheduled){
                updateScheduled = true
                Observable
                        .just(true)
                        .delay(1000, TimeUnit.MILLISECONDS)
                        .observeOn(Schedulers.io())
                        .subscribe {
                            Log.d("happySchedule", "Thread: ${Thread.currentThread()}")
                            updateResults()
                            updateScheduled=false
                            disposable?.dispose()
                        }.also { disposable = it }
            }
        }
    }
}