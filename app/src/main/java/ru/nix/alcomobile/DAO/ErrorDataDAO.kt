package ru.nix.alcomobile.DAO

import androidx.room.*

@Dao
interface ErrorDataDAO {
    @Query("SELECT * FROM error ORDER BY date desc")
    fun getAll():List<ErrorData>

    @Query("SELECT * FROM error WHERE uploaded = 0 ORDER BY date desc")
    fun getUnuploaded():List<ErrorData>


    @Insert
    fun insert(value:ErrorData)

    @Update
    fun update(value:ErrorData)

    @Delete
    fun delete(value:ErrorData)


}