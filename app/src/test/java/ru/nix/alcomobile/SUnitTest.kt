package ru.nix.alcomobile

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.nix.alcomobile.hardware.*

class SUnitTest {

    @Test
    fun parseMessageTestsLog() {
        val str1 = "\$LOG,SERIAL=0\r\n"
        val result1 = parseMessages(str1)
        assertEquals(1, result1.first.size)
        assertEquals(MessageType.LOG, result1.first[0].messageType)
        assertEquals("SERIAL=0", result1.first[0].params[KEY_LOG_MESSAGE])
    }



    @Test
    fun parseMessageTestsSerial() {
        val str = "\$SERIAL,00007\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.SERIAL, result.first[0].messageType)
        assertEquals("00007", result.first[0].params[KEY_SERIAL])
    }

    @Test
    fun parseMessageTestsGUID() {
        val str = "\$GUID,12345678-1234-1234-1234-123456789012\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.GUID, result.first[0].messageType)
        assertEquals("12345678-1234-1234-1234-123456789012", result.first[0].params[KEY_GUID])
    }

    @Test
    fun parseMessageTestsHARDWARE() {
        val str = "\$HARDWARE,00003\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.HARDWARE, result.first[0].messageType)
        assertEquals("00003", result.first[0].params[KEY_HARDWARE])
    }

    @Test
    fun parseMessageTestsFIRMWARE() {
        val str = "\$FIRMWARE,00003\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.FIRMWARE, result.first[0].messageType)
        assertEquals("00003", result.first[0].params[KEY_FIRMWARE])
    }

    @Test
    fun parseMessageTestsCOUNT() {
        val str = "\$COUNT,00010,00001\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.COUNT_TESTS, result.first[0].messageType)
        assertEquals("00010", result.first[0].params[KEY_COUNT_TESTS_TOTAL])
        assertEquals("00001", result.first[0].params[KEY_COUNT_TESTS_POSITIVE])
    }

    @Test
    fun parseMessageTestsAREF() {
        val str = "\$AREF,01090\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.AREF, result.first[0].messageType)
        assertEquals("01090", result.first[0].params[KEY_AREF])
    }

    @Test
    fun parseMessageTestsPRESSURE() {
        val str = "\$PRESSURE,01090\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.PRESSURE, result.first[0].messageType)
        assertEquals("01090", result.first[0].params[KEY_PRESSURE])
    }

    @Test
    fun parseMessageTestsCALIBRATION() {
        val str = "\$CALIBRATION,01090\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.CALIBRATION, result.first[0].messageType)
        assertEquals("01090", result.first[0].params[KEY_CALIBRATION])
    }

    @Test
    fun parseMessageTestsVoltageTooLow() {
        val str = "\$ERROR,SUPPLY_VOLTAGE_IS_TOO_LOW_(3.0V)\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW, result.first[0].messageType)
        assertEquals("3.0", result.first[0].params[KEY_VOLTAGE])
    }

    @Test
    fun parseMessageTestsVoltageTooHi() {
        val str = "\$ERROR,SUPPLY_VOLTAGE_IS_TOO_HIGH_(6.0V)\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH, result.first[0].messageType)
        assertEquals("6.0", result.first[0].params[KEY_VOLTAGE])
    }

    @Test
    fun parseMessageTestsVoltageNotAllCoilTestsPassed() {
        val str = "\$ERROR,NOT_ALL_COIL_TESTS_PASSED\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_NOT_ALL_COIL_TESTS_PASSED, result.first[0].messageType)
    }

    @Test
    fun parseMessageTestsSupplyVoltageDroppedTooLowWhilePumping() {
        val str = "\$ERROR,SUPPLY_VOLTAGE_DROPPED_TOO_LOW_(3.0V)_WHILE_PUMPING\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING, result.first[0].messageType)
        assertEquals("3.0", result.first[0].params[KEY_VOLTAGE])
    }

    @Test
    fun parseMessageTestsPumpIsNotConnected() {
        val str = "\$ERROR,PUMP_IS_NOT_CONNECTED\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_PUMP_IS_NOT_CONNECTED, result.first[0].messageType)
    }

    @Test
    fun parseMessageTestsPumpCannotTurnOn() {
        val str = "\$ERROR,PUMP_CANNOT_TURN_ON\r\n"
        val result = parseMessages(str)
        assertEquals(1, result.first.size)
        assertEquals(MessageType.ERROR_PUMP_CANNOT_TURN_ON, result.first[0].messageType)
    }









    fun ByteArray.toHexString()=this.joinToString(":"){ String.format("%02X",(it.toInt() and 0xFF)) }
}