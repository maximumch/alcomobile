package ru.nix.alcomobile

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_device_settings.*
import kotlinx.android.synthetic.main.dialog_enter_setting_value.view.*
import ru.nix.alcomobile.hardware.ConnectionState
import ru.nix.alcomobile.modelnew.ProgrammingResponseType

import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.viewmodelsnew.DeviceSettingsViewModel

class DeviceSettingsActivity : AppCompatActivity() {
    private lateinit var viewModel: DeviceSettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = provideViewModel()

        viewModel.procedureProgress.observe(this, Observer {
            serial_textview.text = it.settings.serial?.toString()?:""
            guid_textview.text = it.settings.guid?:""
            hardware_textview.text = it.settings.hardware?:""
            firmware_textview.text = it.settings.firmware?:""
            tests_count_sum_textview.text = it.settings.testsCountTotal?.toString()?:""
            tests_count_positive_textview.text = it.settings.testsCountPositive?.toString()?:""
            tests_count_failed_textview.text = it.settings.testsCountFailed?.toString()?:""
            aref_textview.text = it.settings.aref?.toString()?:""
            pressure_textview.text = it.settings.pressure?.toString()?:""
            calibration_textview.text = it.settings.calibration?.toString()?:""

        })

        viewModel.programmingResponse.observe(this, Observer {
           when(it){
               ProgrammingResponseType.OK -> startMessageDialog(getText(R.string.setting_change_ok_message).toString())
               ProgrammingResponseType.ERROR_SAFETY_BUTTON_NOT_PRESSED -> startMessageDialog(getText(R.string.setting_change_error_safety_button_message).toString())
               ProgrammingResponseType.ERROR_UNKNOWN_COMMAND -> startMessageDialog(getText(R.string.setting_change_error_unknown_command).toString())
               ProgrammingResponseType.ERROR_INVALID_PARAMETER -> startMessageDialog(getText(R.string.setting_change_error_invalid_parameter).toString())
               else -> {}
           }
        })



        viewModel.connectionState.observe(this, Observer{connectionState ->
            if(connectionState!=ConnectionState.CONNECTED){
                startMessageDialog(getString(R.string.message_device_disconnected))
            }


        })

        serial_layout.setOnClickListener {
            startSettingDialog(R.string.serial_number,
                    viewModel.procedureProgress.value?.settings?.serial?.toString()?:"",
                    true,5){value ->
                if(value.length in 1..5) viewModel.setSerial(value.toInt())
            }
        }

        guid_layout.setOnClickListener {
            startSettingDialog(R.string.guid,
                    viewModel.procedureProgress.value?.settings?.guid?:"",
                    false,36){value ->
                if(value.length in 1..36) {
                    viewModel.setGuid(value.replace("-",""))
                }
            }
        }

        aref_layout.setOnClickListener {
            startSettingDialog(R.string.aref,
                    viewModel.procedureProgress.value?.settings?.aref?.toString()?:"",
                    true,5){value ->
                if(value.length in 1..5) { viewModel.setAref(value.toInt()) }
            }
        }

        pressure_layout.setOnClickListener {
            startSettingDialog(R.string.pressure,
                    viewModel.procedureProgress.value?.settings?.pressure?.toString()?:"",
                    true,5){value ->
                if(value.length in 1..5) { viewModel.setPressure(value.toInt()) }
            }
        }

        calibration_layout.setOnClickListener {
            startSettingDialog(R.string.calibration,
                    viewModel.procedureProgress.value?.settings?.calibration?.toString()?:"",
                    true,5){value ->
                if(value.length in 1..5) { viewModel.setCalibration(value.toInt()) }
            }
        }

        val countListener =  View.OnClickListener{
            startOkCancelDialog(R.string.reset_counters_dialog_message){
                viewModel.countsToZero()
            }

        }

        tests_count_sum_layout.setOnClickListener(countListener)
        tests_count_failed_layout.setOnClickListener(countListener)
        tests_count_positive_layout.setOnClickListener(countListener)

    }

    override fun onResume() {
        super.onResume()
        viewModel.startInfo()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId==android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun provideViewModel(): DeviceSettingsViewModel {
        val factory = InjectorUtils.provideDeviceSettingsViewModelFactory(this)
        return ViewModelProviders.of(this,factory).get(DeviceSettingsViewModel::class.java)
    }

    private fun startSettingDialog(titleResId:Int, valueHint:String, isNumber:Boolean, maxLength:Int,
                                   listener:(String) -> Unit
                                   ){
        val builder = AlertDialog.Builder(this)
        val inflater = LayoutInflater.from(builder.context)
        val convertView = inflater.inflate(R.layout.dialog_enter_setting_value,null)
        convertView.settings_value.hint = valueHint
        convertView.settings_value.setOnKeyListener(){ view: View, i: Int, keyEvent: KeyEvent ->
            if(view is TextView){
                view.text.length>=maxLength
            }else{
                true
            }
        }
        if(isNumber) {
            convertView.settings_value.inputType = InputType.TYPE_CLASS_NUMBER
        }else{
            convertView.settings_value.inputType = InputType.TYPE_CLASS_TEXT
        }

        builder.setTitle(titleResId)
                .setView(convertView)
                .setNegativeButton(R.string.cancel){ dialogInterface: DialogInterface, i: Int ->  }
                .setPositiveButton("OK"){ dialogInterface: DialogInterface, i: Int ->
                    convertView.settings_value.text.toString().apply(listener)
                }
                .show()




    }

    private fun startMessageDialog(message:String){
        val builder = AlertDialog.Builder(this)
        builder.setPositiveButton("OK",null)
                .setMessage(message)
                .show()
    }

    private fun startOkCancelDialog(messageResId: Int, okListener:() -> Unit){
        val builder = AlertDialog.Builder(this)
        builder.setPositiveButton("OK"){ dialogInterface: DialogInterface, i: Int -> okListener.invoke()}
                .setNegativeButton(R.string.cancel,null)
                .setMessage(messageResId)
                .show()






//
//            dialogInterface: DialogInterface, i: Int ->
//            okListener.invoke()
//        }



    }
}
