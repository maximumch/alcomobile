package ru.nix.alcomobile


import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import org.junit.Test
import ru.nix.alcomobile.rx.ReliableWriteAction
import ru.nix.alcomobile.rx.chunk
import ru.nix.alcomobile.rx.createReliableWriter
import java.util.concurrent.TimeUnit


class RxTests {

    @Test
    fun parseMessageTestsLog() {
        val inputData = Observable.just("abcdefghijklmnopqrstuvwxyz1234567890", "0987654321qwertyuioplkjhgfdsazxcvbnm")
        val btWriter = BTWriter()
//        btWriter.writtenDataStream.doOnSubscribe {
//            println()
//        }
//
//        btWriter.reliableWriteCompleted.doOnSubscribe {
//            println()
//        }
//
//        btWriter.writtenDataStream.subscribe {
//            println(it)
//        }
//        btWriter.reliableWriteCompleted.subscribe {
//            println("$it")
//        }

        btWriter.writtenDataStream.doOnNext {
            println()
        }
        btWriter.reliableWriteCompleted.doOnNext {
            println()
        }


        val dataToStartWriteStream = Observable.zip(
                inputData.chunk(10),
                Observable.concat(
                        Observable.just(true),
                        btWriter.reliableWriteCompleted
                ),
                BiFunction { str: String, _: Boolean -> str }
        ).subscribe {
            println()
            btWriter.commitReliableWrite()
        }

//        createReliableWriter(
//                dataToStartWriteStream,
//                btWriter.writtenDataStream
//        ).subscribe {action->
//            when(action.first){
//                ReliableWriteAction.WRITE -> {  btWriter.writeDataReliable(action.second) }
//                ReliableWriteAction.COMMIT -> { btWriter.commitReliableWrite()  }
//                ReliableWriteAction.ABORT -> { btWriter.abortReliableWrite()  }
//                else -> {throw IllegalStateException()}
//        }

    }




    class BTWriter{
        private val _writtenDataStream = PublishSubject.create<String>()
        val writtenDataStream:Observable<String> = _writtenDataStream
        private val _reliableWriteCompleted = PublishSubject.create<Boolean>()
        val reliableWriteCompleted:Observable<Boolean> = _reliableWriteCompleted

        private var writeSessionActive:Boolean = false
        private var writeCount = 0



        fun writeDataReliable(str:String){
            if(!writeSessionActive){
                writeSessionActive = true
                if(writeCount % 2 == 0){
                    _writtenDataStream.onNext(str)
                }else{
                    _writtenDataStream.onNext("_$str")
                }
                writeCount++
            }else{
                throw  IllegalStateException("Write data when session is active")
            }
        }

        fun commitReliableWrite(){
            _reliableWriteCompleted.onNext(true)
            writeSessionActive = false

        }

        fun abortReliableWrite(){
            _reliableWriteCompleted.onNext(false)
            writeSessionActive = false
        }
    }











    fun ByteArray.toHexString()=this.joinToString(":"){ String.format("%02X",(it.toInt() and 0xFF)) }
}