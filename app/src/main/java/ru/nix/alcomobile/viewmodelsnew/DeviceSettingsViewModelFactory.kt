package ru.nix.alcomobile.viewmodelsnew

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.nix.alcomobile.Director


class DeviceSettingsViewModelFactory(
        private val director: Director
): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DeviceSettingsViewModel(director) as T
    }
}