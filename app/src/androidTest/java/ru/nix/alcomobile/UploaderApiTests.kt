package ru.nix.alcomobile

import android.content.Context
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.RequestBody
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import ru.nix.alcomobile.api.RESPONSE_STATUS_ABSENT
import ru.nix.alcomobile.api.RESPONSE_STATUS_COMPLETE
import ru.nix.alcomobile.api.UploaderApi
import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.utilites.SslUtils
import ru.nix.replicator.createRandomFile
import java.io.FileInputStream
import java.io.InputStream
import java.util.*
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

const val  CLIENT_CERT_PASSWORD = "IijIkejje33j"
const val CLIENT_CERT_PASSWORD_22 = "kmkdhdj9jd"
private const val BUFFER_SIZE = 128*1024

@RunWith(AndroidJUnit4::class)
class UploaderApiTests {
    lateinit var  appContext: Context
    lateinit var uploaderApi: UploaderApi
    lateinit var sslContext: SSLContext
    lateinit var x509TrustManager: X509TrustManager
    lateinit var repository: Repository

    @Before
    fun initServices(){
        appContext = InstrumentationRegistry.getTargetContext()
        sslContext = SslUtils.getSslContext(getCertificateStream("client22"), CLIENT_CERT_PASSWORD_22)
        //sslContext = SslUtils.getSslContext(getCertificateStream("client4"), CLIENT_CERT_PASSWORD)
        //sslContext = SslUtils.getSslContext(getCertificateStream("client18"), CLIENT_CERT_PASSWORD_18)
        //sslContext = SslUtils.getSslContext(getCertificateStream("client19"), CLIENT_CERT_PASSWORD_19)
        //sslContext = SslUtils.getSslContext(getCertificateStreamServerStage(), CLIENT_CERT_PASSWORD_SERVER_STAGE)
        x509TrustManager = SslUtils.trustAllCerts[0] as  X509TrustManager
        uploaderApi = UploaderApi.create(sslContext,x509TrustManager)
        repository = InjectorUtils.provideRepository(appContext)
    }

    private fun getCertificateStream(name:String): InputStream {
        val certResId = appContext.resources.getIdentifier(name,"raw",appContext.packageName)
        return  appContext.resources.openRawResource(certResId)
    }

    @Test
    fun checkIdentity(){
        runBlocking {
            val response = uploaderApi.identityAsync().await()
            //val response = uploaderApi.identity().execute()
            val result = response.message()
            assertTrue(response.isSuccessful)
            assertFalse(response.body().isNullOrEmpty())
        }
    }

    @Test
    fun randomFileUpload(){
        //val fileName = UUID.randomUUID().toString()
        val fileName = "randomFile455667_1"
        val file = createRandomFile(appContext,1024)

        runBlocking {
            var response: UploaderApi.MResponse = uploaderApi.statusAsync(fileName).await().body()
                    ?: throw IllegalStateException("uploaderApi.statusAsync with null body")
            var offset:Int = if (response.status == RESPONSE_STATUS_ABSENT) {
                uploaderApi.uploadDescriptorAsync(fileName, file.length().toInt()).await().body()?.size?: throw IllegalStateException("uploaderApi.uploadDescriptorAsync with null body")
            } else {
                response.size?: throw IllegalStateException("uploaderApi.statusAsync with null size")
            }

            var bytesRead:Int=0

            val inputStream = FileInputStream(file).apply {
                if (offset > 0) skip(offset.toLong())
            }

            inputStream.use {
                val buffer = ByteArray(BUFFER_SIZE)

                while (it.read(buffer).also { bytesRead = it } != -1) {
                    val requestBody =
                            RequestBody.create(MediaType.parse("application/octet-stream"), buffer, 0, bytesRead)
                    response = uploaderApi.uploadPartAsync(fileName, offset, bytesRead, requestBody).await().body()
                            ?: throw java.lang.IllegalStateException("uploaderApi.uploadPartAsync return null body")
                    offset += bytesRead
                }
                check(response.status == RESPONSE_STATUS_COMPLETE){"Last part upload call mast return status COMPLETE"}
            }
        }

        assertEquals(true,true)
    }

    @Test
    fun startUploadResults(){
        runBlocking {
            repository.startUploadResults()
        }

    }

    @Test
    fun startUploadLogs(){
        runBlocking {
            repository.startUploadLogs()
        }

    }

    @Test
    fun startUploadErrors(){
        runBlocking {
            repository.startUploadErrors()
        }

    }



}