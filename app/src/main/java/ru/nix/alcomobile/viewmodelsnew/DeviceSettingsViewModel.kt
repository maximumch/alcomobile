package ru.nix.alcomobile.viewmodelsnew

import androidx.lifecycle.ViewModel
import ru.nix.alcomobile.Director


class DeviceSettingsViewModel internal constructor(
        //private val cameraManager: CameraManager
        private val director: Director
): ViewModel()  {
//    val serial = director.serial
//    val guid = director.guid
//    val hardware = director.hardware
//    val firmware = director.firmware
//    val testsCountTotal = director.testsCountTotal
//    val testsCountPositive = director.testsCountPositive
//    val testsCountFailed = director.testsCountFailed
//    val aref = director.aref
//    val pressure = director.pressure
//    val calibration = director.calibration
//    val gotOk = director.gotOK
//    val gotErrorSafetyButton= director.gotErrorSafetyButton
//    val gotHardwareError = director.gotHardwareError
    val connectionState = director.connectionState
    val procedureProgress = director.procedureProgress
    val programmingResponse = director.programmingResponse



    fun startInfo() = director.startInfo()
    fun setSerial(serial:Int) = director.setSerial(serial)
    fun setGuid(guid:String) = director.setGuid(guid)
    fun setAref(aref:Int)=director.setAref(aref)
    fun setPressure(pressure:Int) = director.setPressure(pressure)
    fun setCalibration(calibration:Int) = director.setCalibration(calibration)
    fun countsToZero() = director.countsToZero()

}