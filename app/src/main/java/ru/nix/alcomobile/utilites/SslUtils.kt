package ru.nix.alcomobile.utilites

import java.io.InputStream
import java.lang.Exception
import java.security.KeyStore
import java.security.cert.CertificateException
import javax.net.ssl.*

object SslUtils {
    // Create a trust manager that does not validate certificate chains
    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        @Throws(CertificateException::class)
        override fun checkClientTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String
        ) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String
        ) {
        }

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
            return arrayOf()
        }
    })

    fun getSslContext(inputStream: InputStream, clientCertPassword:String): SSLContext{
        val keyStore = KeyStore.getInstance("PKCS12")

        keyStore.load(inputStream,clientCertPassword.toCharArray());
        val kmf = KeyManagerFactory.getInstance("X509")
        kmf.init(keyStore, clientCertPassword.toCharArray())
        val keyManagers = kmf.keyManagers
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(keyManagers, null, null)
        return sslContext
    }


//    val unsafeOkHttpClient: OkHttpClient
//        get() {
//            try {
//                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
//                    @Throws(CertificateException::class)
//                    override fun checkClientTrusted(
//                        chain: Array<java.security.cert.X509Certificate>,
//                        authType: String
//                    ) {
//                    }
//
//                    @Throws(CertificateException::class)
//                    override fun checkServerTrusted(
//                        chain: Array<java.security.cert.X509Certificate>,
//                        authType: String
//                    ) {
//                    }
//
//                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
//                        return arrayOf()
//                    }
//                })
//                val sslContext = SSLContext.getInstance("SSL")
//                sslContext.init(null, trustAllCerts, java.security.SecureRandom())
//                val sslSocketFactory = sslContext.socketFactory
//
//                val builder = OkHttpClient.Builder()
//                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
//                builder.hostnameVerifier { hostname, session -> true }
//
//                return builder.build()
//            } catch (e: Exception) {
//                throw RuntimeException(e)
//            }
//
//        }
}