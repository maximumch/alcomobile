package ru.nix.alcomobile.camera

import android.content.Context
import android.hardware.Camera
import android.os.Environment
import android.util.Log
import android.view.SurfaceHolder
import com.google.zxing.BinaryBitmap
import com.google.zxing.MultiFormatReader
import com.google.zxing.NotFoundException
import com.google.zxing.PlanarYUVLuminanceSource
import com.google.zxing.common.HybridBinarizer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.nix.alcomobile.modelnew.CameraCommandType
import ru.nix.alcomobile.utilites.InjectorUtils
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean
import java.util.regex.Pattern
import kotlin.coroutines.CoroutineContext

@Suppress("DEPRECATION")
class CameraOldHelper(context: Context):CameraHelper {
    private var mCamera: Camera?=null
    private var mSurfaceHolder:SurfaceHolder?=null
    private val d = CompositeDisposable()
    private val qrDecodeExecutor = Executors.newSingleThreadExecutor()
    private val dirForPictures = InjectorUtils.dirForPictures
    private val isPictureTaking = AtomicBoolean(false)
    private val isCameraDeviceFree = AtomicBoolean(true)


    override var surfaceHoldersStream:Observable<SurfaceHolder> = Observable.empty()
    set(value){
        field = value
        previewSubscribe()
    }

    override var cameraCommandTypeStream:Observable<CameraCommandType> = Observable.empty()
    set(value) {
        field = value
        previewSubscribe()
    }

    override val qrCodeGot: PublishSubject<Employee> = PublishSubject.create<Employee>()
    override val photoTaken: PublishSubject<File> = PublishSubject.create<File>()
    override val cameraError: PublishSubject<Throwable> = PublishSubject.create<Throwable>()


    private fun previewSubscribe() {
        surfaceHoldersStream.subscribe { mSurfaceHolder = it }
        cameraCommandTypeStream
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when(it) {
                        CameraCommandType.START_PREVIEW -> if (isCameraDeviceFree.get()) startCamera()
                        CameraCommandType.GET_QR_CODE ->{
                            if (isCameraDeviceFree.get()) startCamera()
                            if (!isCameraDeviceFree.get()) cameraQrCodeStart()
                        }
                        CameraCommandType.TAKE_PHOTO -> if (!isCameraDeviceFree.get()) cameraTakePhoto()
                        CameraCommandType.STOP_PREVIEW -> if (!isCameraDeviceFree.get()) stopCamera()
                        else -> {
                        }
                    }
                }
    }

    private fun cameraTakePhoto() {
        mCamera?.also { camera ->
            takePicture(camera)
                    .subscribe({
                        Log.d("xxx_happy", "Photo = $it")
                        photoTaken.onNext(it)
                        d.clear()

                    },{
                        cameraError.onNext(it)
                    },{}).also { d.add(it) }
        }
    }

    private fun cameraQrCodeStop() {
        mCamera?.also { camera ->
            camera.setPreviewCallback(null)
            d.clear()
        }
    }

    private fun cameraQrCodeStart() {
        mCamera?.also { camera ->
            camera.setPreviewCallback(null)

            fromPreviewCallback(camera)
                    .subscribe({
                        Log.d("xxx_happy", "QR CODE = $it")
                        qrCodeGot.onNext(it)
                        cameraQrCodeStop()
                        d.clear()
                    },{
                        cameraError.onNext(it)
                    },{}).also { d.add(it) }
        }
    }

    private fun stopCamera() {
        Log.d("happyCamera", "stopCamera")

        mCamera?.setPreviewCallback(null)
        mCamera?.stopPreview()
        mCamera?.release()
        mCamera = null
        isCameraDeviceFree.set(true)
    }

    private fun startCamera(){
        try{
            mSurfaceHolder?.let { surfaceHolder ->

                val camera = Camera.open(getFrontCameraId())
                Log.d("happy_camera", "camera = $camera")


                val parameters = camera.parameters
                val modes = parameters.supportedFocusModes
                val sizes = parameters.supportedPreviewSizes
                if (parameters.supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)
                }

                parameters.supportedPreviewSizes
                        //.filter {(it.width*16 == it.height*9) || (it.width*9 == it.height*16)}
                        .filter { (it.width * 4 == it.height * 3) || (it.width * 3 == it.height * 4) }
                        .filter { it.width * it.height > 500000 }
                        .sortedBy { it.width * it.height }
                        .first()
                        .also { parameters.setPreviewSize(it.width, it.height) }


                parameters.supportedPictureSizes
                        .filter { (it.width * 4 == it.height * 3) || (it.width * 3 == it.height * 4) }
                        .sortedByDescending { it.width }
                        .first()
                        .also { parameters.setPictureSize(it.width, it.height) }

                parameters.setRotation(270)
                camera.parameters = parameters
                camera.setDisplayOrientation(90)
                camera.setPreviewDisplay(surfaceHolder)


                camera.startPreview()
                mCamera = camera
                isCameraDeviceFree.set(false)
            }

        }catch (e:Exception){
            cameraError.onNext(e)
        }
    }


    private fun getFrontCameraId() = getCameraWithFacing(Camera.CameraInfo.CAMERA_FACING_FRONT)

    private fun getCameraWithFacing(facing:Int):Int{
        val info = Camera.CameraInfo()

        for(i in 0 until Camera.getNumberOfCameras()){
            Camera.getCameraInfo(i,info)
            if(info.facing==facing){
                return i
            }
        }

        return 0
    }

    private fun fromPreviewCallback(camera:Camera): Observable<Employee>{
        val multiFormatReader = MultiFormatReader()
        val decodeInProcess = AtomicBoolean(false)

        return Observable.create {emitter ->
            try {
                camera.setPreviewCallback { data, camera ->
                    if (!emitter.isDisposed && decodeInProcess.compareAndSet(false, true)) {
                        qrDecodeExecutor.execute {
                            Log.d("xxx_happy", "QR Decode start thread = ${Thread.currentThread().name}")
                            val parameters = camera.parameters
                            val size = parameters.previewSize
                            val luminanceSource = PlanarYUVLuminanceSource(data, size.width, size.height, 0, 0, size.width, size.height, false)
                            val hybridBinarizer = HybridBinarizer(luminanceSource)
                            val binaryBitmap = BinaryBitmap(hybridBinarizer)
                            try {
                                val result = multiFormatReader.decode(binaryBitmap)
                                val employee = Employee.parse(result.text)
                                employee?.also { emitter.onNext(it) }
                            } catch (ex: NotFoundException) {
                                
                            }
                            decodeInProcess.set(false)
                            Log.d("xxx_happy", "QR Decode stop size:${size.width}, ${size.height}")
                        }
                    }

                }
            }catch (e:Exception){
                emitter.onError(e)
            }
        }
    }

    private fun takePicture(camera:Camera):Observable<File>{
        if(isPictureTaking.compareAndSet(false,true)) {
            return Observable.create { emitter ->
                Log.d("happynew", "Taking picture thread=${Thread.currentThread().name}")
                try {
                    camera.takePicture(null, null, Camera.PictureCallback { bytes, camera1 ->
                        CoroutineScope(Dispatchers.IO).launch {
                            Log.d("happynewCamera", "Taking picture callback thread=${Thread.currentThread().name}")
                            if (bytes != null) {
                                val fileName = "alcotest${UUID.randomUUID()}.jpg" //System.currentTimeMillis().toString()
                                val file = File(dirForPictures, fileName)
                                if (!file.exists()) {
                                    try {
                                        file.createNewFile()
                                        FileOutputStream(file).use { fileOutputStream ->
                                            fileOutputStream.write(bytes, 0, bytes.size)
                                        }
                                        emitter.onNext(file)
                                        //emitter.onComplete()
                                    } catch (e: FileNotFoundException) {
                                        emitter.onError(e)
                                    } catch (e: IOException) {
                                        emitter.onError(e)
                                    }
                                }
                            }
                            isPictureTaking.set(false)
                            Log.d("happynew", "Finish taking picture callback")
                            Thread.sleep(300)
                            camera1.startPreview()
                        }
                    })
                }catch (e:Exception){
                    emitter.onError(e)
                }
                Log.d("happynew", "Finish taking picture")

            }
        }else{
            //return Observable.empty()
            return Observable.error<File>(IOException("Camera is in use"))
        }

    }

}

data class Employee(val idc:Int, val name:String){
    companion object{
        private const val pattern = "\\A(35)(%)(\\d+)(%)(\\d+)([\\s]+)([\\S])([\\s\\S]*)\\z"
        private const val group_employee_id = 3
        private const val groups_employee_name_1 = 7
        private const val groups_employee_name_2 = 8

        fun parse(str:String):Employee? {

            val p = Pattern.compile(pattern)
            val m = p.matcher(str)
            if(m.find()){
                val idc = Integer.valueOf(m.group(group_employee_id))
                val name = (m.group(groups_employee_name_1) + m.group(groups_employee_name_2)).trim()
                return Employee(idc.toInt(), name)
            }
            return null
        }
    }



}


