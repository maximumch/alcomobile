package ru.nix.alcomobile.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import android.content.Context
import android.view.SurfaceHolder
import androidx.lifecycle.viewModelScope
import io.reactivex.Observer
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.launch
import ru.nix.alcomobile.Director
import ru.nix.alcomobile.audio.VoiceMessenger
import ru.nix.alcomobile.camera.CameraDataType
import ru.nix.alcomobile.hardware.ConnectionState
import ru.nix.alcomobile.model.ProcedureProgress

class AlcotestViewModel internal constructor(
        //private val cameraManager: CameraManager
        private val director: Director
): ViewModel() {
    private val mOnSurfaceAvailable = PublishSubject.create<SurfaceHolder>()
//    val onSurfaceAvailable: Observer<SurfaceHolder> get() = mOnSurfaceAvailable
    private val mOnSurfaceDestroyed = PublishSubject.create<Boolean>()
//    val onSurfaceDestroyed: Observer<Boolean> get() = mOnSurfaceDestroyed
//    val cameraFreeObservable get() = director.cameraFreeObservable

    //val connectPreview: Observable<Boolean> get() = director.connectPreview(mOnSurfaceAvailable,mOnSurfaceDestroyed)

    //val bluetoothDisabledError: LiveData<Boolean> = director.bluetoothDisabledError
    //val alcotestProgress:LiveData<AlcotestProgress> = director.alcotestProgress
    val connectionState:LiveData<ConnectionState> = director.connectionState

    //val procedureProgress:LiveData<ProcedureProgress> = director.procedureProgress
    //val resultPhoto:LiveData<ProcedureProgress> = director.result
//    val gotHardwareError = director.gotHardwareError
//    val contactState = director.contactState
//    val voiceMessageSingleLiveEvent = director.voiceMessageSingleLiveEvent
//    val voiceMessageObservable = director.voiceMessage
//    var voiceMessenger:VoiceMessenger?=null
//    private var mNeedQRCode = false

//    fun initVoiceMessenger(context:Context){
//        if(voiceMessenger==null || voiceMessenger?.isDisposed == true) {
//            viewModelScope.launch {
//                voiceMessenger = VoiceMessenger.getInstance(context)
//                voiceMessenger?.setInputStream(voiceMessageObservable)
//            }
//        }
//    }

//    fun disposeVoiceMessenger(){
//        voiceMessenger?.dispose()
//        voiceMessenger = null
//    }



//    fun startDevicesScan()=director.scanDevices()
//    fun startTest() = director.startTest()
//
//    fun xxx_onSurfaceHolderCreated(surfaceHolder:SurfaceHolder) =
//            director.surfaceFromActivityObserver.onNext(surfaceHolder)
//    fun xxx_onActivityCanDisplayPreviewChanged(canDisplay:Boolean) =
//            director.activityCanDisplayPreviewObserver.onNext(canDisplay)
//
//    fun xxx_setNeedImage() = director.xxx_setNeedDataFromCamera(CameraDataType.IMAGE)
//    fun xxx_flushNeedImage() = director.xxx_setNeedDataFromCamera(CameraDataType.NONE)
//
//    fun xxx_needQRCode() {
//        mNeedQRCode = !mNeedQRCode
//        director.needQRCodeObserver.onNext(mNeedQRCode)
//    }
//    fun xxx_needPhoto() =  director.needPhotoObserver.onNext(true)
//    fun reconnect(context: Context) = director.reconnect(context)


}