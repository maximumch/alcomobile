package ru.nix.alcomobile.rxnew


import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.subjects.PublishSubject
import ru.nix.alcomobile.TorchMode
import ru.nix.alcomobile.audio.VoiceMessageType
import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.Command
import ru.nix.alcomobile.hardware.ConnectionState
import ru.nix.alcomobile.hardware.Message
import ru.nix.alcomobile.modelnew.*
import java.io.File

data class ProcedureProgressOutputStreams(
        val procedureProgressStream:Observable<ProcedureProgress>,
        val hardwareInputStream:Observable<Command>,
        val cameraCommandStream:Observable<CameraCommandType>,
        val textMessageStream:Observable<TextMessageType>,
        val voiceMessageStream:Observable<VoiceMessageType>,
        val alcotestResultStream:Observable<AlcotestResult>,
        val alcotestErrorStream:Observable<AlcotestError>,
        val messageForLogStream:Observable<String>,
        val programmingResponseStream:Observable<ProgrammingResponseType>
)

fun createProcedureProgressNew(
        messages: Observable<Message>,
        employees: Observable<Employee>,
        photos: Observable<File>,
        connectionStates: Observable<ConnectionState>,
        activityCanDisplayPreview: Observable<Boolean>,
        cameraError:Observable<Throwable>
): ProcedureProgressOutputStreams {
    val procedureProgressStream = PublishSubject.create<ProcedureProgress>()
    val hardwareInputStream = PublishSubject.create<Command>()
    val cameraCommandStream = PublishSubject.create<CameraCommandType>()
    val textMessageStream = PublishSubject.create<TextMessageType>()
    val voiceMessageStream = PublishSubject.create<VoiceMessageType>()
    val alcotestResultStream = PublishSubject.create<AlcotestResult>()
    val alcotestErrorStream = PublishSubject.create<AlcotestError>()
    val messageForLogStream = PublishSubject.create<String>()
    val infoMessageStream = PublishSubject.create<AlcotesterSettings>()
    val programmingResponseStream = PublishSubject.create<ProgrammingResponseType>()
    var resultSet = ResultSet(ProcedureProgress())
    val lock=Any()

    fun newResultSet(newResultSet:ResultSet){
        synchronized(lock) {
            newResultSet.procedureProgress.let { procedureProgressStream.onNext(it) }
            newResultSet.command?.let { hardwareInputStream.onNext(it) }
            newResultSet.cameraCommandType?.let { cameraCommandStream.onNext(it) }
            newResultSet.textMessageType?.let { textMessageStream.onNext(it) }
            newResultSet.voiceMessageType?.let { voiceMessageStream.onNext(it) }
            newResultSet.alcotestResult?.let { alcotestResultStream.onNext(it)}
            newResultSet.alcotestError?.let { alcotestErrorStream.onNext(it)}
            newResultSet.messageForLog?.let { messageForLogStream.onNext(it)}

            newResultSet.programmingResponse?.let{programmingResponseStream.onNext(it)}
            resultSet = newResultSet
        }
    }

    messages.subscribe { newResultSet(resultSet.procedureProgress.applyMessage(it)) }
    employees.subscribe { newResultSet(resultSet.procedureProgress.applyEmployee(it)) }
    photos.subscribe { newResultSet(resultSet.procedureProgress.applyPhoto(it)) }
    connectionStates.subscribe { newResultSet(resultSet.procedureProgress.applyConnectionState(it)) }
    activityCanDisplayPreview.subscribe { newResultSet(resultSet.procedureProgress.applyCanDisplayPreview(it)) }
    cameraError.subscribe{newResultSet(resultSet.procedureProgress.applyCameraError(it))}

    return ProcedureProgressOutputStreams(
            procedureProgressStream,
            hardwareInputStream,
            cameraCommandStream,
            textMessageStream,
            voiceMessageStream,
            alcotestResultStream,
            alcotestErrorStream,
            messageForLogStream,
            programmingResponseStream)
}

fun createTorchOnOffStream(
        cameraCommandStream:Observable<CameraCommandType>,
        luminanceBelowThresholdStream:Observable<Boolean>,
        torchModeStream:Observable<TorchMode>
):Observable<Boolean>{
    return Observable.combineLatest(
            cameraCommandStream.map {
                when(it){
                    CameraCommandType.START_PREVIEW, CameraCommandType.GET_QR_CODE, CameraCommandType.TAKE_PHOTO -> true
                    CameraCommandType.STOP_PREVIEW -> false
                }
            }.distinctUntilChanged(),
            luminanceBelowThresholdStream,
            torchModeStream,
            Function3{ cameraOn: Boolean, luminanceBelowThreshold: Boolean, torchMode: TorchMode ->
               when{
                   cameraOn && luminanceBelowThreshold && torchMode==TorchMode.AUTO -> true
                   else -> false
               }
            }
    ).distinctUntilChanged()
}
