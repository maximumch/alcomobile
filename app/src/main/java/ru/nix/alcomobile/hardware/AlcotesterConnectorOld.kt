package ru.nix.alcomobile.hardware

import android.content.Context
import io.reactivex.Observable

interface AlcotesterConnector {
    fun scanDevices():Observable<BTDevice>
    fun connectDevice(context: Context, address:String): Observable<Pair<ConnectionStateOld,Observable<String>>>
    fun disconnect()

}

enum class ConnectionStateOld(){
    DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING
}
