package ru.nix.alcomobile.DAO

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ResultDataDAO {
    @Query("SELECT * FROM result ORDER BY date desc")
    fun getAll():List<ResultData>

    @Query("SELECT * FROM result WHERE uploaded = 0 ORDER BY date desc")
    fun getUnuploaded():List<ResultData>

    @Query("SELECT * FROM result ORDER BY date desc limit 1000 ")
    fun getAllLiveData(): LiveData<List<ResultData>>

    @Insert
    fun insert(value:ResultData)

    @Update
    fun update(value:ResultData)

    @Delete
    fun delete(value:ResultData)


}