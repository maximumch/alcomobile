package ru.nix.alcomobile.modelnew

import ru.nix.alcomobile.audio.VoiceMessageType
import ru.nix.alcomobile.camera.Employee
import ru.nix.alcomobile.hardware.*
import java.io.File
import kotlin.IllegalArgumentException

enum class ProcedureProgressType{
    UNKNOWN, WAIT_FOR_READY, READY, WAIT_FOR_EMPLOYEE, WAIT_FOR_ALCOTEST
}

enum class CameraCommandType{
    START_PREVIEW, STOP_PREVIEW, TAKE_PHOTO, GET_QR_CODE
}

enum class TextMessageType{
    WAIT, PRESS_START_BUTTON, SCAN_EMPLOYEE_QR_CODE, BLOW, KEEP_BLOW, BAD_BLOW, PROCESSING, UNFOLD_MIRROR_AND_HOLDER,
    MIRROR_OR_HOLDER_FOLDED, CAMERA_ERROR, NO_PICTURE_RESULT, LOW_VOLTAGE
}



enum class AlcotestErrorType(val code: Int) { TIMEOUT_EXPIRED(1), BAD_BLOW(2), SOFTWARE(7), HARDWARE(6);

}

enum class ProgrammingResponseType{
    ERROR_SAFETY_BUTTON_NOT_PRESSED, OK, ERROR_UNKNOWN_COMMAND,ERROR_INVALID_PARAMETER
}

data class ProcedureProgress(
        val type:ProcedureProgressType = ProcedureProgressType.UNKNOWN,
        val alcoTestProgress: AlcotestProgress =AlcotestProgress(),
        val employee: Employee?=null,
        val photo:File? = null,
        val contactState:ContactState = ContactState(),
        val canDisplayPreview: Boolean = true,
        val connectionState: ConnectionState = ConnectionState.DISCONNECTED,
        val settings: AlcotesterSettings  = AlcotesterSettings()
)


data class ResultSet(
        val procedureProgress: ProcedureProgress,
        val command: Command? = null,
        val cameraCommandType: CameraCommandType? = null,
        val textMessageType: TextMessageType? = null,
        val voiceMessageType: VoiceMessageType? = null,
        val alcotestResult: AlcotestResult? = null,
        val alcotestError: AlcotestError? = null,
        val messageForLog:String?=null,
        val programmingResponse:ProgrammingResponseType? = null
)

data class AlcotestResult(
        val employee: Employee,
        val concentration: Double,
        val success:Boolean,
        val photo:File
)

data class AlcotestError(
        val employee: Employee,
        val type: AlcotestErrorType
)

data class AlcotesterSettings(
    val serial:Int? = null,
    val guid:String? = null,
    val hardware:String? = null,
    val firmware:String? = null,
    val testsCountTotal:Int? = null,
    val testsCountPositive:Int? = null,
    val testsCountFailed:Int? = null,
    val aref:Int? = null,
    val pressure:Int? = null,
    val calibration:Int? = null
)

fun ProcedureProgress.update(
        type:ProcedureProgressType = this.type,
        alcoTestProgress:AlcotestProgress = this.alcoTestProgress,
        employee: Employee? = this.employee,
        photo:File? = this.photo,
        contactState:ContactState = this.contactState,
        canDisplayPreview: Boolean = this.canDisplayPreview,
        connectionState: ConnectionState = this.connectionState,
        settings: AlcotesterSettings = this.settings
)= ProcedureProgress(type, alcoTestProgress, employee, photo, contactState, canDisplayPreview,connectionState, settings)

fun ProcedureProgress.updateAndClearAlcotestProgress(
        type:ProcedureProgressType = this.type,
        contactState:ContactState = this.contactState,
        canDisplayPreview: Boolean = this.canDisplayPreview,
        connectionState: ConnectionState = this.connectionState,
        settings: AlcotesterSettings = this.settings
) = ProcedureProgress(type = type, contactState =  contactState, canDisplayPreview =  canDisplayPreview, connectionState = connectionState, settings = settings)

fun ProcedureProgress.applyMessage(messageToApply: Message):ResultSet{

    return when {
        connectionState!=ConnectionState.CONNECTED -> applyConnectionState(ConnectionState.CONNECTED)
        messageToApply.isAlcotestProgress() -> applyAlcotestProgress(alcoTestProgress.applyMessage(messageToApply))
        messageToApply.isContactState() -> applyContactState(contactState.applyMessage(messageToApply))
        messageToApply.isHardwareError() -> {
            val textMessageType = when(messageToApply.messageType){
                MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW,
                MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING -> TextMessageType.LOW_VOLTAGE
                else -> null
            }

            val voiceMessageType = when(messageToApply.messageType){
                MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW,
                MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING -> VoiceMessageType.LOW_VOLTAGE
                else -> VoiceMessageType.HARDWARE_ERROR
            }

            when (type) {
                ProcedureProgressType.UNKNOWN,
                ProcedureProgressType.WAIT_FOR_ALCOTEST -> {
                    if (employee != null) {
                        ResultSet(
                                procedureProgress = updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY),
                                cameraCommandType = CameraCommandType.STOP_PREVIEW,
                                alcotestError = AlcotestError(employee, AlcotestErrorType.HARDWARE),
                                voiceMessageType = voiceMessageType,
                                messageForLog = messageToApply.messageType.toString(),
                                textMessageType = textMessageType


                        )
                    } else {
                        ResultSet(
                                procedureProgress = updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY),
                                cameraCommandType = CameraCommandType.STOP_PREVIEW,
                                voiceMessageType = voiceMessageType,
                                messageForLog = messageToApply.messageType.toString(),
                                textMessageType = textMessageType
                        )
                    }
                }
                else -> ResultSet(
                        procedureProgress = this,
                        messageForLog = messageToApply.messageType.toString(),
                        textMessageType = textMessageType
                )
            }
        }



        messageToApply.messageType == MessageType.RED_BUTTON_PRESSED -> when(type){
            ProcedureProgressType.UNKNOWN -> ResultSet(
                    procedureProgress = updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY),
                    messageForLog = messageToApply.messageType.toString()
            )
            ProcedureProgressType.READY -> if(canDisplayPreview){
                ResultSet(
                        procedureProgress = update(type = ProcedureProgressType.WAIT_FOR_EMPLOYEE),
                        cameraCommandType = CameraCommandType.GET_QR_CODE,
                        textMessageType = TextMessageType.SCAN_EMPLOYEE_QR_CODE,
                        voiceMessageType = VoiceMessageType.SCAN_PASS,
                        messageForLog = messageToApply.messageType.toString()
                        )
            } else ResultSet(
                    procedureProgress = this,
                    messageForLog = messageToApply.messageType.toString()
            )
            else -> ResultSet(procedureProgress = this,
                    messageForLog = messageToApply.messageType.toString()
            )
        }
        messageToApply.isInfo() -> ResultSet(procedureProgress = update(settings = settings.update(createSettings(messageToApply))))
        messageToApply.isProgrammingResponse() -> ResultSet(procedureProgress = this, programmingResponse = createProgrammingResponseType(messageToApply))
        messageToApply.isLog() -> ResultSet(procedureProgress = this, messageForLog = messageToApply.toString())
        else -> ResultSet(this)
    }
}

fun ProcedureProgress.applyContactState(stateToApply:ContactState):ResultSet{
    val procedureProgress = when(stateToApply.allTrue()){
        null -> when(type){
            ProcedureProgressType.UNKNOWN, ProcedureProgressType.WAIT_FOR_ALCOTEST ->
                updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY,contactState = stateToApply)
            else ->update(contactState = stateToApply)
        }

        false ->
            if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST && photo == null) {
                updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY,contactState = stateToApply)
            }else if(type==ProcedureProgressType.UNKNOWN){
                updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY,contactState = stateToApply)
            }else{
                update(contactState = stateToApply)
            }
        true ->
            if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE && employee!=null){
                update(type = ProcedureProgressType.WAIT_FOR_ALCOTEST, contactState = stateToApply)
            }else if(type==ProcedureProgressType.UNKNOWN){
                updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY, contactState = stateToApply)
            }else{
                update(contactState = stateToApply)
            }
    }

//    val command = if(
//            type==ProcedureProgressType.WAIT_FOR_EMPLOYEE &&
//            employee!=null &&
//            stateToApply.allTrue()==true
//    ) CommandStart() else null
    val command = when{
        type==ProcedureProgressType.WAIT_FOR_EMPLOYEE &&
                employee!=null &&
                stateToApply.allTrue() == true -> CommandStart()
        type==ProcedureProgressType.WAIT_FOR_ALCOTEST &&
                photo == null &&
                stateToApply.allTrue() != true -> CommandStop()
        else -> null
    }



    val cameraCommandType = when{
        stateToApply.allTrue()==null  -> CameraCommandType.STOP_PREVIEW
        stateToApply.allTrue()==false && type == ProcedureProgressType.WAIT_FOR_ALCOTEST -> CameraCommandType.STOP_PREVIEW
        else -> null
    }

    val textMessage = if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST && stateToApply.allTrue()==false){
        TextMessageType.MIRROR_OR_HOLDER_FOLDED
    }else null

    val voiceMessage = if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST && stateToApply.allTrue()==false){
        if(photo==null) VoiceMessageType.ERROR_MIRROR_OR_HOLDER_FLIPPED_DURING_TEST else VoiceMessageType.MIRROR_OR_HOLDER_FOLDED
    }else null

    val alcotestError = if(employee!=null){
        when{
            stateToApply.allTrue() == false && photo == null -> AlcotestError(employee, AlcotestErrorType.SOFTWARE)
            stateToApply.allTrue() == null -> AlcotestError(employee, AlcotestErrorType.SOFTWARE)
            else -> null
        }
    }else{
        null
    }

    return ResultSet(
            procedureProgress = procedureProgress,
            command = command,
            cameraCommandType = cameraCommandType,
            alcotestError = alcotestError,
            textMessageType = textMessage,
            voiceMessageType = voiceMessage,
            messageForLog = stateToApply.toString()
    )
}

fun ProcedureProgress.applyAlcotestProgress(alcoTestProgressToApply:AlcotestProgress):ResultSet{
   // Log.d("happynew", "ProcedureProgress.applyAlcotestProgress $alcoTestProgressToApply")
    val procedureProgress = when(type){
        ProcedureProgressType.UNKNOWN -> updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY)
        ProcedureProgressType.WAIT_FOR_READY ->
            when(alcoTestProgressToApply.type){
                AlcotestProgressType.READY -> update(
                        type = ProcedureProgressType.READY,
                        alcoTestProgress = alcoTestProgressToApply)
                else->update(alcoTestProgress = alcoTestProgressToApply)
            }
        ProcedureProgressType.READY,
        ProcedureProgressType.WAIT_FOR_EMPLOYEE->
            when(alcoTestProgressToApply.type){
                AlcotestProgressType.READY -> update(alcoTestProgress = alcoTestProgressToApply)
                else -> updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY)
            }
        ProcedureProgressType.WAIT_FOR_ALCOTEST ->
            when(alcoTestProgressToApply.type){
                //AlcotestProgressType.UNKNOWN,
                AlcotestProgressType.RESULT,
                AlcotestProgressType.END_AFTER_TIMEOUT,
                AlcotestProgressType.HARDWARE_ERROR -> updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY)
                AlcotestProgressType.FLOW_ERROR -> update(alcoTestProgress = alcoTestProgressToApply, photo = null)
                else -> update(alcoTestProgress = alcoTestProgressToApply)
            }
    }

    val cameraCommandType = when{
        type ==ProcedureProgressType.WAIT_FOR_EMPLOYEE && alcoTestProgressToApply.type != AlcotestProgressType.READY ->
            CameraCommandType.STOP_PREVIEW
        type == ProcedureProgressType.WAIT_FOR_ALCOTEST ->{
            when(alcoTestProgressToApply.type){
                AlcotestProgressType.UNKNOWN,
                AlcotestProgressType.END_AFTER_TIMEOUT,
                AlcotestProgressType.SAMPLE_PROCESSING,
                AlcotestProgressType.HARDWARE_ERROR -> CameraCommandType.STOP_PREVIEW
                AlcotestProgressType.FLOW_DETECTED -> CameraCommandType.TAKE_PHOTO
                //AlcotestProgressType.WAIT_FOR_FLOW -> CameraCommandType.START_PREVIEW
                AlcotestProgressType.WAIT_AFTER_START -> CameraCommandType.START_PREVIEW
                else -> null
            }
        }
        else -> null
    }

    val alcotestResult = if(alcoTestProgressToApply.type == AlcotestProgressType.RESULT){
        if(employee!=null && photo!=null) {
            AlcotestResult(
                    employee = employee,
                    concentration = alcoTestProgressToApply.param?.toDoubleOrNull()?: throw IllegalArgumentException(),
                    success = true,
                    photo = photo)
        }else {
            null
        }
    }else null

    val textMessageType = when(type){
        ProcedureProgressType.WAIT_FOR_READY ->
            when{
                alcoTestProgressToApply.type == AlcotestProgressType.READY && canDisplayPreview -> TextMessageType.PRESS_START_BUTTON
                else -> null
            }
        ProcedureProgressType.WAIT_FOR_ALCOTEST ->
            when(alcoTestProgressToApply.type){
                AlcotestProgressType.WAIT_AFTER_START ->
                    if(this.alcoTestProgress.type!= AlcotestProgressType.WAIT_AFTER_START) TextMessageType.WAIT else null
                AlcotestProgressType.WAIT_AFTER_FLOW_ERROR ->
                    if(this.alcoTestProgress.type!= AlcotestProgressType.WAIT_AFTER_FLOW_ERROR) TextMessageType.WAIT else null
                AlcotestProgressType.WAIT_FOR_FLOW ->
                    if(this.alcoTestProgress.type!= AlcotestProgressType.WAIT_FOR_FLOW) TextMessageType.BLOW else null
                AlcotestProgressType.FLOW_DETECTED -> TextMessageType.KEEP_BLOW
                AlcotestProgressType.SAMPLE_PROCESSING -> TextMessageType.PROCESSING
                AlcotestProgressType.FLOW_ERROR -> TextMessageType.BAD_BLOW
                AlcotestProgressType.RESULT -> {
                    if(employee!=null && photo == null) TextMessageType.NO_PICTURE_RESULT else null
                }
                else -> null
            }
        else -> null
    }

    val voiceMessageType = if(alcotestResult!=null) {
        if(alcotestResult.concentration>0) VoiceMessageType.POSITIVE_RESULT else VoiceMessageType.NEGATIVE_RESULT
    }else {
        when (type) {
            ProcedureProgressType.WAIT_FOR_READY ->
                when {
                    alcoTestProgressToApply.type == AlcotestProgressType.READY && canDisplayPreview -> VoiceMessageType.PRESS_START_BUTTON
                    else -> null
                }
            ProcedureProgressType.WAIT_FOR_ALCOTEST ->
                when (alcoTestProgressToApply.type) {
                    AlcotestProgressType.WAIT_AFTER_START ->
                        if (this.alcoTestProgress.type != AlcotestProgressType.WAIT_AFTER_START) VoiceMessageType.WAIT else null
                    AlcotestProgressType.WAIT_AFTER_FLOW_ERROR ->
                        if (this.alcoTestProgress.type != AlcotestProgressType.WAIT_AFTER_FLOW_ERROR) VoiceMessageType.WAIT else null
                    AlcotestProgressType.WAIT_FOR_FLOW ->
                        if (this.alcoTestProgress.type != AlcotestProgressType.WAIT_FOR_FLOW) VoiceMessageType.BLOW else null
                    AlcotestProgressType.FLOW_DETECTED -> VoiceMessageType.CONTINUE_BLOW
                    AlcotestProgressType.SAMPLE_PROCESSING -> VoiceMessageType.PROCESSING
                    AlcotestProgressType.FLOW_ERROR -> VoiceMessageType.BAD_FLOW
                    AlcotestProgressType.RESULT -> {
                        if(employee!=null && photo == null) VoiceMessageType.NO_PICTURE_RESULT else null
                    }
                    else -> null
                }
            else -> null
        }
    }

    val command = if(!contactState.allDefined()) CommandInfo() else null

    val alcotestError = if(employee!=null) {
        when {
            type == ProcedureProgressType.WAIT_FOR_ALCOTEST && alcoTestProgressToApply.type == AlcotestProgressType.FLOW_ERROR ->
                AlcotestError(employee, AlcotestErrorType.BAD_BLOW)
            type == ProcedureProgressType.WAIT_FOR_ALCOTEST && alcoTestProgressToApply.type == AlcotestProgressType.TIMEOUT ->
                AlcotestError(employee, AlcotestErrorType.TIMEOUT_EXPIRED)
            type == ProcedureProgressType.WAIT_FOR_ALCOTEST && alcoTestProgressToApply.type == AlcotestProgressType.HARDWARE_ERROR ->
                AlcotestError(employee, AlcotestErrorType.HARDWARE)
            type == ProcedureProgressType.WAIT_FOR_ALCOTEST && alcoTestProgressToApply.type == AlcotestProgressType.RESULT && photo == null ->
                AlcotestError(employee, AlcotestErrorType.SOFTWARE)
            else -> null
        }
    }else null

    val messageForLog = when {
        alcoTestProgress.type != alcoTestProgressToApply.type -> {
            alcoTestProgressToApply.type.toString() +
                    if (alcoTestProgressToApply.type == AlcotestProgressType.RESULT) "  ${alcoTestProgressToApply.param}" else ""
        }
        alcoTestProgressToApply.type == AlcotestProgressType.RESULT && employee != null && photo == null -> {
            "NO PHOTO RESULT $employee"
        }
        else -> null
    }
        // Log.d("happynew", "ProcedureProgress.applyAlcotestProgress textMessage =  $textMessageType")
            return ResultSet(
                    procedureProgress = procedureProgress,
                    command = command,
                    cameraCommandType = cameraCommandType,
                    alcotestResult = alcotestResult,
                    textMessageType = textMessageType,
                    voiceMessageType = voiceMessageType,
                    alcotestError = alcotestError,
                    messageForLog = messageForLog
            )
}

fun ProcedureProgress.applyEmployee(employeeToApply: Employee):ResultSet{
    //Log.d("happynew", "ProcedureProgress.applyEmployee $employeeToApply")
    val procedureProgress = when(type){
        ProcedureProgressType.UNKNOWN -> updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY)
        ProcedureProgressType.WAIT_FOR_EMPLOYEE ->
           if(contactState.allTrue()==true){
               update(type = ProcedureProgressType.WAIT_FOR_ALCOTEST, employee = employeeToApply)
           }else{
               update(employee = employeeToApply)
           }
        else -> this
    }

    val command = if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE && contactState.allTrue()==true) CommandStart() else null
    val textMessage = if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE && contactState.allTrue()==false) TextMessageType.UNFOLD_MIRROR_AND_HOLDER else null
    val voiceMessage = if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE && contactState.allTrue()==false) VoiceMessageType.MIRROR_OR_HOLDER_FOLDED else null
    val messageForLog = employeeToApply.toString()
    val cameraCommand = if(type==ProcedureProgressType.WAIT_FOR_EMPLOYEE) CameraCommandType.STOP_PREVIEW else null

    return ResultSet(
            procedureProgress = procedureProgress,
            command = command,
            textMessageType = textMessage,
            voiceMessageType = voiceMessage,
            messageForLog = messageForLog,
            cameraCommandType = cameraCommand
    )
}


fun ProcedureProgress.applyPhoto(photoToApply: File):ResultSet{
    //Log.d("happynew", "ProcedureProgress.applyPhoto $photoToApply")
   val procedureProgress = when(type){
       ProcedureProgressType.UNKNOWN -> update(type = ProcedureProgressType.WAIT_FOR_READY)
       ProcedureProgressType.WAIT_FOR_ALCOTEST -> update(photo = photoToApply)
       else -> this
   }
    val cameraCommandType = null //if(type==ProcedureProgressType.WAIT_FOR_ALCOTEST) CameraCommandType.STOP_PREVIEW else null
    val messageForLog = "Photo: ${photoToApply.name}"

    return ResultSet(
            procedureProgress = procedureProgress,
            cameraCommandType = cameraCommandType,
            messageForLog = messageForLog
    )
}

fun ProcedureProgress.applyConnectionState(connectionStateToApply: ConnectionState):ResultSet{
    //val command = if(connectionStateToApply==ConnectionState.CONNECTED) CommandInfo() else null
    val cameraCommandType = when(connectionStateToApply){
        ConnectionState.DISCONNECTED,
        ConnectionState.DISCONNECTING,
        ConnectionState.CONNECTING,
        ConnectionState.SEARCHING -> when(type) {
            ProcedureProgressType.WAIT_FOR_EMPLOYEE,
            ProcedureProgressType.WAIT_FOR_ALCOTEST -> CameraCommandType.STOP_PREVIEW
            else -> null
        }
        else -> null
    }

    val alcotestError =  if(employee!=null){
        when{
            type==ProcedureProgressType.WAIT_FOR_ALCOTEST && connectionStateToApply!=ConnectionState.CONNECTED ->
                AlcotestError(employee,AlcotestErrorType.HARDWARE)
            else -> null
        }
    }else null

    val messageForLog = "Connection state: $connectionStateToApply"



    return ResultSet(
            procedureProgress = updateAndClearAlcotestProgress(
                    type = ProcedureProgressType.WAIT_FOR_READY,
                    connectionState = connectionStateToApply,
                    contactState = ContactState()

            ),
            //command = command,
            cameraCommandType = cameraCommandType,
            alcotestError = alcotestError,
            messageForLog = messageForLog
    )
}

fun ProcedureProgress.applyCanDisplayPreview(canDisplayPreviewToApply: Boolean): ResultSet{
   // Log.d("happynew", "ProcedureProgress.applyCanDisplayPreview $canDisplayPreviewToApply")
    val procedureProgress = when(type){
        ProcedureProgressType.UNKNOWN -> update(type = ProcedureProgressType.WAIT_FOR_READY)
        ProcedureProgressType.READY,
        ProcedureProgressType.WAIT_FOR_EMPLOYEE,
        ProcedureProgressType.WAIT_FOR_ALCOTEST ->
            if(canDisplayPreviewToApply){
                update(canDisplayPreview = canDisplayPreviewToApply)
            }else{
                updateAndClearAlcotestProgress(type = ProcedureProgressType.WAIT_FOR_READY, canDisplayPreview = canDisplayPreviewToApply)
            }
        ProcedureProgressType.WAIT_FOR_READY -> update(canDisplayPreview = canDisplayPreviewToApply)
    }

    val cameraCommandType = if(!canDisplayPreviewToApply) CameraCommandType.STOP_PREVIEW else null

    val command = when{
        type==ProcedureProgressType.WAIT_FOR_ALCOTEST &&
                !canDisplayPreview -> CommandStop()
        else -> null
    }

    val textMessage = when {
        type == ProcedureProgressType.READY && canDisplayPreviewToApply -> TextMessageType.PRESS_START_BUTTON
        type == ProcedureProgressType.WAIT_FOR_ALCOTEST && !canDisplayPreviewToApply -> TextMessageType.WAIT
        else -> null
    }
    val voiceMessage = if(type==ProcedureProgressType.READY && canDisplayPreviewToApply && !canDisplayPreview) VoiceMessageType.PRESS_START_BUTTON else null

    val alcotestError =  if(employee!=null){
        when{
            type==ProcedureProgressType.WAIT_FOR_ALCOTEST && !canDisplayPreviewToApply ->
                AlcotestError(employee,AlcotestErrorType.SOFTWARE)
            else -> null
        }
    }else null
    val messageForLog = if(canDisplayPreview!=canDisplayPreviewToApply) "Can display preview: $canDisplayPreviewToApply" else null

    return ResultSet(
            procedureProgress = procedureProgress,
            cameraCommandType = cameraCommandType,
            textMessageType = textMessage,
            voiceMessageType = voiceMessage,
            alcotestError = alcotestError,
            messageForLog = messageForLog,
            command = command
            )
}

fun ProcedureProgress.applyCameraError(e:Throwable):ResultSet{
    val procedureProgress = updateAndClearAlcotestProgress(ProcedureProgressType.WAIT_FOR_READY)
    val cameraCommandType = CameraCommandType.STOP_PREVIEW
    val command = when{
        type==ProcedureProgressType.WAIT_FOR_ALCOTEST  -> CommandStop()
        else -> null
    }
    val textMessageType = TextMessageType.CAMERA_ERROR
    val voiceMessageType = VoiceMessageType.CAMERA_ERROR
    val alcotestError = if(employee!=null && type == ProcedureProgressType.WAIT_FOR_ALCOTEST){
        AlcotestError(employee,AlcotestErrorType.SOFTWARE)
    } else null
    val messageForLog = "CAMERA ERROR: ${e.message}"
    return ResultSet(
            procedureProgress = procedureProgress,
            cameraCommandType = cameraCommandType,
            textMessageType = textMessageType,
            voiceMessageType = voiceMessageType,
            alcotestError = alcotestError,
            messageForLog = messageForLog,
            command = command
    )

}

fun createSettings(message:Message):AlcotesterSettings{
    return when(message.messageType){
        MessageType.SERIAL -> AlcotesterSettings(serial = message.params[KEY_SERIAL]?.toInt())
        MessageType.GUID -> AlcotesterSettings(guid = message.params[KEY_GUID])
        MessageType.HARDWARE -> AlcotesterSettings(hardware = message.params[KEY_HARDWARE])
        MessageType.FIRMWARE -> AlcotesterSettings(firmware = message.params[KEY_FIRMWARE])
        MessageType.COUNT_TESTS -> AlcotesterSettings(
                testsCountTotal = message.params[KEY_COUNT_TESTS_TOTAL]?.toInt(),
                testsCountPositive = message.params[KEY_COUNT_TESTS_POSITIVE]?.toInt()
        )
        MessageType.COUNT_TESTS_TOTAL -> AlcotesterSettings(testsCountTotal = message.params[KEY_COUNT_TESTS_TOTAL]?.toInt())
        MessageType.COUNT_TESTS_POSITIVE -> AlcotesterSettings(testsCountPositive = message.params[KEY_COUNT_TESTS_POSITIVE]?.toInt())
        MessageType.COUNT_TESTS_FAILED -> AlcotesterSettings(testsCountFailed = message.params[KEY_COUNT_TESTS_FAILED]?.toInt())
        MessageType.AREF -> AlcotesterSettings(aref = message.params[KEY_AREF]?.toInt())
        MessageType.PRESSURE -> AlcotesterSettings(pressure = message.params[KEY_PRESSURE]?.toInt())
        MessageType.CALIBRATION-> AlcotesterSettings(calibration = message.params[KEY_CALIBRATION]?.toInt())
        else -> AlcotesterSettings()
    }
}

fun createProgrammingResponseType(message: Message):ProgrammingResponseType?{
    return when(message.messageType){
        MessageType.ERROR_SAFETY_BUTTON_NOT_PRESSED -> ProgrammingResponseType.ERROR_SAFETY_BUTTON_NOT_PRESSED
        MessageType.OK -> ProgrammingResponseType.OK
        MessageType.ERROR_UNKNOWN_COMMAND -> ProgrammingResponseType.ERROR_UNKNOWN_COMMAND
        MessageType.ERROR_INVALID_PARAMETER -> ProgrammingResponseType.ERROR_INVALID_PARAMETER
        else -> null
    }

}

fun AlcotesterSettings.update(infoMessage: AlcotesterSettings):AlcotesterSettings{
        return AlcotesterSettings(
                infoMessage.serial?:this.serial,
                infoMessage.guid?:this.guid,
                infoMessage.hardware?:this.hardware,
                infoMessage.firmware?:this.firmware,
                infoMessage.testsCountTotal?:this.testsCountTotal,
                infoMessage.testsCountPositive?:this.testsCountPositive,
                infoMessage.testsCountFailed?:this.testsCountFailed,
                infoMessage.aref?:this.aref,
                infoMessage.pressure?:this.pressure,
                infoMessage.calibration?:this.calibration
        )
}

