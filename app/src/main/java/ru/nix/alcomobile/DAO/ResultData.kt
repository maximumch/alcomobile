package ru.nix.alcomobile.DAO

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "result")
data class ResultData(
        val photo:String,
        val result:Double,
        val idc:Int,
        val date:Long,
        val name:String? = null,
        val uploaded:Boolean?=false,
        @PrimaryKey val id:String = UUID.randomUUID().toString()
)

fun ResultData.setUploaded() = ResultData(
        photo = photo,
        result = result,
        idc = idc,
        date = date,
        name = name,
        uploaded = true,
        id = id
)