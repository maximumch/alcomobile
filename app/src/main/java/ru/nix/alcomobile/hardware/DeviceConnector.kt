package ru.nix.alcomobile.hardware

import android.content.Context
import io.reactivex.Observable



interface DeviceConnector {
    var inputData:Observable<String>
    val outputData:Observable<String>
    val connectionState: Observable<ConnectionState>
    fun scanDevices(): Observable<BTDevice>
    fun connectDevice(context: Context, address:String)


    fun disconnect()
}

