package ru.nix.alcomobile.hardware

import java.util.regex.Pattern

const val KEY_TEST_RESULT = "key_test_result"
const val KEY_LOG_MESSAGE = "key_log_message"
const val KEY_SERIAL = "key_serial"
const val KEY_GUID = "key_guid"
const val KEY_HARDWARE = "key_hardware"
const val KEY_FIRMWARE = "key_firmware"
const val KEY_COUNT_TESTS_TOTAL = "key_count_tests_total"
const val KEY_COUNT_TESTS_POSITIVE = "key_count_tests_positive"
const val KEY_COUNT_TESTS_FAILED = "key_count_tests_failed"
const val KEY_AREF = "key_aref"
const val KEY_PRESSURE = "key_pressure"
const val KEY_CALIBRATION = "key_calibration"
const val KEY_VOLTAGE = "key_voltage"
const val KEY_CONTACT_STATE = "key_contact_state"


val messagePatterns = listOf<MessagePattern>(
        MessagePattern(MessageType.END, Pattern.compile("([$]END[\\r\\n]+)"), null),
        MessagePattern(MessageType.WAIT, Pattern.compile("([$]WAIT[\\r\\n]+)"), null),
        MessagePattern(MessageType.STANDBY, Pattern.compile("([$]STANBY[\\r\\n]+)"), null),
        MessagePattern(MessageType.TRIGGER, Pattern.compile("([$]TRIGGER[\\r\\n]+)"), null),
        MessagePattern(MessageType.BREATH, Pattern.compile("([$]BREATH[\\r\\n]+)"), null),
        MessagePattern(MessageType.RESULT, Pattern.compile("([$]RESULT)(,)([\\d.]+)(-OK[\\r\\n]+)"), hashMapOf(3 to KEY_TEST_RESULT)),
        MessagePattern(MessageType.FLOW_ERROR, Pattern.compile("([$]FLOW)(,)(ERR[\\r\\n]+)"), null),
        MessagePattern(MessageType.TIME_OUT, Pattern.compile("([$]TIME)(,)(OUT[\\r\\n]+)"), null),
        MessagePattern(MessageType.RED_BUTTON_PRESSED, Pattern.compile("([$]REDB_PRESSED[\\r\\n]+)"), null),
        MessagePattern(MessageType.OK, Pattern.compile("([$]OK[\\r\\n]+)"), null),


        MessagePattern(MessageType.ERROR_UNKNOWN_COMMAND, Pattern.compile("([$]ERROR,UNKNOWN_COMMAND[\\r\\n]+)"), null),
        MessagePattern(MessageType.ERROR_INVALID_PARAMETER, Pattern.compile("([$]ERROR,INVALID_PARAMETER[\\r\\n]+)"), null),
        MessagePattern(MessageType.ERROR_SAFETY_BUTTON_NOT_PRESSED, Pattern.compile("([$]ERROR,SAFETY_BUTTON_NOT_PRESSED[\\r\\n]+)"), null),
        MessagePattern(MessageType.LOG, Pattern.compile("([$]LOG)(,)(.+)([\\r])([\\n]+)"), hashMapOf(3 to KEY_LOG_MESSAGE)),
        MessagePattern(MessageType.SERIAL, Pattern.compile("([$]SERIAL)(,)([\\d.]+)([\\r\\n]+)"), hashMapOf(3 to KEY_SERIAL)),
        MessagePattern(MessageType.GUID, Pattern.compile("([$]GUID)(,)(.+)([\\r])([\\n]+)"), hashMapOf(3 to KEY_GUID)),
        MessagePattern(MessageType.HARDWARE, Pattern.compile("([$]HARDWARE)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_HARDWARE)),
        MessagePattern(MessageType.FIRMWARE, Pattern.compile("([$]FIRMWARE)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_FIRMWARE)),
        MessagePattern(MessageType.COUNT_TESTS, Pattern.compile("([$]COUNT)(,)([\\d]+)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_COUNT_TESTS_TOTAL, 5 to KEY_COUNT_TESTS_POSITIVE)),
        MessagePattern(MessageType.AREF, Pattern.compile("([$]AREF)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_AREF)),
        MessagePattern(MessageType.PRESSURE, Pattern.compile("([$]PRESSURE)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_PRESSURE)),
        MessagePattern(MessageType.CALIBRATION, Pattern.compile("([$]CALIBRATION)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_CALIBRATION)),
        MessagePattern(MessageType.COUNT_TESTS_TOTAL, Pattern.compile("([$]USE_COUNT)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_COUNT_TESTS_TOTAL)),
        MessagePattern(MessageType.COUNT_TESTS_POSITIVE, Pattern.compile("([$]POS_COUNT)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_COUNT_TESTS_POSITIVE)),
        MessagePattern(MessageType.COUNT_TESTS_FAILED, Pattern.compile("([$]FAIL_COUNT)(,)([\\d]+)([\\r\\n]+)"), hashMapOf(3 to KEY_COUNT_TESTS_FAILED)),

        MessagePattern(MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW, Pattern.compile("([$]ERROR,SUPPLY_VOLTAGE_IS_TOO_LOW_[(])([\\d.]+)(V[)][\\r\\n]+)"), hashMapOf(2 to KEY_VOLTAGE)),
        MessagePattern(MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH, Pattern.compile("([$]ERROR,SUPPLY_VOLTAGE_IS_TOO_HIGH_[(])([\\d.]+)(V[)][\\r\\n]+)"), hashMapOf(2 to KEY_VOLTAGE)),
        MessagePattern(MessageType.ERROR_NOT_ALL_COIL_TESTS_PASSED, Pattern.compile("([$]ERROR,NOT_ALL_COIL_TESTS_PASSED[\\r\\n]+)"), null),
        MessagePattern(MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING, Pattern.compile("([$]ERROR,SUPPLY_VOLTAGE_DROPPED_TOO_LOW_[(])([\\d.]+)(V[)]_WHILE_PUMPING[\\r\\n]+)"), hashMapOf(2 to KEY_VOLTAGE)),
        MessagePattern(MessageType.ERROR_PUMP_IS_NOT_CONNECTED, Pattern.compile("([$]ERROR,PUMP_IS_NOT_CONNECTED[\\r\\n]+)"), null),
        MessagePattern(MessageType.ERROR_PUMP_CANNOT_TURN_ON, Pattern.compile("([$]ERROR,PUMP_CANNOT_TURN_ON[\\r\\n]+)"), null),

        MessagePattern(MessageType.CONTACT1_STATE, Pattern.compile("([$]CONTACT1_STATE)(,)([\\d.]+)([\\r\\n]+)"), hashMapOf(3 to KEY_CONTACT_STATE)),
        MessagePattern(MessageType.CONTACT2_STATE, Pattern.compile("([$]CONTACT2_STATE)(,)([\\d.]+)([\\r\\n]+)"), hashMapOf(3 to KEY_CONTACT_STATE))

)

enum class MessageType {
    END, WAIT, STANDBY, TRIGGER, BREATH, RESULT, FLOW_ERROR, TIME_OUT, RED_BUTTON_PRESSED, OK,
    ERROR_UNKNOWN_COMMAND, ERROR_INVALID_PARAMETER, ERROR_SAFETY_BUTTON_NOT_PRESSED, LOG,
    SERIAL, GUID, HARDWARE, FIRMWARE, COUNT_TESTS, AREF, PRESSURE, CALIBRATION,
    COUNT_TESTS_TOTAL, COUNT_TESTS_POSITIVE, COUNT_TESTS_FAILED,
    ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW,
    ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH,
    ERROR_NOT_ALL_COIL_TESTS_PASSED,
    ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING,
    ERROR_PUMP_IS_NOT_CONNECTED,
    ERROR_PUMP_CANNOT_TURN_ON,
    CONTACT1_STATE,CONTACT2_STATE,
    ERROR_ADVICE_USE_BETTER_QUALITY_USB_CABLE,
    ERROR_ADVICE_USE_USB_HUB_WITH_EXTERNAL_POWER_SUPPLY,
    ERROR_ADVICE_USE_ANOTHER_PC_MOTHERBOARD,
    ERROR_ADVICE_USE_ANOTHER_USB_PORT,
    ERROR_ADVICE_CHECK_D2_Q3_R19_R20,
    ERROR_ADVICE_CHECK_CONNECTION_OF_PUMP_COIL,
    ERROR_ADVICE_CHECK_DIODE_D2,

    START, INFO, ZERO

}



data class Message(val messageType: MessageType, val params: Map<String,String>)

class MessagePattern(val messageType:MessageType, val pattern: Pattern, val groupToParamMap: Map<Int,String>?)



fun parseMessages(str:String): Pair<List<Message>,Int>{
    val messageList = ArrayList<Message>()
    var pos = 0
    for(messagePattern in messagePatterns){
        val matcher = messagePattern.pattern.matcher(str)
        while(matcher.find()){
            val params = HashMap<String,String>()
            if(messagePattern.groupToParamMap!=null){
                for((group, key) in messagePattern.groupToParamMap){
                    val paramValue = matcher.group(group)
                    params[key] = paramValue
                }
            }

            messageList.add(Message(messagePattern.messageType,params))

            pos = kotlin.math.max(pos,matcher.end())
        }
    }
    return Pair(messageList,pos)
}

fun Message.isAlcotestProgress()= when(messageType){
    MessageType.END,
    MessageType.WAIT,
    MessageType.STANDBY,
    MessageType.TRIGGER,
    MessageType.BREATH,
    MessageType.RESULT,
    MessageType.FLOW_ERROR,
    MessageType.TIME_OUT -> true
    else -> false
}

fun Message.isContactState() = when(messageType){
    MessageType.CONTACT1_STATE,
        MessageType.CONTACT2_STATE -> true
    else -> false
}

fun Message.isHardwareError() = when(messageType){
    MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_LOW,
    MessageType.ERROR_SUPPLY_VOLTAGE_IS_TOO_HIGH,
    MessageType.ERROR_NOT_ALL_COIL_TESTS_PASSED,
    MessageType.ERROR_SUPPLY_VOLTAGE_DROPPED_TO_LOW_WHILE_PUMPING,
    MessageType.ERROR_PUMP_IS_NOT_CONNECTED,
    MessageType.ERROR_PUMP_CANNOT_TURN_ON -> true
    else -> false
}

fun Message.isInfo() = when(messageType){
    MessageType.SERIAL,
    MessageType.GUID,
    MessageType.HARDWARE,
    MessageType.FIRMWARE,
    MessageType.COUNT_TESTS,
    MessageType.AREF,
    MessageType.PRESSURE,
    MessageType.CALIBRATION,
    MessageType.COUNT_TESTS_TOTAL,
    MessageType.COUNT_TESTS_POSITIVE,
    MessageType.COUNT_TESTS_FAILED -> true
    else -> false
}

//fun Message.isProgrammingError() = when(messageType){
//    MessageType.ERROR_UNKNOWN_COMMAND,
//    MessageType.ERROR_INVALID_PARAMETER -> true
//    else -> false
//}

fun Message.isProgrammingResponse() = when(messageType){
    MessageType.ERROR_SAFETY_BUTTON_NOT_PRESSED,
    MessageType.OK,
    MessageType.ERROR_UNKNOWN_COMMAND,
    MessageType.ERROR_INVALID_PARAMETER -> true
    else -> false
}

fun Message.isLog() = when(messageType){
    MessageType.LOG -> true
    else -> false
}





