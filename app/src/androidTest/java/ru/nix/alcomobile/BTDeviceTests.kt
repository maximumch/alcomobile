package ru.nix.alcomobile



import android.bluetooth.BluetoothManager
import android.content.Context
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import android.util.Log
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import ru.nix.alcomobile.hardware.*
import ru.nix.alcomobile.rx.OperatorAlcotestProgress
import ru.nix.alcomobile.rx.OperatorParseMessage
import ru.nix.alcomobile.rx.filterResult
import java.text.DecimalFormat


@RunWith(AndroidJUnit4::class)
class BTDeviceTests{
    lateinit var  appContext: Context

    @Before
    fun init(){
        appContext = InstrumentationRegistry.getTargetContext()
    }

    @Test
    fun scanDevices(){

        val bluetoothManager = appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
        val mBluetoothAdapter = bluetoothManager?.getAdapter()
        val commandPublishSubject = PublishSubject.create<Command>()

        if(mBluetoothAdapter!=null) {
            val alcotesterConnector = AlcotesterConnectorHardware(appContext, commandPublishSubject)
            var s:Disposable? = null
            var c:Disposable?=null
            var d:Disposable?=null
            s = alcotesterConnector.scanDevices().subscribe() { btDevice ->
                Log.d("happy", "Name = ${btDevice.name} Address = ${btDevice.address}" )
                if(btDevice.name=="HMSoft"){
                    s?.dispose()
                    c = alcotesterConnector.connectDevice(appContext,btDevice.address).subscribe{
                        Log.d("happy", "State = ${it.first.name}")
                        if(it.first== ConnectionStateOld.CONNECTED){
                            d=it.second
                                    //.doOnNext { str-> Log.d("happy","Data  = ${str}")}
                                    .lift(OperatorParseMessage())
                                    .lift(OperatorAlcotestProgress())
                                    .filterResult()
                                    .subscribe{
                                        result->
                                        Log.d("happy","Progress  = ${result}")}
                        }
                    }


                }

            }


            Thread.sleep(15000)
            commandPublishSubject.onNext(CommandStart())
            Thread.sleep(60000)


            s.dispose()
            d?.dispose()
            c?.dispose()
            Thread.sleep(3000)

        }



        assert(true)




    }

    @Test
    fun format(){
        val format = DecimalFormat()
        val i=1.087
        format.minimumIntegerDigits = 5
        format.maximumFractionDigits = 0
        format.isGroupingUsed = false
        val s = format.format(i*1000)
        Log.d("happy",s)


    }

    @Test
    fun match(){
        val str = "as;dklje\$ENDldkvj\$WAITlskdjv"
        val msgs = parseMessages(str)
        assert(msgs.first.size==2)


        val str2 = "as;dklje\$ENDldkvjkdjv"
        val msgs2 = parseMessages(str)
        assert(msgs.first.size==1)
        assert(msgs.first[0].messageType==MessageType.END)





    }

    @Test
    fun asd(){
        val a = AlcotesterConnectorFakeOld(null,null)
        a.scanDevices().subscribe{t -> Log.d("happy",t.toString())}

        Thread.sleep(10000)
    }


    @Test
    fun makeTest(){
        val a = AlcotesterConnectorFakeOld(null,null)
        a.makeTest()
                .doOnNext { t-> Log.d("happy", "STRING: ${t.toString()}") }
                .lift(OperatorParseMessage())
                .doOnNext { t-> Log.d("happy", "MESSAGE: ${t.messageType.name}") }
                .lift(OperatorAlcotestProgress())
                .doOnNext { t-> Log.d("happy", "PROGRESS: ${t.type.name}") }
                .filterResult()
                .subscribe{r->Log.d("happy", "RESULT: success = ${r.success} result = ${r.result}") }
        Thread.sleep(20000)
    }

    @Test
    fun postTestOnDemand() {
        val commandPublishSubject = PublishSubject.create<Command>()
        val a = AlcotesterConnectorFakeOld(null,null)
        val d = a.postTestOnDemand(commandPublishSubject)
                .doOnNext { t-> Log.d("happy", "STRING: ${t.toString()}") }
                .lift(OperatorParseMessage())
                .doOnNext { t-> Log.d("happy", "MESSAGE: ${t.messageType.name}") }
                .lift(OperatorAlcotestProgress())
                .doOnNext { t-> Log.d("happy", "PROGRESS: ${t.type.name}") }
                .filterResult()
                .subscribe{r->Log.d("happy", "RESULT: success = ${r.success} result = ${r.result}") }
        Thread.sleep(2000)
        Log.d("happy", "START TEST")
        commandPublishSubject.onNext(CommandStart())
        Thread.sleep(12000)
        Log.d("happy", "START TEST")
        commandPublishSubject.onNext(CommandStart())

        Thread.sleep(32000)
        d.dispose()

    }

    @Test
    fun qwer(){
        val commandPublishSubject = PublishSubject.create<Command>()
        val s = AlcotesterConnectorHardware(appContext,commandPublishSubject).scanDevices()
                .subscribe({d->Log.d("happy", "EVENT ${d.name}")},
                        {t->Log.d("happy", "OnError ${t.message}")}
                        )



        Thread.sleep(10000)
        s.dispose()

    }

}