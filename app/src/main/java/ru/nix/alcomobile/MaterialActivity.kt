package ru.nix.alcomobile

import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.DialogInterface
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Size
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.*
import androidx.lifecycle.Observer
import io.reactivex.disposables.Disposable
import ru.nix.alcomobile.audio.VoiceMessageType
import ru.nix.alcomobile.hardware.ConnectionState
import ru.nix.alcomobile.modelnew.TextMessageType
import ru.nix.alcomobile.modelnew.getState

import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.viewmodelsnew.AlcotestViewModel

import android.media.AudioManager

import android.media.MediaPlayer
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.alcotest_name
import kotlinx.android.synthetic.main.activity_main.alcotets_message
import kotlinx.android.synthetic.main.activity_main.connection_state
import kotlinx.android.synthetic.main.activity_main.contact1_message
import kotlinx.android.synthetic.main.activity_main.contact2_message
import kotlinx.android.synthetic.main.activity_main.mSurfaceView
import kotlinx.android.synthetic.main.activity_main.reconnect
import kotlinx.android.synthetic.main.activity_material_main_content.*
import kotlinx.android.synthetic.main.bottom_sheet_camera_preview.*
import kotlinx.android.synthetic.main.breath_progress.*
import kotlinx.android.synthetic.main.enter_password_dialog.view.*
import ru.nix.alcomobile.DAO.ResultData
import java.text.DateFormat
import java.util.concurrent.TimeUnit


private const val PERMISSIONS_REQUEST_CODE = 99
private const val READ_CERTIFICATE_REQUEST_CODE = 42

class MaterialActivity : AppCompatActivity(), EnterPasswordDialogFragment.EnterPasswordDialogListener,
        SensorEventListener {



    private lateinit var previewSize: Size
    private lateinit var imageSize:Size
    private var mSurfaceHolder:SurfaceHolder? = null
    private val mCompositeDisposable = CompositeDisposable()
    private lateinit var viewModel: AlcotestViewModel
    private var lastUserActionTime = System.currentTimeMillis()
    private lateinit var bluetoothStateBroadcastReceiver: BluetoothStateBroadcastReceiver
    private lateinit var voiceMessageDisposable:Disposable
    private var voiceMessages:Map<VoiceMessageType,MediaPlayer>?=null

    private lateinit var sensorManager: SensorManager
    private var light: Sensor? = null

    private val luminanceLiveData:MutableLiveData<Int> = MutableLiveData()
    private var luminance:Int = 0
    set(value) {
        field = value
        luminanceLiveData.postValue(value)
    }

    private var keepScreenOnDisposable:Disposable?=null




    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("happyStart", "onCreate 0")
        super.onCreate(savedInstanceState)
        Log.d("happyStart", "onCreate 1")
        setContentView(R.layout.activity_material)
        Log.d("happyStart", "onCreate 2")
        viewModel = provideViewModel()
        Log.d("happyStart", "onCreate finish")

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)



        val cameraSheet = camera_preview_sheet
        val cameraSheetBehavior = BottomSheetBehavior.from(cameraSheet)


        viewModel.connectionState.observe(this, Observer{
            val messageTextResId = when(it) {
                ConnectionState.DISCONNECTED -> R.string.connection_state_disconnected
                ConnectionState.CONNECTED -> R.string.connection_state_connected
                ConnectionState.CONNECTING -> R.string.connection_state_connecting
                ConnectionState.DISCONNECTING -> R.string.connection_state_disconnecting
                ConnectionState.SEARCHING -> R.string.connection_state_searching
            }
            connection_state.text=getText(messageTextResId)
            reconnect.isEnabled = when(it){
                ConnectionState.DISCONNECTED,
                ConnectionState.CONNECTED -> true
                else -> false
            }
            settings.isEnabled= (it==ConnectionState.CONNECTED)

            when(it){
                ConnectionState.CONNECTED -> reconnect.text = getString(R.string.disconnect)
                ConnectionState.DISCONNECTED -> reconnect.text = getString(R.string.reconnect)
            }
            if(it!=ConnectionState.CONNECTED) alcotets_message.text="-"
        })

        viewModel.procedureProgress.observe(this, Observer {
            contact1_message.text = when(it.contactState.getState(1)){
                true -> getString(R.string.text_message_mirror_unfolded)
                false -> getString(R.string.text_message_mirror_folded)
                null -> getString(R.string.text_message_mirror_default)
            }
            contact2_message.text = when(it.contactState.getState(2)){
                true -> getString(R.string.text_message_holder_unfolded)
                false -> getString(R.string.text_message_holder_folded)
                null -> getString(R.string.text_message_holder_default)
            }

             card_mirror.setBackgroundColor(resources.getColor(
                    if(it.contactState.getState(1)==false) R.color.colorErrorBackground else R.color.colorSurface
            ))

            card_holder.setBackgroundColor(resources.getColor(
                    if(it.contactState.getState(2)==false) R.color.colorErrorBackground else R.color.colorSurface
            ))

            alcotest_name.text = it.employee?.name?:"-"

        })

        viewModel.textMessageType.observe(this, Observer {
            val texMessageResId = when(it){
                TextMessageType.WAIT -> R.string.text_message_type_wait
                TextMessageType.PRESS_START_BUTTON -> R.string.text_message_type_press_start_button
                TextMessageType.SCAN_EMPLOYEE_QR_CODE -> R.string.text_message_type_scan_emplyee_qr_code
                TextMessageType.BLOW -> R.string.text_message_type_blow
                TextMessageType.KEEP_BLOW -> R.string.text_message_type_keep_blow
                TextMessageType.BAD_BLOW -> R.string.text_message_type_bad_blow
                TextMessageType.PROCESSING -> R.string.text_message_type_processing
                TextMessageType.UNFOLD_MIRROR_AND_HOLDER -> R.string.text_message_type_unfold_mirror_and_holder
                TextMessageType.MIRROR_OR_HOLDER_FOLDED -> R.string.text_message_type_mirror_or_holder_folded
                TextMessageType.CAMERA_ERROR -> R.string.text_message_type_camera_error
                TextMessageType.NO_PICTURE_RESULT -> R.string.text_message_type_no_picture_result
                TextMessageType.LOW_VOLTAGE -> R.string.text_message_type_low_voltage
            }
            alcotets_message.text = getString(texMessageResId)
            lastUserActionTime = System.currentTimeMillis()
        })

        viewModel.vibrate.observe(this,Observer{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                (getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(VibrationEffect.createOneShot(300,255))
            }else{
                (getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(300)
            }
            val tone = ToneGenerator(AudioManager.STREAM_ALARM,100)
            tone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD,500)
        })

        viewModel.showPreview.observe(this, Observer {
            cameraSheetBehavior.state = if(it) BottomSheetBehavior.STATE_EXPANDED else BottomSheetBehavior.STATE_COLLAPSED
            //backgroundView.visibility= if(it) View.INVISIBLE else View.VISIBLE

        })

        cameraSheetBehavior.setBottomSheetCallback(object:BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) { }
            override fun onStateChanged(view: View, state: Int) {
                if(viewModel.showPreview.value==true && state == BottomSheetBehavior.STATE_COLLAPSED) cameraSheetBehavior.state = STATE_EXPANDED
            }
        })

        viewModel.playCameraShutter.observe(this, Observer{
            val sound = MediaActionSound()
            sound.play(MediaActionSound.SHUTTER_CLICK);
        })

        viewModel.result.observe(this, Observer{
            alcotets_message.text = "${getString(R.string.alcotest_progress_result)} ${it.concentration}"
        })

        val resultsAdapter = ResultAdapter()
        results_list.adapter = resultsAdapter
        results_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        viewModel.results.observe(this, Observer {
            resultsAdapter.notifyDataSetChanged()
        })

        viewModel.certificate.observe(this, Observer {
            sync_message.text = if(it.isNullOrBlank()) "" else it

        })

        viewModel.certificateError.observe(this, Observer {str ->
            AlertDialog.Builder(this)
                    .setMessage(str)
                    .create()
                    .show()
        })

        reconnect.setOnClickListener {
            if(viewModel.connectionState.value==ConnectionState.CONNECTED) viewModel.disconnect()
            if(viewModel.connectionState.value==ConnectionState.DISCONNECTED) viewModel.reconnect(this)
        }

        settings.setOnClickListener {
            val intent = Intent(this,DeviceSettingsActivity::class.java)
            startActivity(intent)
        }

        search.setOnClickListener {
            viewModel.startDevicesScan()
            val intent = Intent(this,DevicesScanActivity::class.java)
            startActivity(intent)
        }

        card_sync.setOnLongClickListener {
            showSyncDialog()
            true
        }


        checkPermissions()

//        Observable.interval(10L,60L, TimeUnit.SECONDS).subscribe{
//                            if(System.currentTimeMillis() - lastUserActionTime< 5*60*1000){
//                    runOnUiThread { window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) }
//
//                }else{
//                    runOnUiThread{window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)}
//                }
//            }
        viewModel.initVoiceMessenger(this)

        viewModel.breathProgress.observe(this, Observer {
            breath_progress.visibility = if(it==null) View.INVISIBLE else View.VISIBLE

            fun progressColor(pointNumber:Long, progress:Long):Int{
                return if(pointNumber>progress) {
                    resources.getColor(R.color.colorInactiveProgress)
                }else{
                    resources.getColor(R.color.colorPrimary)
                }
            }


            if(it!=null){
                breath_progress_1.setBackgroundColor(progressColor(1,it))
                breath_progress_2.setBackgroundColor(progressColor(2,it))
                breath_progress_3.setBackgroundColor(progressColor(3,it))
                breath_progress_4.setBackgroundColor(progressColor(4,it))
                breath_progress_5.setBackgroundColor(progressColor(5,it))
                breath_progress_6.setBackgroundColor(progressColor(6,it))
                breath_progress_7.setBackgroundColor(progressColor(7,it))
                breath_progress_8.setBackgroundColor(progressColor(8,it))
            }
        })

        viewModel.deviceName.observe(this, Observer {
            device_name.text = it?:"-"
        })
        viewModel.deviceAddress.observe(this, Observer {
            device_address.text = it?:"-"
        })

        viewModel.reconnect(this)

        mSurfaceView.holder.addCallback(object:SurfaceHolder.Callback{
            override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {

            }

            override fun surfaceDestroyed(surfaceHolder: SurfaceHolder?) {
                mSurfaceHolder = null
                //viewModel.activityCanDisplayPreviewChanged(false)
            }

            override fun surfaceCreated(surfaceHolder: SurfaceHolder?) {
                if(surfaceHolder!=null){
                    mSurfaceHolder = surfaceHolder
                    viewModel.onSurfaceHolderCreated(surfaceHolder)
                    viewModel.activityCanDisplayPreviewChanged(true)
                }
            }


        })

        bluetoothStateBroadcastReceiver = BluetoothStateBroadcastReceiver(){
            viewModel.notifyBluetoothTurnOff()
        }
        val intentFilter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(bluetoothStateBroadcastReceiver,intentFilter)

        viewModel.torchMode.observe(this, Observer {
            updateTorchViews(it, viewModel.torchLiveData.value?:false)
        })

        viewModel.torchLiveData.observe(this, Observer {
            updateTorchViews(viewModel.torchMode.value?:TorchMode.OFF, it)
        })

        card_light.setOnClickListener {
            viewModel.changeTorchMode()
        }

        card_light.setOnLongClickListener {
            startActivity(Intent(this, LightDialogActivity::class.java))
            true
        }

        card_led_on.setOnClickListener {
            viewModel.torchOn()
        }

        card_led_off.setOnClickListener {
            viewModel.torchOff()
        }

        card_stop.setOnClickListener {
            viewModel.stop()
        }

        card_start.setOnClickListener {
            viewModel.startTest()
        }

        card_info.setOnClickListener {
            viewModel.startInfo()
        }

        viewModel.debugTextLiveData.observe(this, Observer {
            debugText.text = it
        })
    }

    private fun updateTorchViews(torchMode: TorchMode, torchOn:Boolean){
        torch_mode.setImageResource(when{
            torchMode == TorchMode.OFF -> R.drawable.ic_lightbulb_disabled
            torchMode == TorchMode.AUTO && torchOn -> R.drawable.ic_lightbulb_on
            else -> R.drawable.ic_lightbulb_off
        })
        card_light.setBackgroundColor(resources.getColor(
                if(torchOn) R.color.colorLightbulbOff else R.color.colorSurface
        ))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            READ_CERTIFICATE_REQUEST_CODE -> {
                if(resultCode== Activity.RESULT_OK) {
                    data?.data?.let {
                        viewModel.certFileSelected=true
                        viewModel.certFileUri = it
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

//    override fun onBackPressed() {
//        if(viewModel.isTestInProgress) {
//            viewModel.stop()
//        } else {
//            super.onBackPressed()
//        }
//    }



    private fun showSyncDialog(){
        AlertDialog.Builder(this)
                .setTitle(R.string.sync_dialog_title)
                .setItems(R.array.sync_dialog_items){ dialog, which ->
                    when(which){
                        0->{
                            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                                type = "*/*"
                            }
                            startActivityForResult(intent, READ_CERTIFICATE_REQUEST_CODE)
                        }
                        1->{
                            AlertDialog.Builder(this)
                                    .setMessage(R.string.message_certificate_delete)
                                    .setPositiveButton(R.string.button_ok){ _,_ ->
                                        viewModel.removeCertificate(this)
                                    }
                                    .setNegativeButton(R.string.button_cancel){_,_ ->

                                    }
                                    .create().show()
                        }
                    }
                }
                .create().show()

    }

    override fun onDialogPositiveClick(dialog: DialogFragment, password: String) {
        viewModel.certFileUri?.let{
            if(password.isNotEmpty()){
                viewModel.addCertificate(this, it, password)
            }
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }


    private fun provideViewModel(): AlcotestViewModel {
        val factory = InjectorUtils.provideAlcotestViewModelFactory(this)
        return ViewModelProviders.of(this,factory).get(AlcotestViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        Log.d("happy_voice","onResume thread ${Thread.currentThread()}")
        //if(mSurfaceHolder!=null) viewModel.activityCanDisplayPreviewChanged(true)
        viewModel.activityCanDisplayPreviewChanged(true)
        if(viewModel.certFileSelected) EnterPasswordDialogFragment().show(supportFragmentManager, "EnterPasswordDialog")
        viewModel.certFileSelected = false
        sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        keepScreenOnDisposable = Observable.interval(1,1, TimeUnit.MINUTES).subscribe {
            if(System.currentTimeMillis() - lastUserActionTime> 5*60*1000) {
                runOnUiThread { window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.activityCanDisplayPreviewChanged(false)
        sensorManager.unregisterListener(this)
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        keepScreenOnDisposable?.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            R.id.startScan -> {
                viewModel.startDevicesScan()
                val intent = Intent(this,DevicesScanActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.disconnect -> {
                Director.getInstance(this).disconnect()
                true
            }
            R.id.device_settings -> {
                val intent = Intent(this,DeviceSettingsActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposeVoiceMessenger()
        unregisterReceiver(bluetoothStateBroadcastReceiver)
    }

    private fun showBluetoothDisabledAlert() {
        AlertDialog.Builder(this)
                .setTitle(R.string.bluetooth_disabled_dialog_title)
                .setMessage(R.string.bluetooth_disabled_dialog_message)
                .show()

    }



    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            try {
                val permissionsToRequest = ArrayList<String>()
                val pm = this.packageManager
                val info = pm.getPackageInfo(this.packageName, PackageManager.GET_PERMISSIONS)
                val permissions = info.requestedPermissions
                for (permission in permissions) {
                    if(Build.VERSION.SDK_INT <Build.VERSION_CODES.P && permission=="android.permission.FOREGROUND_SERVICE") continue
                    if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        permissionsToRequest.add(permission)
                    }
                }
                if (permissionsToRequest.size > 0) {
                    val p = permissionsToRequest.toTypedArray()
                    requestPermissions(p, PERMISSIONS_REQUEST_CODE)
                }
            } catch (e: PackageManager.NameNotFoundException) {

            }

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        for(s in permissions)  Log.d("happyPermission","permission $s")
        for(r in grantResults)  Log.d("happyPermission","result $r")

        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            for (grantResult in grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {

                    return
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }



    private fun startMessageDialog(message:String){
        val builder = AlertDialog.Builder(this)
        builder.setPositiveButton("OK",null)
                .setMessage(message)
                .show()
    }

    override fun onAccuracyChanged(senosor: Sensor, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {
        luminanceLiveData.postValue(event.values[0].toInt())
        viewModel.setLuminance(event.values[0].toInt())
    }


    inner class ResultAdapter: RecyclerView.Adapter<ResultHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultHolder {
            val pressView = layoutInflater.inflate(R.layout.press_item_new, parent, false)
            return ResultHolder(pressView)
        }

        override fun getItemCount(): Int = viewModel.results.value?.size?:0
        override fun onBindViewHolder(holder: ResultHolder, position: Int){
            holder.bind(viewModel.results.value?.get(position)?: ResultData("",0.0, 0, 0,"ERROR"))
        }
    }

    class ResultHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var resultData: ResultData
        lateinit var name: TextView
        lateinit var result: TextView
        lateinit var resultDate: TextView
        lateinit var syncImage: ImageView

        init{
            itemView.findViewById<TextView>(R.id.name)?.also {name = it}
            itemView.findViewById<TextView>(R.id.result)?.also {result = it}
            itemView.findViewById<TextView>(R.id.result_date)?.also {resultDate = it}
            itemView.findViewById<ImageView>(R.id.sync_image)?.also {syncImage = it}
        }

        fun bind(resultData:ResultData){
            this.resultData = resultData
            name.text = resultData.name?:""
            result.text = resultData.result.toString()
            resultDate.text = DateFormat.getDateTimeInstance().format(resultData.date)
            syncImage.visibility=if(resultData.uploaded==true) View.VISIBLE else View.INVISIBLE
        }
    }
}

class EnterPasswordDialogFragment: DialogFragment(){
    private lateinit var listener: EnterPasswordDialogListener


    interface EnterPasswordDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment, password: String)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = context as EnterPasswordDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException((context.toString() +
                    " must implement NoticeDialogListener"))
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            var passwordEditText: EditText
            builder.setView(inflater.inflate(R.layout.enter_password_dialog,null).also{passwordEditText = it.password})
                    .setMessage(R.string.enter_certificate_password)
                    .setPositiveButton(R.string.button_ok,
                            DialogInterface.OnClickListener { dialog, id ->
                                val edit = this.view?.findViewById<EditText>(R.id.password)
                                listener.onDialogPositiveClick(this, passwordEditText.text.toString())
                            })
                    .setNegativeButton(R.string.cancel,
                            DialogInterface.OnClickListener { dialog, id ->
                                listener.onDialogNegativeClick(this)
                            })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }




}