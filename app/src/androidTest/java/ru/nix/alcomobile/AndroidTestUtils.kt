package ru.nix.replicator

import android.content.Context
import java.io.File
import java.io.FileOutputStream
import java.util.*


    fun createRandomFile(context: Context, maxSizeKb:Int): File {
        val fileName = UUID.randomUUID().toString()
        val file = File(context.filesDir,fileName)
        val fileOutputStream = FileOutputStream(file)
        fileOutputStream.use {
            val l = Random().nextInt(maxSizeKb)
            val data = ByteArray(1024)
            val dataRandom = Random()
            for (i in 0 until l) {
                dataRandom.nextBytes(data)
                fileOutputStream.write(data)
            }
        }
        return file




}