package ru.nix.alcomobile

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_devices_scan.*
import ru.nix.alcomobile.hardware.BTDevice
import ru.nix.alcomobile.utilites.InjectorUtils
import ru.nix.alcomobile.viewmodels.DevicesScanViewModel

class DevicesScanActivity : AppCompatActivity() {
    lateinit var viewModel: DevicesScanViewModel
    var devicesList: List<BTDevice> = ArrayList()
    lateinit var deviceAdapter:DeviceAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devices_scan)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = provideViewModel()

        viewModel.devicesScanning.observe(this, Observer{
            it?.also {refreshing -> swipe_refresh_layout.isRefreshing = refreshing  }
        })

        viewModel.devicesList.observe(this, Observer {
            it?.let {
                devicesList=it
                deviceAdapter.notifyDataSetChanged()
            }
        })

        viewModel.bluetoothDisabledError.observe(this, Observer{
            if(it==true) showBluetoothDisabledAlert()
        })

        swipe_refresh_layout.setOnRefreshListener {
            if(viewModel.devicesScanning.value==null || viewModel.devicesScanning.value==false){
                viewModel.startDevicesScan()
            }
        }

        deviceAdapter = DeviceAdapter()
        recycler.adapter = deviceAdapter
        recycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

   

    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId==android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showBluetoothDisabledAlert() {
        AlertDialog.Builder(this)
                .setTitle(R.string.bluetooth_disabled_dialog_title)
                .setMessage(R.string.bluetooth_disabled_dialog_message)
                .show()

    }

    private fun provideViewModel():DevicesScanViewModel{
        val factory = InjectorUtils.provideDeviceScanViewModelFactory(this)
        return ViewModelProviders.of(this,factory).get(DevicesScanViewModel::class.java)
    }

    fun connectDevice(device: BTDevice){
        viewModel.connect(this,device)
        this.finish()
    }

    inner class DeviceAdapter: RecyclerView.Adapter<DeviceHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceHolder {
            val deviceView = layoutInflater.inflate(R.layout.button_item2, parent, false)
            return DeviceHolder(deviceView){device -> connectDevice(device)}
        }

        override fun getItemCount(): Int = devicesList.size
        override fun onBindViewHolder(holder: DeviceHolder, position: Int){
            holder?.bind(devicesList[position])
        }
    }

    class DeviceHolder(itemView: View, private val onDeviceSelected: (BTDevice) -> Unit ) : RecyclerView.ViewHolder(itemView) {
        var device: BTDevice?=null
        lateinit var address: TextView
        lateinit var name:TextView

       init{
           itemView?.findViewById<TextView>(R.id.address)?.also {address = it}
           itemView?.findViewById<TextView>(R.id.name)?.also {name = it}
           itemView?.setOnClickListener {
               device?.also {onDeviceSelected.invoke(it) }
           }
       }


        fun bind(device: BTDevice){
            address.text = device.address
            name.text = device.name?:""
            this.device = device

        }



    }

}
